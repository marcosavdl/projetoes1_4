package entity;

	public class Cidade extends Entity{
	
		private String nomeCidade;
		private UnidadeFederacao uf;
		
		public Cidade() {
			super("idCidade");
			uf = new UnidadeFederacao();
		}
		
		public Cidade(long idCidade) {
			super("idCidade",idCidade);
			uf = new UnidadeFederacao();
		}
		
		public Cidade(String nomeCidade, UnidadeFederacao uf) {
			super("idCidade");
			this.nomeCidade = nomeCidade;
			this.uf = uf;
		}
		
		public Cidade(long idCidade, String nomeCidade, UnidadeFederacao uf) {
			super("idCidade",idCidade);
			this.nomeCidade = nomeCidade;
			this.uf = uf;
		}

		public long getIdCidade() {
			return (long) super.getIdentifier();
		}
		
		public void setIdCidade(long idCidade) {
			super.setIdentifier(idCidade);
		}
		
		public String getNomeCidade() {
			return nomeCidade;
		}

		public void setNomeCidade(String nomeCidade) {
			this.nomeCidade = nomeCidade;
		}

		public UnidadeFederacao getUf() {
			return uf;
		}

		public void setUf(UnidadeFederacao uf) {
			this.uf = uf;
		}
		
		
	}
