package entity;

import java.util.ArrayList;
import java.util.List;

	public class PrescricaoRemedio extends Entity{
	
		private String posologia;
		private ConsultaMedica consultaMedica;
		private List<Remedio> listaRemedio;
		
		public PrescricaoRemedio() {
			super("idPrescricaoRemedio");
			listaRemedio = new ArrayList<>();
			consultaMedica = new ConsultaMedica();
		}
		
		public PrescricaoRemedio(long idPrescricaoRemedio) {
			super("idPrescricaoRemedio",idPrescricaoRemedio);
			listaRemedio = new ArrayList<>();
			consultaMedica = new ConsultaMedica();
		}

		public PrescricaoRemedio(String posologia) {
			super("idPrescricaoRemedio");
			this.posologia = posologia;	
			listaRemedio = new ArrayList<>();
			consultaMedica = new ConsultaMedica();
		}
		
		public PrescricaoRemedio(String posologia, ConsultaMedica consultaMedica) {
			super("idPrescricaoRemedio");
			this.posologia = posologia;
			listaRemedio = new ArrayList<>();
			this.consultaMedica = consultaMedica;
		}

		public PrescricaoRemedio(long idPrescricaoRemedio, String posologia, ConsultaMedica consultaMedica) {
			super("idPrescricaoRemedio",idPrescricaoRemedio);
			this.posologia = posologia;
			listaRemedio = new ArrayList<>();
			this.consultaMedica = consultaMedica;
		}
		
		public PrescricaoRemedio(long idPrescricaoRemedio, String posologia, List<Remedio> listaRemedio, 
								 ConsultaMedica consultaMedica) {
			super("idPrescricaoRemedio",idPrescricaoRemedio);
			this.posologia = posologia;
			this.listaRemedio = listaRemedio;
			this.consultaMedica = consultaMedica;
		}

		public String getPosologia() {
			return posologia;
		}

		public void setPosologia(String posologia) {
			this.posologia = posologia;
		}

		public List<Remedio> getListaRemedio() {
			return listaRemedio;
		}

		public void setListaRemedio(List<Remedio> listaRemedio) {
			this.listaRemedio = listaRemedio;
		}

		public ConsultaMedica getConsultaMedica() {
			return consultaMedica;
		}

		public void setConsultaMedica(ConsultaMedica consultaMedica) {
			this.consultaMedica = consultaMedica;
		}
		
	}
