package entity;

	public class Entity {

		private String identifierName;
		private Object identifier;
		
		public Entity() {
			
		}
		
		public Entity(String identifierName) {
			this.identifierName = identifierName;
		}
		
		public Entity(String identifierName, Object identifier) {
			this.identifierName = identifierName;
			this.identifier = identifier;
		}

		public String getIdentifierName() {
			return identifierName;
		}

		public void setIdentifierName(String identifierName) {
			this.identifierName = identifierName;
		}
		
		public Object getIdentifier() {
			return identifier;
		}

		public void setIdentifier(Object identifier) {
			this.identifier = identifier;
		}
		
		
	}
