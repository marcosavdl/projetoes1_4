package entity;

	public class UnidadeFederacao extends Entity{
	
		private String nomeUF;
		
		public UnidadeFederacao() {
			super("siglaUF");
		}
		
		public UnidadeFederacao(String nomeUF) {
			super("siglaUF");
			this.nomeUF = nomeUF;
		}
		
		public UnidadeFederacao(String siglaUF, String nomeUF) {
			super("siglaUF",siglaUF);
			this.nomeUF = nomeUF;
		}

		public String getSiglaUF() {
			return (String) super.getIdentifier();
		}
		
		public void setSiglaUF(String siglaUF) {
			super.setIdentifier(siglaUF);
		}
		
		public String getNomeUF() {
			return nomeUF;
		}

		public void setNomeUF(String nomeUF) {
			this.nomeUF = nomeUF;
		}

		@Override
		public String toString() {
			return "UnidadeFederacao [nomeUF=" + nomeUF + "]";
		}
		
	}
