package entity;

	public class Logradouro extends Entity{
	
		private String nomeLogradouro;
		private TipoLogradouro tipoLogradouro;

		public Logradouro() {
			super("idLogradouro");
			tipoLogradouro = new TipoLogradouro();
		}
		
		public Logradouro(long idLogradouro) {
			super("idLogradouro",idLogradouro);
			tipoLogradouro = new TipoLogradouro();
		}
		
		public Logradouro(String nomeLogradouro) {
			super("idLogradouro");
			this.nomeLogradouro = nomeLogradouro;
		}
		
		public Logradouro(String nomeLogradouro, TipoLogradouro tipoLogradouro) {
			super("idLogradouro");
			this.nomeLogradouro = nomeLogradouro;
			this.tipoLogradouro = tipoLogradouro;
		}
		
		public Logradouro(long idLogradouro, String nomeLogradouro, TipoLogradouro tipoLogradouro) {
			super("idLogradouro",idLogradouro);
			this.nomeLogradouro = nomeLogradouro;
			this.tipoLogradouro = tipoLogradouro;
		}

		public long getIdLogradouro() {
			return (long) super.getIdentifier();
		}
		
		public void setIdLogradouro(long idLogradouro) {
			super.setIdentifier(idLogradouro);
		}
		
		public String getNomeLogradouro() {
			return nomeLogradouro;
		}

		public void setNomeLogradouro(String nomeLogradouro) {
			this.nomeLogradouro = nomeLogradouro;
		}

		public TipoLogradouro getTipoLogradouro() {
			return tipoLogradouro;
		}

		public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
			this.tipoLogradouro = tipoLogradouro;
		}
		
	}
