package entity;

	public class Medico_EspecialidadeMedica extends Entity{
		
		private long idMedico;
		private long idEspecialidadeMedica;
		
		public Medico_EspecialidadeMedica() {
			super();
		}
		
		public Medico_EspecialidadeMedica(long idMedico, long idEspecialidadeMedica) {
			super();
			this.idMedico = idMedico;
			this.idEspecialidadeMedica = idEspecialidadeMedica;
		}

		public long getIdMedico() {
			return idMedico;
		}

		public void setIdMedico(long idMedico) {
			this.idMedico = idMedico;
		}

		public long getIdEspecialidadeMedica() {
			return idEspecialidadeMedica;
		}

		public void setIdEspecialidadeMedica(long idEspecialidadeMedica) {
			this.idEspecialidadeMedica = idEspecialidadeMedica;
		}
		
	}
