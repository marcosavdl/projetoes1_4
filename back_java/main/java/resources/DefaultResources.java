package resources;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jakarta.ws.rs.Path;

	@Path("/")
	public class DefaultResources {

		private Gson gson;
		
		public DefaultResources() {
			gson = new GsonBuilder().setLenient().create();
		}
		
		/*
		@GET
		@Produces (MediaType.TEXT_PLAIN)
		public String sayPlainTextHello() {
			return "Hi to the Server";
		}
		*/
		
		public String entityToString(Object object) {

			return gson.toJson(object);
		}
		
	}
