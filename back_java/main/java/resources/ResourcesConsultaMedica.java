package resources;

import dao.DaoConsultaMedica;
import entity.ConsultaMedica;
import entity.Medico;
import entity.Paciente;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

	@Path("/appointment")
	public class ResourcesConsultaMedica extends DefaultResources{

		private DaoConsultaMedica dao = new DaoConsultaMedica();
		
		public ResourcesConsultaMedica() {
			super();
		}
		
		@GET
		@Path("/getMedicHistory/{medicID}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterHistoricoConsultaMedica_porMedico(@PathParam("medicID") long idMedico) {
		
			
			return "";
		}
		
		@GET
		@Path("/getPatientHistory/{patientID}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterHistoricoConsultaMedica_porPaciente(@PathParam("patientID") long idPaciente) {
		
			return "";
		}
		
		@GET
		@Path("/getAll")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaConsultaMedica() {
			
			return "";
		}
		
		@GET
		@Path("/quantity")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterQuantidadeConsultaMedica() {
			
			return "";
		}
		
		@POST
		@Path("/insert")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String cadastrarConsultaMedica(ConsultaMedica consultaMedica) throws Exception{
			
			consultaMedica = new ConsultaMedica(10,"10/12/2000",new Medico(1L), new Paciente(2L));
			return this.entityToString(dao.registerMedicAppointment(consultaMedica));
		}
		
	}
