package resources;

import java.util.Arrays;

import org.apache.catalina.filters.CorsFilter;
import org.glassfish.jersey.client.ClientResponse;
import org.glassfish.jersey.client.JerseyClient;
import org.glassfish.jersey.client.JerseyClientBuilder;

import dao.DaoPaciente;
import entity.Paciente;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import okhttp3.OkHttpClient;

	@Path("/patient")
	public class ResourcesPaciente extends DefaultResources{
	
		private DaoPaciente dao = new DaoPaciente();
		
		public ResourcesPaciente() {
			super();
		}
		
		@GET
		@Path("/get/{patientID}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterPaciente(@PathParam("patientID") long id) throws Exception {
		
			return this.entityToString(dao.getPatientByID(id));
		}
		
		@GET
		@Path("/getAll")
		@Produces (MediaType.APPLICATION_JSON)
		public Response obterListaPaciente() throws Exception{
			
			Response r = Response.ok(entityToString(dao.getPatientList()))
					.header("Access-Control-Allow-Origin","http://localhost:8080/rest/patient/getAll")	
					.header("Access-Control-Allow-Methods","GET,POST,PUT,DELETE,OPTIONS")
					.header("Access-Control-Allow-Headers", "*")
					.header("Access-Control-Max-Age", "86400")
					.build();
			System.out.println(r.getHeaders());
			return r;
			/*
			return Response.ok(entityToString(dao.getPatientList()))
					.header("Access-Control-Allow-Origin","*")	
					.header("Access-Control-Allow-Methods","GET,POST,PUT,DELETE,OPTIONS")
					.header("Access-Control-Allow-Headers", "*")
					.header("Access-Control-Max-Age", "86400")
					.build();
//			return this.entityToString(dao.getPatientList());
			*/
		}
		
		@GET
		@Path("/quantity")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterQuantidadePaciente() throws Exception{
			
			return this.entityToString(dao.getPatientQuantity());
		}
		
		@POST
		@Path("/insert")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String cadastrarPaciente(Paciente paciente) throws Exception{
			
			return this.entityToString(dao.registerPatient(paciente));
		}
		
		@POST
		@Path("/getID")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String getID(Paciente p) throws Exception{
			
			return this.entityToString(dao.getIDByPatient(p));
		}
	}
