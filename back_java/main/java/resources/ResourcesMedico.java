package resources;

import dao.DaoMedico;
import entity.Medico;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

	@Path("/medic")
	public class ResourcesMedico extends DefaultResources {

		private DaoMedico dao = new DaoMedico();
		
		public ResourcesMedico() {
			super();
		}
		
		@GET
		@Path("/get/{medicID}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterMedico(@PathParam("medicID") long idMedico) throws Exception {
			
			return entityToString(dao.getMedicByID(idMedico));
		}
		
		@GET
		@Path("/getAll")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaMedico() throws Exception{
			
			return entityToString(dao.getMedicList());
		}
		
		@GET
		@Path("/quantity")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterQuantidadeMedico() throws Exception{
			
			return entityToString(dao.getMedicQuantity());
		}
		
		@POST
		@Path("/insert")
		@Consumes (MediaType.APPLICATION_JSON)
		public String cadastrarMedico(Medico m) throws Exception{
			
			return entityToString(dao.registerMedic(m));
		}
		
		@POST
		@Path("/insertSpecialityList")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String insertSpeciality(Medico m) throws Exception{
			
			return entityToString(dao.registerMedicSpecialityList(m));
		}
		
	}
