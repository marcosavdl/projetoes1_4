package resources;

import dao.DaoPrescricaoRemedio;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

	@Path("/prescription")
	public class ResourcesPrescricaoRemedio extends DefaultResources{
	
		private DaoPrescricaoRemedio dao = new DaoPrescricaoRemedio();
		
		public ResourcesPrescricaoRemedio() {
			super();
		}
		
		@GET
		@Path("/getAll")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaPrescricaoRemedio() throws Exception{
			return this.entityToString(dao.getRemedyPrescriptionList());
		}
		
		@GET
		@Path("/get/{remedyID}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterPrescricaoRemedio_porID(@PathParam("remedyID") long idPrescricaoRemedio) throws Exception{
			return this.entityToString(dao.getRemedyPrescriptionByID(idPrescricaoRemedio));
		}
		
		@GET
		@Path("/getByAppointment/{appointmentID}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaPrescricaoRemedio_porConsultaMedica(@PathParam("appointmentID") long idConsultaMedica) throws Exception{
			return this.entityToString(dao.getRemedyPrescriptionListByAppopintmentID(idConsultaMedica));
		}
		
		
	}
