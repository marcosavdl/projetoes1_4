package resources;

import dao.DaoEndereco;
import entity.Endereco;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

	@Path("/address")
	public class ResourcesEndereco extends DefaultResources{
	
		private DaoEndereco dao = new DaoEndereco();
		
		public ResourcesEndereco() {
			super();
		}
		
		@GET
		@Path("/getAll")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaEndereco() throws Exception{
			return this.entityToString(dao.getAddressList());
		}
		
		@GET
		@Path("/get/{addressID}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterEndereco_porID(@PathParam("addressID") long idEndereco) throws Exception{
			return this.entityToString(dao.getAddressByID(idEndereco));
		}
		
		@GET
		@Path("/getByPatient/{patientID}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterEnderecoPaciente(@PathParam("patientID") long idPaciente) throws Exception{
			return this.entityToString(dao.getAddressByPatientID(idPaciente));
		}
		
		@GET
		@Path("/getByMedic/{medicID}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterEnderecoMedico(@PathParam("medicID") long idMedico) throws Exception{
			return this.entityToString(dao.getAddressByMedicID(idMedico));
		}
		
		@POST
		@Path("/insert")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String cadastrarEndereco(Endereco endereco) throws Exception{
			return this.entityToString(dao.insertAddress(endereco));
		}
		
		@POST
		@Path("/getInsert")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String getInsert(Endereco endereco) throws Exception{
			return this.entityToString(dao.getAddressIDWithInsert(endereco));
		}
		
	}
