package resources;

import api.PostmonAPI;
import api.ViaCepAPI;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

	@Path("/cep")
	public class TestePreenchimentoCEP {

		@GET
		@Path("/postmon/{cep}")
		@Produces (MediaType.APPLICATION_JSON)
		public String postmon(@PathParam("cep") String cep) {
			
			System.out.println("CEP = " + cep);
			PostmonAPI p = new PostmonAPI(cep);
			p.run();
			return p.getJson(); 
		}
		
		@GET
		@Path("viacep/postalCode/{cep}")
		@Produces (MediaType.APPLICATION_JSON)
		public String viacepByCep(@PathParam("cep") String cep) {
			
			System.out.println("CEP = " + cep);
			ViaCepAPI viacep = new ViaCepAPI(cep);
			viacep.run();
			return viacep.getJson();
		}
		
		@GET
		@Path("viacep/address/{state}/{city}/{place}")
		@Produces (MediaType.APPLICATION_JSON)
		public String viacepByCep(@PathParam("state") String siglaUF, @PathParam("city") String cidade, 
							  @PathParam("place") String lograd) {
			
			System.out.println("Parametros = " + siglaUF + " | " + cidade + " | " + lograd);
			ViaCepAPI viacep = new ViaCepAPI(siglaUF,cidade,lograd);
			viacep.run();		
			return viacep.getJson();
		}
	}
