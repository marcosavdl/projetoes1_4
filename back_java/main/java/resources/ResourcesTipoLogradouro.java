package resources;

import dao.DaoTipoLogradouro;
import entity.TipoLogradouro;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

	@Path("/placeType")
	public class ResourcesTipoLogradouro extends DefaultResources{

		private DaoTipoLogradouro dao = new DaoTipoLogradouro();
	
		@GET
		@Path("/getAll")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaTipoLogradouro() throws Exception{
			return this.entityToString(dao.getPlaceTypeList());
		}
		
		@GET
		@Path("/getByInitials/{initials}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterTipoLogradouro_porSiglaTipo(@PathParam("initials") String siglaTipo) throws Exception{
			return this.entityToString(dao.getPlaceTypeNameByInitials(siglaTipo));
		}
		
		@PUT
		@Path("/insert")
		@Consumes (MediaType.APPLICATION_JSON)
		public String cadastrarTipoLogradouro(TipoLogradouro tipoLograd) throws Exception{
			TipoLogradouro tl = new TipoLogradouro("Av","Avenida");
			return this.entityToString(dao.registerPlaceType(tl));
		}
	}
