package resources;

import dao.DaoUnidadeFederacao;
import entity.UnidadeFederacao;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

	@Path("/state")
	public class ResourcesUnidadeFederacao extends DefaultResources{
	
		private DaoUnidadeFederacao dao = new DaoUnidadeFederacao();
		
		@GET
		@Path("/getAll")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaUnidadeFederacao() throws Exception{
			return this.entityToString(dao.getStateList());
		}

		@GET
		@Path("/getByInitials/{uf}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterUnidadeFederacao_porSiglaUF(@PathParam("uf") String siglaUF) throws Exception{
			return this.entityToString(dao.getUFByInitials(siglaUF));
		}
		
		@POST
		@Path("/insert")
		@Consumes (MediaType.APPLICATION_JSON)
		public String cadastrarUnidadeFederacao(UnidadeFederacao uf) throws Exception{
			return this.entityToString(dao.registerUF(uf));
		}
		
		@POST
		@Path("/insertGet")
		@Consumes (MediaType.APPLICATION_JSON)
		public String insertAndGet(UnidadeFederacao uf) throws Exception{
			return this.entityToString(dao.getInitialsWithInsert(uf));
		}
	}
