package util;

import java.sql.Date;
import java.util.List;

public class Formatter {
	
		public static String formatSQLString(String str) {
			return "'" + str + "'";
		}
		
		public static String convertRealToSQL_DateTime(String realDateTime) {
			return "";
		}
		
		public static String convertSQLToReal_DateTime(String sqlDateTime) {
			return "";
		}
		
		public static Object formatSQLValue(Object obj) {
			try {
				if(obj.getClass() == String.class || obj.getClass() == Date.class)	return formatSQLString((String) obj);
				return obj;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}
