package util;

import java.io.IOException;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
	
	public class CustomCORSFilter implements Filter {
	
		public static String VALID_METHODS = "DELETE, HEAD, GET, OPTIONS, POST, PUT";
		
	    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
	        
	    	HttpServletResponse response = (HttpServletResponse) res;
	        /*
	    	HttpServletRequest request = (HttpServletRequest) req;
	        String origin = request.getHeader("Origin");
	        if(origin == null) {
	        	if("OPTIONS".equalsIgnoreCase(request.getMethod())) {
	        		response.setHeader("Allow", VALID_METHODS);
	        		response.setStatus(200);
	        		return;
	        	}
	        }else {
	        	response.setHeader("Access-Control-Allow-Origin", origin);
	        	response.setHeader("Access-Control-Allow-Methods", VALID_METHODS);
	        	String headers = request.getHeader("Access-Control-Request-Headers");
	        	if(headers != null) {
	        		response.setHeader("Access-Control-Allow-Headers", headers);
	        	}
	        	response.setHeader("Access-Control-Max-Age", "3600");
	        	if(!"OPTIONS".equalsIgnoreCase(request.getMethod())) {
	        		chain.doFilter(request, response);
	        	}
	        }
	        */    
	        response.setHeader("Access-Control-Allow-Origin", "*");
	        response.setHeader("Access-Control-Allow-Methods", "*");
	        response.setHeader("Access-Control-Max-Age", "86400");
	        response.setHeader("Access-Control-Allow-Headers", "*");
	     
	        chain.doFilter(req, response);
	    }
	
	    public void destroy() {
	        // TODO Auto-generated method stub
	
	    }
	
	    public void init(FilterConfig arg0) throws ServletException {
	        // TODO Auto-generated method stub
	
	    }
	}