package api;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

	public class ViaCepAPI implements Runnable{

		private String initialUrl = "https://viacep.com.br/ws/";
		private String json;
		private String cep, siglaUF, cidade, lograd;

		public ViaCepAPI() {
			super();
		}
		
		public ViaCepAPI(String cep) {
			super();
			this.cep = cep;
			siglaUF = cidade = lograd = null;
		}
		
		public ViaCepAPI(String siglaUF, String cidade, String lograd) {
			super();
			this.siglaUF = siglaUF;
			this.cidade = cidade;
			this.lograd = lograd;
			cep = null;
		}
		
		public String getJson() {
			return json;
		}
		
		public void setJson(String json) {
			this.json = json;
		}
		
		public String requestURLByCep() {
			return initialUrl + cep + "/json";
		}
	
		public String requestURLWithoutCep() {
			return initialUrl + 
				   siglaUF + "/" +
				   cidade + "/" +
				   lograd + "/json";
		}
		
		@Override
		public void run() {	
			// TODO Auto-generated method stub	
			try {
	    		Thread t = new Thread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						Gson gson = new GsonBuilder().setPrettyPrinting().create();
			            URL url = null;
						try {
							if(cep == null)		url = new URL(requestURLWithoutCep());		
							else				url = new URL(requestURLByCep());
						} catch (MalformedURLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
			            OkHttpClient client = new OkHttpClient();
			            Request request = new Request.Builder().url(url).build();
			            try (Response response = client.newCall(request).execute()) {
			                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
			               // System.out.println(response.body().string() + " ==> " + response.code());
			                //return response.body().string() + " ==> " + response.code();
			                assert response.body() != null;
			                json = gson.toJson(response.body().string());
			            }catch(Exception e) {
			            	e.printStackTrace();
			            	json = null;
			            	// ErrorCodes != 400
			            }
					}
	    			
	    		});
	    		t.start();
	    		t.join();
	        } catch (Exception e) {
	            e.printStackTrace();
	            json = null;
	        }
		}
		
	}
