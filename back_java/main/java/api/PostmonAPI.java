package api;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

	public class PostmonAPI implements Runnable{
	
		private final String url = "https://api.postmon.com.br/v1/cep/";
		private String cep;
	    private String json;
	    
	    public PostmonAPI() {
	    	super();
	    }
	    
	    public PostmonAPI(String cep) {
	    	super();
	    	this.cep = cep;
	    }
	    
	    public String getUrl() {
			return url;
		}

		public String getCep() {
			return cep;
		}

		public void setCep(String cep) {
			this.cep = cep;
		}
	
	    public String getJson() {
			return json;
		}

		public void setJson(String json) {
			this.json = json;
		}
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
	    	try {
	    		Thread t = new Thread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						Gson gson = new GsonBuilder().setPrettyPrinting().create();
			            URL url = null;
						try {
							url = new URL(getUrl() + getCep());
						} catch (MalformedURLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
			            OkHttpClient client = new OkHttpClient();
			            Request request = new Request.Builder().url(url).build();
			            try (Response response = client.newCall(request).execute()) {
			                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
			               // System.out.println(response.body().string() + " ==> " + response.code());
			                //return response.body().string() + " ==> " + response.code();
			                assert response.body() != null;
			                json = gson.toJson(response.body().string());
			            }catch(Exception e) {
			            	e.printStackTrace();
			            	json = null;
			            	// ErrorCodes != 400
			            }
					}
	    			
	    		});
	    		t.start();
	    		t.join();
	        } catch (Exception e) {
	            e.printStackTrace();
	            json = null;
	        }
		}

	}

