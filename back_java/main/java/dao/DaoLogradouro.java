package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import entity.Logradouro;
import util.SQLBuilder;

public class DaoLogradouro {

		public DaoLogradouro() {
			super();
		}
		
		public long getIDByPlace(Logradouro lograd) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(lograd)
					.joinOn(lograd, lograd.getTipoLogradouro(), lograd.getTipoLogradouro().getIdentifierName()) 
					.where("nomeLogradouro").equal(lograd.getNomeLogradouro())
					.AND(lograd.getTipoLogradouro().getIdentifierName()).equal(lograd.getTipoLogradouro().getIdentifier())
					.AND("nomeTipo").equal(lograd.getTipoLogradouro().getNomeTipo());
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(!rs.first())		return -1;
					return rs.getLong(lograd.getIdentifierName());
				}
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public Logradouro getPlaceByID(long idLogradouro) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				Logradouro l = new Logradouro();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(l)
					 .where(l,l.getIdentifierName())
					 .equal(idLogradouro);
				return null;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public Logradouro getPlaceByName(String nomeLogradouro) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				Logradouro l = new Logradouro();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(l)
					 .where(l,"nomeLogradouro")
					 .equal(nomeLogradouro);
				return null;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<Logradouro> getPlaceList() throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				Logradouro l = new Logradouro();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(l);
				List<Logradouro> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						list.add(new Logradouro());
					}
				}
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public long getPlaceIDWithInsert(Logradouro lograd) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(lograd)
					 .where("nomeLogradouro")
					 .equal(lograd.getNomeLogradouro())
					 .AND(lograd.getTipoLogradouro().getIdentifierName())
					 .equal(lograd.getTipoLogradouro().getSiglaTipoLogradouro());
				System.out.println(query.getSQL());
				long idLogradouro = -1;
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						idLogradouro = rs.getLong(lograd.getIdentifierName());
					}else {
						query.endStatement();
						query.insert(lograd);
						try(Statement insertStmt = conn.createStatement()){
							insertStmt.executeUpdate(query.getSQL(),Statement.RETURN_GENERATED_KEYS);
							ResultSet rs1 = insertStmt.getGeneratedKeys();
							if(rs1.next()) {
								idLogradouro = rs1.getLong(1);
							}	
						}
					}
				}
				return idLogradouro;	 
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public boolean registerPlace(Logradouro l) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder();
				query.insert(l);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					DaoTipoLogradouro daoTipo = new DaoTipoLogradouro();
					if(daoTipo.getInitialsByPlaceType(l.getTipoLogradouro()) == null) {
						
					}
					return true;
				}
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
	}
