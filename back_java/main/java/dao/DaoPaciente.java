package dao;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import entity.Bairro;
import entity.Cidade;
import entity.Endereco;
import entity.Logradouro;
import entity.Paciente;
import entity.TipoLogradouro;
import entity.UnidadeFederacao;
import util.Formatter;
import util.SQLBuilder;

	public class DaoPaciente {

		public DaoPaciente() {
			super();
		}
		
		public long getIDByPatient(Paciente p) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				DaoEndereco daoEnd = new DaoEndereco();
				long idEndereco = daoEnd.getIDByAddress(p.getEndereco());
				if(idEndereco != -1) {
					p.getEndereco().setIdentifier(idEndereco);
					SQLBuilder query = new SQLBuilder(conn);
					query.select("*").from(p)
						 .whereAllAND(this.getAllFieldsConditionFromPatient(p));
					System.out.println(query.getSQL());
					try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
						ResultSet rs = stmt.executeQuery();
						if(rs.next()) {
							return rs.getLong(1);
						}
					}
					/*
					try (Statement stmt = conn.createStatement()){
						stmt.execute(query.getSQL(),Statement.RETURN_GENERATED_KEYS);
						ResultSet rs = stmt.getGeneratedKeys();
						if(rs.next()) {
							return rs.getLong(1);
						}
					}
					*/
				}
				return -1;
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public Paciente getPatientByID(long idPaciente) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				Paciente p = new Paciente();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(p)
					 .joinOn(p, p.getEndereco(), p.getEndereco().getIdentifierName())
					 .joinOn(p.getEndereco(), p.getEndereco().getBairro(), 
							 p.getEndereco().getBairro().getIdentifierName())
					 .joinOn(p.getEndereco(), p.getEndereco().getCidade(), 
							 p.getEndereco().getCidade().getIdentifierName())
					 .joinOn(p.getEndereco().getCidade(), p.getEndereco().getCidade().getUf(),
							 p.getEndereco().getCidade().getUf().getIdentifierName())
					 .joinOn(p.getEndereco(), p.getEndereco().getLogradouro(), 
							 p.getEndereco().getLogradouro().getIdentifierName())
					 .joinOn(p.getEndereco().getLogradouro(), p.getEndereco().getLogradouro().getTipoLogradouro(), 
							 p.getEndereco().getLogradouro().getTipoLogradouro().getIdentifierName())
					 .where(p,p.getIdentifierName()).equal(idPaciente);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						p = new Paciente(rs.getString("cpf"),rs.getString("primeiroNome"),rs.getString("sobrenome"),rs.getDate("dataNascimento").toLocalDate().toString(),
										 rs.getString("tipoSanguineo"), rs.getString("fatorRh"), rs.getString("sexo"), rs.getInt("nroLogradouro"), rs.getString("complementoLogradouro"),
										 new Endereco(new Bairro(rs.getString("nomeBairro")), 
												      new Cidade(rs.getString("nomeCidade"), 
												    		     new UnidadeFederacao(rs.getString("nomeUF"))),
												      new Logradouro(rs.getString("nomeLogradouro"),
												    		  	     new TipoLogradouro(rs.getString("nomeTipo"))),
												      rs.getString("cep")));
						if(p.equals(new Paciente()))	return null;
						return p;
					}
				}
				return null;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<Paciente> getPatientList() throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				Paciente p = new Paciente();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(p)
				 .joinOn(p, p.getEndereco(), p.getEndereco().getIdentifierName())
				 .joinOn(p.getEndereco(), p.getEndereco().getBairro(), 
						 p.getEndereco().getBairro().getIdentifierName())
				 .joinOn(p.getEndereco(), p.getEndereco().getCidade(), 
						 p.getEndereco().getCidade().getIdentifierName())
				 .joinOn(p.getEndereco().getCidade(), p.getEndereco().getCidade().getUf(),
						 p.getEndereco().getCidade().getUf().getIdentifierName())
				 .joinOn(p.getEndereco(), p.getEndereco().getLogradouro(), 
						 p.getEndereco().getLogradouro().getIdentifierName())
				 .joinOn(p.getEndereco().getLogradouro(), p.getEndereco().getLogradouro().getTipoLogradouro(), 
						 p.getEndereco().getLogradouro().getTipoLogradouro().getIdentifierName());
				List<Paciente> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					System.out.println(stmt);
					while(rs.next()) {
						list.add(new Paciente(rs.getString("cpf"),rs.getString("primeiroNome"),rs.getString("sobrenome"),rs.getDate("dataNascimento").toLocalDate().toString(),
								 rs.getString("tipoSanguineo"), rs.getString("fatorRh"), rs.getString("sexo"), 
								 rs.getInt("nroLogradouro"), rs.getString("complementoLogradouro"),
								 new Endereco(new Bairro(rs.getString("nomeBairro")), 
										      new Cidade(rs.getString("nomeCidade"), 
										    		     new UnidadeFederacao(rs.getString("siglaUF"), rs.getString("nomeUF"))),
										      new Logradouro(rs.getString("nomeLogradouro"),
										    		  	     new TipoLogradouro(rs.getString("nomeTipo"))),
										      rs.getString("cep"))));
					}
				}
				conn.close();
				if(list.isEmpty())		return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		/*
		public Paciente generatePatientChartByID(long idPaciente) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				Paciente p = new Paciente();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(p)
					 .joinOn(p, p.getEndereco(), p.getEndereco().getIdentifierName())
					 .joinOn(p.getEndereco(), p.getEndereco().getBairro(), 
							 p.getEndereco().getBairro().getIdentifierName())
					 .joinOn(p.getEndereco(), p.getEndereco().getCidade(), 
							 p.getEndereco().getCidade().getIdentifierName())
					 .joinOn(p.getEndereco().getCidade(), p.getEndereco().getCidade().getUf(),
							 p.getEndereco().getCidade().getUf().getIdentifierName())
					 .joinOn(p.getEndereco(), p.getEndereco().getLogradouro(), 
							 p.getEndereco().getLogradouro().getIdentifierName())
					 .joinOn(p.getEndereco().getLogradouro(), p.getEndereco().getLogradouro().getTipoLogradouro(), 
							 p.getEndereco().getLogradouro().getTipoLogradouro().getIdentifierName())
					 .where(p,p.getIdentifierName()).equal(idPaciente);
					// Pegar informações de médico, consulta médica e prescrição de remédio
				try (PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					ResultSet rs = stmt.executeQuery();
					rs.next();
//					p = new Paciente();
				}
				if(p.equals(new Paciente()))	return null;
				return p;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		*/
		
		public long getPatientQuantity() throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder(conn);
				Paciente p = new Paciente();
				query.selectCount(p.getIdentifierName()).from(p);
				long count = -1;
				try (PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					ResultSet rs = stmt.executeQuery();
					rs.next();
					count = rs.getLong("COUNT(" + p.getIdentifierName() + ")");
				}
				conn.close();
				return count;
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public boolean registerPatient(Paciente p) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				DaoEndereco daoEndereco = new DaoEndereco();
				long idEndereco = daoEndereco.getAddressIDWithInsert(p.getEndereco());
				p.getEndereco().setIdEndereco(idEndereco);
				SQLBuilder query = new SQLBuilder(conn);
				query.insert(p);
				try(Statement stmt = conn.createStatement()){
					stmt.execute(query.getSQL(),Statement.RETURN_GENERATED_KEYS);
					ResultSet rs = stmt.getGeneratedKeys();
					if(rs.next()) {
						p.setIdentifier(rs.getLong(1));
					}else {
						return false;
					}
				}
				return true;
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
		public ArrayList<String> getAllFieldsConditionFromPatient(Paciente p){
			
			ArrayList<String> list = new ArrayList<>();
			try {
				for(Field f : p.getClass().getDeclaredFields()) {
					f.setAccessible(true);
					if(f.get(p) instanceof Endereco) {
						Endereco e = (Endereco) f.get(p);
						list.add(e.getIdentifierName() + " = " + Formatter.formatSQLValue(e.getIdEndereco()));
					}else{
						list.add(f.getName() + " = " + Formatter.formatSQLValue(f.get(p)));
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			/*
			list.add("cpf = " + Formatter.formatSQLString(p.getCpf()));
			list.add("primeiroNome = " + Formatter.formatSQLString(p.getPrimeiroNome()));
			list.add("sobrenome = " + Formatter.formatSQLString(p.getSobrenome()));
			list.add("dataNascimento = " + Formatter.formatSQLString(p.getDataNascimento()));
			list.add("tipoDanguineo = " + Formatter.formatSQLString(p.getTipoSanguineo()));
			list.add("fatorRh = " + Formatter.formatSQLString(p.getFatorRh()));
			list.add("sexo = " + Formatter.formatSQLString(p.getSexo()));
			list.add("nroLogradouro = " + p.getNroLogradouro());
			if(p.getComplementoLogradouro() != null)	list.add("complementoLogradouro = " + Formatter.formatSQLValue(list)))
			*/
			return list;
		}
	}
