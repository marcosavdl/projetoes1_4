package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import connection.ConnectionFactory;
import entity.ConsultaMedica;
import entity.Medico;
import entity.Paciente;
import util.SQLBuilder;

	public class DaoConsultaMedica {

		public DaoConsultaMedica() {
			super();
		}
		
		public ConsultaMedica getMedicAppointmentByID(long idConsultaMedica) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				ConsultaMedica cm = new ConsultaMedica();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(cm)
					 .where(cm.getIdentifierName() + " = " + idConsultaMedica);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					System.out.println("Row Quantity = " + stmt.getMaxRows());
					
				}
				return null;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<ConsultaMedica> getMedicAppointmentList() throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				ConsultaMedica cm = new ConsultaMedica();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(cm);
				return null;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<ConsultaMedica> getMedicAppointmentListByPatientID(long idPaciente) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				ConsultaMedica cm = new ConsultaMedica();
				Paciente p = new Paciente();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(cm)
					 .joinOn(cm, p, p.getIdentifierName())
					 .where(p.getIdentifierName() + " = " + idPaciente);
				
				return null;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<ConsultaMedica> getMedicAppointmentListByMedicID(long idMedico) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				ConsultaMedica cm = new ConsultaMedica();
				Medico m = new Medico();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(cm)
					 .joinOn(cm, m, m.getIdentifierName())
					 .where(m,m.getIdentifierName())
					 .equal(idMedico);
				return null;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public long getMedicAppointmentQuantity() throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder(conn);
				ConsultaMedica cm = new ConsultaMedica();
				query.selectCount(cm.getIdentifierName())
					 .from(cm);
				long count = -1;
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					rs.next();
					count = rs.getLong("COUNT(" + cm.getIdentifierName() + ")");
				}
				return count;
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public boolean registerMedicAppointment(ConsultaMedica cm) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				DaoPaciente daoPaciente = new DaoPaciente();
				DaoMedico daoMedico = new DaoMedico();	
				/*
				 * 
				 */
				SQLBuilder query = new SQLBuilder(conn);
				query.insert(cm);
				System.out.println(query.getSQL());
				try(Statement stmt = conn.createStatement()){
					stmt.execute(query.getSQL(),Statement.RETURN_GENERATED_KEYS);
					ResultSet rs = stmt.getGeneratedKeys();
					if(rs.next()) {
						cm.setIdConsultaMedica(rs.getLong(1));
						System.out.println(cm.getIdConsultaMedica());
						return true;
					}
				}
				return false;
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
	}
