package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import entity.Cidade;
import entity.UnidadeFederacao;
import util.SQLBuilder;

	public class DaoCidade {

		public DaoCidade() {
			super();
		}
		
		public long getIDByCity(Cidade cidade) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(cidade)
					 .joinOn(cidade, cidade.getUf(), cidade.getUf().getIdentifierName())
					 .where("Cidade.nomeCidade").equal(cidade.getNomeCidade())
					 .AND("UnidadeFederacao." + cidade.getUf().getIdentifierName()).equal(cidade.getUf().getIdentifier());
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						return rs.getLong(cidade.getIdentifierName());
					}else {
						return -1;
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public Cidade getCityByID(long idCidade) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				Cidade c = new Cidade();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(c)
					 .joinOn(c, c.getUf(),c.getUf().getIdentifierName())
					 .where(c,c.getIdentifierName())
					 .equal(idCidade);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					rs.next();
					c = new Cidade(rs.getString("nomeCidade"), 
							new UnidadeFederacao(rs.getString("siglaUF"),rs.getString("nomeUF")));
				}
				conn.close();
				if(c.equals(new Cidade()))		return null;
				return c;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public Cidade getCityByName(String nomeCidade) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				Cidade c = new Cidade();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(c)
					 .joinOn(c, c.getUf(),c.getUf().getIdentifierName())
					 .where(c,"nomeCidade")
					 .equal(nomeCidade);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					rs.next();
					c = new Cidade(rs.getString("nomeCidade"), 
							new UnidadeFederacao(rs.getString("siglaUF"),rs.getString("nomeUF")));
				}
				conn.close();
				if(c.equals(new Cidade()))		return null;
				return c;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<String> getCityList() throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				Cidade c = new Cidade();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("nomeCidade").from(c);
				List<String> list = new ArrayList<>();
				try (PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						list.add(rs.getString("nomeCidade"));
					}
				}
				conn.close();
				if(list.isEmpty())		return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<Cidade> getCityListByUF(UnidadeFederacao uf) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				Cidade c = new Cidade();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(c)
					 .joinOn(c, uf, uf.getIdentifierName())
					 .where(uf,uf.getIdentifierName())
					 .equal(uf.getSiglaUF());
				List<Cidade> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						list.add(new Cidade(rs.getString("nomeCidade"),
								 new UnidadeFederacao(rs.getString(c.getUf().getIdentifierName()),
										 			  rs.getString("nomeUF"))));
					}
				}
				conn.close();
				if(list.isEmpty())	return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public long getCityIDWithInsert(Cidade c) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				DaoUnidadeFederacao daoUf = new DaoUnidadeFederacao();
				String siglaUf = daoUf.getInitialsWithInsert(c.getUf());
				
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(c)
					 .where("nomeCidade").equal(c.getNomeCidade())
					 .AND("siglaUF").equal(siglaUf);
				long idCidade = -1;
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						idCidade = rs.getLong(c.getIdentifierName());
					}else {
						query.endStatement();
						query.insert(c);
						try(Statement insertStmt = conn.createStatement()){
							insertStmt.executeUpdate(query.getSQL(),Statement.RETURN_GENERATED_KEYS);
							ResultSet rs1 = insertStmt.getGeneratedKeys();
							if(rs1.next()) {
								idCidade = rs1.getLong(1);
							}
						}
					}
				}
				return idCidade;
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public boolean registerCity(Cidade cidade) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder();
				query.insert(cidade);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					DaoUnidadeFederacao daoUf = new DaoUnidadeFederacao();
					if(daoUf.getInitialsByUf(cidade.getUf()) == null)	daoUf.registerUF(cidade.getUf());
					if(this.getIDByCity(cidade) == -1) {
						return stmt.execute();
					}
					return true;
				}
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
	}
