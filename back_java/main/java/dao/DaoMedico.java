package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import connection.ConnectionFactory;
import entity.Bairro;
import entity.Cidade;
import entity.Endereco;
import entity.EspecialidadeMedica;
import entity.Logradouro;
import entity.Medico;
import entity.Medico_EspecialidadeMedica;
import entity.TipoLogradouro;
import entity.UnidadeFederacao;
import util.SQLBuilder;

	public class DaoMedico {

		public DaoMedico() {
			super();
		}
		
		public Medico getMedicByID(long idMedico) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			
			System.out.println(conn);
			try {
				Medico m = new Medico();
				EspecialidadeMedica em = new EspecialidadeMedica();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(m)
					 .joinOn(m, new Medico_EspecialidadeMedica(), m.getIdentifierName())
					 .joinOn(new Medico_EspecialidadeMedica(), em, em.getIdentifierName())	
					 .joinOn(m, m.getEndereco(), m.getEndereco().getIdentifierName())
					 .joinOn(m.getEndereco(), m.getEndereco().getBairro(), 
							 m.getEndereco().getBairro().getIdentifierName())
					 .joinOn(m.getEndereco(), m.getEndereco().getCidade(), 
							 m.getEndereco().getCidade().getIdentifierName())
					 .joinOn(m.getEndereco().getCidade(), m.getEndereco().getCidade().getUf(),
							 m.getEndereco().getCidade().getUf().getIdentifierName())
					 .joinOn(m.getEndereco(), m.getEndereco().getLogradouro(), 
							 m.getEndereco().getLogradouro().getIdentifierName())
					 .joinOn(m.getEndereco().getLogradouro(), m.getEndereco().getLogradouro().getTipoLogradouro(), 
							 m.getEndereco().getLogradouro().getTipoLogradouro().getIdentifierName())
					 .where(m,m.getIdentifierName()).equal(idMedico);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						m = new Medico(rs.getString("crm"), rs.getString("primeiroNome"), rs.getString("sobrenome"),
								rs.getDate("dataNascimento").toLocalDate().toString(), rs.getString("tipoSanguineo"), rs.getString("fatorRh"),
								rs.getString("sexo"), rs.getInt("nroLogradouro"), rs.getString("complementoLogradouro"),
								new Endereco(new Bairro(rs.getString("nomeBairro")), 
											 new Cidade(rs.getString("nomeCidade"),
													    new UnidadeFederacao(rs.getString("siglaUF"),rs.getString("nomeUF"))), 
											 new Logradouro(rs.getString("nomeLogradouro"), 
													 	    new TipoLogradouro(rs.getString("siglaTipoLogradouro"), rs.getString("nomeTipo"))), 
											 rs.getString("cep")), 
								new UnidadeFederacao(rs.getString("crmUF"),""));
						List<EspecialidadeMedica> listEsp = new ArrayList<>();
						listEsp.add(new EspecialidadeMedica(rs.getString("tipoEspecialidade")));
						while(rs.next()) {
							listEsp.add(new EspecialidadeMedica(rs.getString("tipoEspecialidade")));
						}
						m.setListaEspecialidade(listEsp);
					}
				}
				conn.close();
				if(m.equals(new Medico()))	return null;
				return m;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<Medico> getMedicList() throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				Medico m = new Medico();
				EspecialidadeMedica em = new EspecialidadeMedica();
				SQLBuilder query = new SQLBuilder();
//				query.select("*").from(m);
				query.select("*").from(m)
				 .joinOn(m, new Medico_EspecialidadeMedica(), m.getIdentifierName())
				 .joinOn(new Medico_EspecialidadeMedica(), em, em.getIdentifierName())				 
				 .joinOn(m, m.getEndereco(), m.getEndereco().getIdentifierName())
				 .joinOn(m.getEndereco(), m.getEndereco().getBairro(), 
						 m.getEndereco().getBairro().getIdentifierName())
				 .joinOn(m.getEndereco(), m.getEndereco().getCidade(), 
						 m.getEndereco().getCidade().getIdentifierName())
				 .joinOn(m.getEndereco().getCidade(), m.getEndereco().getCidade().getUf(),
						 m.getEndereco().getCidade().getUf().getIdentifierName())
				 .joinOn(m.getEndereco(), m.getEndereco().getLogradouro(), 
						 m.getEndereco().getLogradouro().getIdentifierName())
				 .joinOn(m.getEndereco().getLogradouro(), m.getEndereco().getLogradouro().getTipoLogradouro(), 
						 m.getEndereco().getLogradouro().getTipoLogradouro().getIdentifierName())
				 .groupBy("EspecialidadeMedica.tipoEspecialidade");
				 ;
				LinkedHashMap<Long,Medico> medicMap = new LinkedHashMap<>();	
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						m = new Medico(rs.getString("crm"), rs.getString("primeiroNome"), rs.getString("sobrenome"),
								rs.getDate("dataNascimento").toLocalDate().toString(), rs.getString("tipoSanguineo"), rs.getString("fatorRh"),
								rs.getString("sexo"), rs.getInt("nroLogradouro"), rs.getString("complementoLogradouro"),
								new Endereco(new Bairro(rs.getString("nomeBairro")), 
											 new Cidade(rs.getString("nomeCidade"),
													 	new UnidadeFederacao(rs.getString("siglaUF"),rs.getString("nomeUF"))), 
											 new Logradouro(rs.getString("nomeLogradouro"), 
													 	    new TipoLogradouro(rs.getString("siglaTipoLogradouro"), rs.getString("nomeTipo"))), 
											 rs.getString("cep")), 
								new UnidadeFederacao(rs.getString("crmUF"),""));
						long idMedico = rs.getLong(m.getIdentifierName());
						EspecialidadeMedica esp = new EspecialidadeMedica(rs.getString("tipoEspecialidade"));
						if(medicMap.containsKey(idMedico)) {
							medicMap.get(idMedico).getListaEspecialidade().add(esp);
						}else {
							m.getListaEspecialidade().add(esp);
							medicMap.put(idMedico,m);
						}
					}
				}
				conn.close();
				if(medicMap.isEmpty())	return null;
				return new ArrayList<>(medicMap.values());
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public long getMedicQuantity() throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder();
				Medico m = new Medico();
				query.selectCount(m.getIdentifierName())
					 .from(m);
				long count = -1; 
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					rs.next();
					count = rs.getLong("COUNT(" + m.getIdentifierName() + ")");
				}
				conn.close();
				return count;
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public boolean registerMedicSpecialityList(Medico m) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			m.setIdentifier(1L);
			try {
				SQLBuilder query = new SQLBuilder(conn);
				DaoEspecialidadeMedica daoEsp = new DaoEspecialidadeMedica();
				for(EspecialidadeMedica em : m.getListaEspecialidade()) {
					long idEspecialidade = daoEsp.getSpecialityIDWithInsert(em);
					System.out.println("ID = " + idEspecialidade);
					if(idEspecialidade != -1) {
						Medico_EspecialidadeMedica mem = new Medico_EspecialidadeMedica((long) m.getIdentifier(),idEspecialidade);
						query.insert(mem);
						System.out.println("Medico_EspecialidadeMedica => " + query.getSQL());
						try(PreparedStatement midEntityStmt = conn.prepareStatement(query.getSQL())){
							midEntityStmt.execute();
						}
						query.endStatement();
					}
				}
				// TODO Testar inserção de especialidade médica considerando um médico válido
				return true;
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
		/*
		// Pré-condição: Medico com ID pré-definido
		public void registerMedicSpecialityList(Medico m, List<EspecialidadeMedica> list) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder selectQuery = new SQLBuilder(conn);
				SQLBuilder insertQuery = new SQLBuilder(conn);
				for(EspecialidadeMedica em : list) {
					long idEspecialidade = -1;
					selectQuery.select("*").from(em)
						 .where("tipoEspecialidade")
						 .equal(em.getTipoEspecialidade());
					try(PreparedStatement stmt = conn.prepareStatement(selectQuery.getSQL())){
						ResultSet rs = stmt.executeQuery();
						if(rs.next()) {
							idEspecialidade = rs.getLong(em.getIdentifierName());
						}else {
							insertQuery.insert(em);
							try(Statement insertStmt = conn.createStatement()){
								insertStmt.executeUpdate(insertQuery.getSQL());
								ResultSet rs1 = insertStmt.getGeneratedKeys();
								if(rs1.next()) {
									idEspecialidade = rs1.getLong(1);
								}
							}
						}
					}
					selectQuery.endStatement();
					insertQuery.endStatement();
					if(idEspecialidade != -1) {
						SQLBuilder midEntityQuery = new SQLBuilder(conn);
						Medico_EspecialidadeMedica mem = new Medico_EspecialidadeMedica((long) m.getIdentifier(),idEspecialidade);
						midEntityQuery.insert(mem);
						try(PreparedStatement midEntityStmt = conn.prepareStatement(midEntityQuery.getSQL())){
							midEntityStmt.execute();
						}
					}
				}
				// TODO Testar inserção de especialidade médica considerando um médico válido
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		*/
		
		public boolean registerMedic(Medico m) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			System.out.println(m.toString());
			try {
				DaoEndereco daoEndereco = new DaoEndereco();
				long idEndereco = daoEndereco.getAddressIDWithInsert(m.getEndereco());
				m.getEndereco().setIdEndereco(idEndereco);
				/*
				DaoUnidadeFederacao daoCrmUf = new DaoUnidadeFederacao();
				UnidadeFederacao crmUf = daoCrmUf.getUFByInitials(m.getCrmUF().getSiglaUF());
				if(crmUf == null) {
					daoCrmUf.getInitialsWithInsert(m.getCrmUF());
				}
				*/
				SQLBuilder query = new SQLBuilder(conn);
				query.insert(m);
				try(Statement stmt = conn.createStatement()){
					System.out.println(query.getSQL());
					stmt.execute(query.getSQL(),Statement.RETURN_GENERATED_KEYS);
					System.out.println(stmt);
					ResultSet rs = stmt.getGeneratedKeys();
					if(rs.next()) {
						m.setIdMedico(rs.getLong(1));
					}
				}
				if(m.getIdentifier() != null) {
					this.registerMedicSpecialityList(m);
					return true;
				}
				return false;
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
	}
