package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import entity.TipoLogradouro;
import util.SQLBuilder;

	public class DaoTipoLogradouro {
		
		public DaoTipoLogradouro() {
			super();
		}
	
		public String getInitialsByPlaceType(TipoLogradouro tipoLograd) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(tipoLograd)
					 .where(tipoLograd.getIdentifierName())
					 .equal(tipoLograd.getIdentifier());
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						return (String) tipoLograd.getIdentifier();
					}else {
						return null;
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public String getPlaceTypeNameByInitials(String sigla) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				TipoLogradouro tl = new TipoLogradouro();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(tl)
					 .where(tl,tl.getIdentifierName())
					 .equal(sigla);
				String result = "";
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					rs.next();
					result = rs.getString("nomeTipo");
				}
				conn.close();
				if(result.isBlank())	return null;
				return result;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<TipoLogradouro> getPlaceTypeList() throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				TipoLogradouro tl = new TipoLogradouro();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(tl);
				List<TipoLogradouro> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						list.add(new TipoLogradouro(rs.getString(tl.getIdentifierName()),
													rs.getString("nomeTipo")));
					}
				}
				conn.close();
				if(list.isEmpty())	return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public boolean registerPlaceType(TipoLogradouro tipoLograd) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder();
				query.insert(tipoLograd);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					if(this.getInitialsByPlaceType(tipoLograd) == null)	return stmt.execute();
					return true;
				}
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
	}
