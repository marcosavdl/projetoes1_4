package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import entity.Bairro;
import entity.Cidade;
import entity.Endereco;
import entity.Logradouro;
import entity.Medico;
import entity.Paciente;
import entity.TipoLogradouro;
import entity.UnidadeFederacao;
import util.Formatter;
import util.SQLBuilder;

	public class DaoEndereco {

		public DaoEndereco() {
			super();
		}
		
		public long getIDByAddress(Endereco endereco) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder();
				ArrayList<String> andCondition = new ArrayList<>();
				andCondition.add("cep = " + Formatter.formatSQLString(endereco.getCep()));
				andCondition.add("nomeBairro = " + Formatter.formatSQLString(endereco.getBairro().getNomeBairro()));
				andCondition.add("nomeCidade = " + Formatter.formatSQLString(endereco.getCidade().getNomeCidade()));
				andCondition.add("nomeLogradouro = " + Formatter.formatSQLString(endereco.getLogradouro().getNomeLogradouro()));
				// Condições de UF e TipoLogradouro
				query.select("*").from(endereco)
					.joinOn(endereco, endereco.getBairro(), endereco.getBairro().getIdentifierName())
					.joinOn(endereco, endereco.getCidade(), endereco.getCidade().getIdentifierName())
					.joinOn(endereco, endereco.getLogradouro(), endereco.getLogradouro().getIdentifierName())
					.joinOn(endereco.getCidade(), endereco.getCidade().getUf(), 
							endereco.getCidade().getUf().getIdentifierName())
					.joinOn(endereco.getLogradouro(), endereco.getLogradouro().getTipoLogradouro(), 
							endereco.getLogradouro().getTipoLogradouro().getIdentifierName())
					.whereAllAND(andCondition);
				try(Statement stmt = conn.createStatement()){
					stmt.execute(query.getSQL(),Statement.RETURN_GENERATED_KEYS);
					ResultSet rs = stmt.getGeneratedKeys();
					if(rs.next()) {
						rs.getLong(endereco.getIdentifierName());
					}
					return -1;
				}
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public Endereco getAddressByID(long idEndereco) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				Endereco e = new Endereco();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(e)
 					 .joinOn(e, e.getBairro(), 
							 e.getBairro().getIdentifierName())
					 .joinOn(e, e.getCidade(), 
							 e.getCidade().getIdentifierName())
					 .joinOn(e.getCidade(), e.getCidade().getUf(),
							 e.getCidade().getUf().getIdentifierName())
					 .joinOn(e, e.getLogradouro(), 
							 e.getLogradouro().getIdentifierName())
					 .joinOn(e.getLogradouro(), e.getLogradouro().getTipoLogradouro(), 
							 e.getLogradouro().getTipoLogradouro().getIdentifierName())
					 .where(e,e.getIdentifierName()).equal(idEndereco);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					rs.next();
					e = new Endereco(new Bairro(rs.getString("nomeBairro")), 
									 new Cidade(rs.getString("nomeCidade"), 
											    new UnidadeFederacao(rs.getString(e.getCidade().getUf().getIdentifierName()),
												  	 			     rs.getString("nomeUF"))),
									 new Logradouro(rs.getString("nomeLogradouro")),
									 rs.getString("cep"));
				}
				conn.close();
				if(e.equals(new Endereco()))		return null;
				return e;
			}catch(Exception e){
				e.printStackTrace();
				return null;
			}
		}
		
		public Endereco getAddressByPatientID(long idPaciente) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				Endereco e = new Endereco();
				Paciente p = new Paciente();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(p.getEndereco())
 					 .joinOn(p, p.getEndereco(), p.getEndereco().getIdentifierName())
					 .joinOn(p.getEndereco(), p.getEndereco().getBairro(), 
							 p.getEndereco().getBairro().getIdentifierName())
					 .joinOn(p.getEndereco(), p.getEndereco().getCidade(), 
							 p.getEndereco().getCidade().getIdentifierName())
					 .joinOn(p.getEndereco().getCidade(), p.getEndereco().getCidade().getUf(),
							 p.getEndereco().getCidade().getUf().getIdentifierName())
					 .joinOn(p.getEndereco(), p.getEndereco().getLogradouro(), 
							 p.getEndereco().getLogradouro().getIdentifierName())
					 .joinOn(p.getEndereco().getLogradouro(), p.getEndereco().getLogradouro().getTipoLogradouro(), 
							 p.getEndereco().getLogradouro().getTipoLogradouro().getIdentifierName())
					 .where(p.getIdentifierName() + " = " + idPaciente);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					rs.next();
					e = new Endereco(new Bairro(rs.getString("nomeBairro")), 
									 new Cidade(rs.getString("nomeCidade"), 
											    new UnidadeFederacao(rs.getString(e.getCidade().getUf().getIdentifierName()),
												  	 			     rs.getString("nomeUF"))),
									 new Logradouro(rs.getString("nomeLogradouro")),
									 rs.getString("cep"));
				}
				conn.close();
				if(e.equals(new Endereco()))		return null;
				return e;
			}catch(Exception e){
				e.printStackTrace();
				return null;
			}
		}
		
		public Endereco getAddressByMedicID(long idMedico) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				Endereco e = new Endereco();
				Medico m = new Medico();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(m.getEndereco())
 					 .joinOn(m, m.getEndereco(), m.getEndereco().getIdentifierName())
					 .joinOn(m.getEndereco(), m.getEndereco().getBairro(), 
							 m.getEndereco().getBairro().getIdentifierName())
					 .joinOn(m.getEndereco(), m.getEndereco().getCidade(), 
							 m.getEndereco().getCidade().getIdentifierName())
					 .joinOn(m.getEndereco().getCidade(), m.getEndereco().getCidade().getUf(),
							 m.getEndereco().getCidade().getUf().getIdentifierName())
					 .joinOn(m.getEndereco(), m.getEndereco().getLogradouro(), 
							 m.getEndereco().getLogradouro().getIdentifierName())
					 .joinOn(m.getEndereco().getLogradouro(), m.getEndereco().getLogradouro().getTipoLogradouro(), 
							 m.getEndereco().getLogradouro().getTipoLogradouro().getIdentifierName())
					 .where(m.getIdentifierName() + " = " + idMedico);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					rs.next();
					e = new Endereco(new Bairro(rs.getString("nomeBairro")), 
									 new Cidade(rs.getString("nomeCidade"), 
											    new UnidadeFederacao(rs.getString(e.getCidade().getUf().getIdentifierName()),
												  	 			     rs.getString("nomeUF"))),
									 new Logradouro(rs.getString("nomeLogradouro")),
									 rs.getString("cep"));
				}
				conn.close();
				if(e.equals(new Endereco()))		return null;
				return e;
			}catch(Exception e){
				e.printStackTrace();
				return null;
			}
		}
		
		public List<Endereco> getAddressList() throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder(conn);
				Endereco end = new Endereco();
				query.select("*").from(end)
					 .joinOn(end, end.getBairro(), end.getBairro().getIdentifierName())
					 .joinOn(end, end.getLogradouro(), end.getLogradouro().getIdentifierName())
					 .joinOn(end.getLogradouro(), end.getLogradouro().getTipoLogradouro(), 
							 end.getLogradouro().getTipoLogradouro().getIdentifierName())
					 .joinOn(end, end.getCidade(), end.getCidade().getIdentifierName())
					 .joinOn(end.getCidade(), end.getCidade().getUf(), 
							 end.getCidade().getUf().getIdentifierName());
				List<Endereco> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						list.add(new Endereco(
									 new Bairro(rs.getString("nomeBairro")),
									 new Cidade(rs.getString("nomeCidade"), 
											    new UnidadeFederacao(rs.getString("siglaUF"),rs.getString("nomeUF"))),
									 new Logradouro(rs.getString("nomeLogradouro"),
											 	    new TipoLogradouro(rs.getString("siglaTipoLogradouro"),rs.getString("nomeTipo"))),
									 rs.getString("cep")
									 )
								);
					}
				}
				if(list.isEmpty())		return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public boolean insertAddress(Endereco e) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder(conn);
				DaoBairro daoBairro = new DaoBairro();
				DaoCidade daoCidade = new DaoCidade();
				DaoLogradouro daoLograd = new DaoLogradouro();
				long idBairro, idCidade, idLogradouro;
				
				idBairro = daoBairro.getDistrictIDWithInsert(e.getBairro());
				idCidade = daoCidade.getCityIDWithInsert(e.getCidade());
				idLogradouro = daoLograd.getPlaceIDWithInsert(e.getLogradouro());
				
				e.setBairro(new Bairro(idBairro));
				e.setCidade(new Cidade(idCidade));
				e.setLogradouro(new Logradouro(idLogradouro));
				
				query.insert(e);
				try(Statement insertStmt = conn.createStatement()){
					return insertStmt.execute(query.getSQL());
				}
			}catch(Exception exc) {
				exc.printStackTrace();
				return false;
			}
		}
		
		public long getAddressIDWithInsert(Endereco e) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder(conn);
				DaoBairro daoBairro = new DaoBairro();
				DaoCidade daoCidade = new DaoCidade();
				DaoLogradouro daoLograd = new DaoLogradouro();
				long idBairro, idCidade, idLogradouro;
				
				idBairro = daoBairro.getDistrictIDWithInsert(e.getBairro());
				idCidade = daoCidade.getCityIDWithInsert(e.getCidade());
				idLogradouro = daoLograd.getPlaceIDWithInsert(e.getLogradouro());
				
				e.setBairro(new Bairro(idBairro));
				e.setCidade(new Cidade(idCidade));
				e.setLogradouro(new Logradouro(idLogradouro));
				
				ArrayList<String> cond = new ArrayList<>();
				cond.add("idBairro = " + idBairro);
				cond.add("idCidade = " + idCidade);
				cond.add("idLogradouro = " + idLogradouro);
				cond.add("cep = " + Formatter.formatSQLString(e.getCep()));
				query.select("*").from(e)
					.whereAllAND(cond);
				long idEndereco = -1;
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						idEndereco = rs.getLong(e.getIdentifierName());
					}else {
						query.endStatement();
						query.insert(e);
						try(Statement insertStmt = conn.createStatement()){
							System.out.println("Endereco => " + query.getSQL());
							insertStmt.executeUpdate(query.getSQL(),Statement.RETURN_GENERATED_KEYS);
							ResultSet rs1 = insertStmt.getGeneratedKeys();
							if(rs.next()) {
								idEndereco = rs1.getLong(1);
							}
						}
					}
				}
				return idEndereco;
			}catch(Exception exc) {
				exc.printStackTrace();
				return -1;
			}
		}
		
		public boolean updateAddress(Endereco e) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder(conn);
				query.update(e, "Endereco.idEndereco = " + e.getIdentifier());
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					return stmt.execute();
				}
			}catch(Exception exc) {
				exc.printStackTrace();
				return false;
			}
		}
	}