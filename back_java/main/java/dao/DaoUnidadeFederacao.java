package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import entity.UnidadeFederacao;
import util.SQLBuilder;

	public class DaoUnidadeFederacao {

		public DaoUnidadeFederacao() {
			super();
		}
		
		public String getInitialsByUf(UnidadeFederacao uf) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(uf)
					 .where("nomeUF").equal(uf.getNomeUF());
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						return rs.getString(uf.getIdentifierName());
					}else {
						return null;
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<UnidadeFederacao> getStateList() throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				UnidadeFederacao uf = new UnidadeFederacao();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(uf);
				List<UnidadeFederacao> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						list.add(new UnidadeFederacao(rs.getString(uf.getIdentifierName()),rs.getString("nomeUF")));
					}
				}
				conn.close();
				if(list.isEmpty())		return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public UnidadeFederacao getUFByInitials(String sigla) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				UnidadeFederacao uf = new UnidadeFederacao();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(uf)
					.where(uf,uf.getIdentifierName())
					.equal(sigla);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					ResultSet rs = stmt.executeQuery();
					rs.next();
					uf = new UnidadeFederacao(rs.getString(uf.getIdentifierName()),rs.getString("nomeUF"));
				}
				conn.close();
				if(uf.equals(new UnidadeFederacao()))	return null;
				return uf;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public UnidadeFederacao getUFByName(String nomeUF) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				UnidadeFederacao uf = new UnidadeFederacao();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(uf)
					 .where(uf,"nomeUF")
					 .equal(nomeUF);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					rs.next();
					uf = new UnidadeFederacao(rs.getString(uf.getIdentifierName()),rs.getString("nomeUF"));
				}
				conn.close();
				if(uf.equals(new UnidadeFederacao()))	return null;
				return uf;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public String getInitialsWithInsert(UnidadeFederacao uf) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder(conn);			
				query.select("*").from(uf)
					.where("nomeUF")
					.equal(uf.getNomeUF());
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						return rs.getString(uf.getIdentifierName());
					}else {
						query.endStatement();
						// TODO Lógica não funcional para inserção de medico para registrar lista de especialidades médicas
					}
					
				}
				
				query.insert(uf);
				try(Statement stmt = conn.createStatement()){
					System.out.println(query.getSQL());
					stmt.executeUpdate(query.getSQL());
					query.endStatement();
					query.select("*").from(uf)
						 .where("nomeUF")
						 .equal(uf.getNomeUF());
					ResultSet rs = conn.prepareStatement(query.getSQL()).executeQuery();
					if(rs.next()) {
						return rs.getString(uf.getIdentifierName());
					}
				}
				return null;
			}catch(Exception e) {
				e.printStackTrace();
				if(uf.getIdentifier() != null)	return (String) uf.getIdentifier();
				return null;
			}
		}
		
		public boolean registerUF(UnidadeFederacao uf) throws Exception{
			
			Connection conn = ConnectionFactory.getConnection();
			try {
				SQLBuilder query = new SQLBuilder();
				query.insert(uf);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					if(this.getInitialsByUf(uf) == null)	return stmt.execute();
					return true;
				}
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
	}
