package util;


import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import entity.Entity;
import entity.Medico;
import entity.UnidadeFederacao;

	public class SQLBuilder {
	
		private Connection conn;
		private StringBuilder query;
		
		public SQLBuilder() {
			conn = null;
			query = new StringBuilder();
		}
		
		public SQLBuilder(Connection conn) {
			this.conn = conn;
			query = new StringBuilder();
		}
		
		public void setAutoCommit(boolean autoCommit) throws SQLException {
			this.conn.setAutoCommit(autoCommit);
		}

		public void roolback() throws SQLException {
			this.conn.rollback();
		}
		
		public void commit() throws SQLException {
			this.conn.commit();
		}

		public void setSavepoint() throws SQLException {
			this.conn.setSavepoint();
		}
		
		public void close() throws SQLException {
			this.conn.close();
		}
		
		public SQLBuilder insert(Entity table) throws Exception {
				
			ArrayList<Object> valueList = new ArrayList<>();
			query.append(" INSERT IGNORE INTO ")
	             .append(table.getClass().getSimpleName())
	             .append(" (");
			if(table.getIdentifierName() == null) {
				;
			}
			else if(!table.getIdentifierName().contains("id")) {
				query.append(table.getIdentifierName()).append(",");
				valueList.add(table.getIdentifier());
			}
			for(Field f : table.getClass().getDeclaredFields()) {
				f.setAccessible(true);
				if(f.get(table) != null) {
					if(!(f.get(table) instanceof List || f.get(table) instanceof Map)) {
//						System.out.println(f.getName() + " ==> " + f.get(table));
						if(f.get(table) instanceof Entity) {
							// Resolve para dois níveis de entidades (verificar relacionamento para múltiplas entidades como acontece em endereco)				
							if(table instanceof Medico && f.get(table) instanceof UnidadeFederacao) {	
								query.append("crmUf").append(",");
							}else {
								query.append(((Entity) f.get(table)).getIdentifierName()).append(",");
							}
							valueList.add(((Entity) f.get(table)).getIdentifier());
						}else {
							valueList.add(f.get(table));
							query.append(f.getName()).append(",");
						}
					}
				}
			}
			query.setCharAt(query.toString().length()-1,')');
			query.append(" VALUES (");
			for(Object obj : valueList) {
				if(obj != null) {
					query.append(Formatter.formatSQLValue(obj)).append(",");	
				}
			}
			query.setCharAt(query.toString().length()-1,')');
			query.append(";");
			return this;
		}
		
		public SQLBuilder updateSetWhere(Entity table, LinkedHashMap<String,Object> valueMap, List<String> whereCondition) {
			
			query.append(" UPDATE ")
				 .append(table.getClass().getSimpleName())
				 .append(" SET ");
			
			for(String key : valueMap.keySet()) {
				query.append(key).append(" = ")	
					 .append(Formatter.formatSQLValue(valueMap.get(key)))
					 .append(", ");
			}
			query.replace(query.lastIndexOf(", "), query.toString().length()," ");
			this.whereAllAND(new ArrayList<>(whereCondition));
			
			return this;
		}
		
		public SQLBuilder update(Entity table, String whereCondition) throws Exception{
			
			query.append(" UPDATE ")
				 .append(table.getClass().getSimpleName())
				 .append(" SET ");
			for(Field f : table.getClass().getDeclaredFields()){
				f.setAccessible(true);
				Object obj = f.get(table);
				Object valueType;
				if(obj instanceof Entity) {
					query.append(((Entity) obj).getIdentifierName());
//						 .append(((Entity) obj).getIdentifier());
					valueType = ((Entity) obj).getIdentifier();
				}else {
					query.append(f.getName());
					valueType = obj;
				}
				query.append(" = ");
				if(valueType instanceof String || valueType instanceof Date) {
					query.append(Formatter.formatSQLString((String) obj));
				}else {
					query.append(obj);
				}
				query.append(",");
			}
			query.setCharAt(query.lastIndexOf(","),' ');
			query.append(" WHERE ").append(whereCondition);
			
			return this;
		}
		/*
		// keys.size() = values.size();
		public SQLBuilder insert(Object targetTable, ArrayList<String> keys, 
								 ArrayList<Object> values) {
			
			query.append(" INSERT INTO ")
                 .append(targetTable.getClass().getSimpleName())
                 .append("(");
			for(String s : keys) {
				query.append(s).append(",");
			}
			query.setCharAt(query.toString().length()-1,')');
			query.append(" VALUES (");
			for(Object o : values) {
				if(o.getClass() == String.class) {
					query.append(Formatter.formatSQLString((String) o));
				}else {
					query.append(String.valueOf(o));
				}
				query.append(",");
			}
			query.setCharAt(query.toString().length()-1,')');
			query.append(";");
			return this;
		}
		*/
		/*
		// keys.size() = values.size();
		// Sempre deve estar acompanhado do comando WHERE
		public SQLBuilder update(Object targetTable, ArrayList<String> keys, 
				 ArrayList<Object> values) {
			
			query.append(" UPDATE ")
			     .append(targetTable.getClass().getSimpleName())
			     .append(" SET ");
			for(int i = 0 ; i < keys.size() ; i++) {
				query.append(keys.get(i))
				     .append(" = ");
				if(values.get(i).getClass() == String.class) {
					query.append(Formatter.formatSQLString((String) values.get(i)));
				}else {
					query.append(String.valueOf(values.get(i)));
				}
				query.append(",");
			}
			query.setCharAt(query.toString().length()-1,' ');
			
			return this;
		}
		
		public SQLBuilder updateWhereAND(Object targetTable, ArrayList<String> keys, 
				 ArrayList<Object> values, ArrayList<String> whereCondition) {
			
			this.update(targetTable, keys, values).whereAllAND(whereCondition);	
			return this;
		}
		
		public SQLBuilder updateWhereOR(Object targetTable, ArrayList<String> keys, 
				 ArrayList<Object> values, ArrayList<String> whereCondition) {
			
			this.update(targetTable, keys, values).whereAllOR(whereCondition);
			return this;
		}
		*/
		
		// Sempre deve estar acompanhado do comando WHERE
		public SQLBuilder delete(Object targetTable) {
			
			query.append(" DELETE FROM ")
				 .append(targetTable.getClass().getSimpleName());
			return this;
		}
		
		public SQLBuilder deleteByID(Entity targetTable, Object identifier) {
			
			this.delete(targetTable)
				.where(targetTable.getIdentifierName())
				.equal(identifier);
			return this;
		}
		
		public SQLBuilder deleteWhereAND(Object targetTable, ArrayList<String> whereCondition) {
			
			this.delete(targetTable).whereAllAND(whereCondition);
			return this;
		}
		
		public SQLBuilder deleteWhereOR(Object targetTable, ArrayList<String> whereCondition) {
			
			this.delete(targetTable).whereAllOR(whereCondition);
			return this;
		}
		
		public SQLBuilder select(String ...fields) {
			
			query.append("SELECT ");
			for(String s : fields) {
				query.append(s).append(",");
			}
			query.setCharAt(query.toString().length()-1,' ');
			return this;
		}
		
		public SQLBuilder selectMax(String field) {
			
			query.append("SELECT MAX (")
				 .append(field)
				 .append(") ");
			return this;
		}
		
		public SQLBuilder selectMin(String field) {
			
			query.append("SELECT MIN (")
				 .append(field)
				 .append(") ");
			return this;
		}
		
		public SQLBuilder selectCount(String... field) {
			
			query.append("SELECT ");
			for(String str : field) {
				query.append("COUNT")
					 .append("(").append(str).append(")")
					 .append(", ");
			}
			query.setCharAt(query.toString().lastIndexOf(','),' ');
			return this;
		}
		
		public SQLBuilder from(Object table) {
			
			query.append(" FROM ")
			     .append(table.getClass().getSimpleName())
			     .append(" ");
			return this;
		}
		
		public SQLBuilder joinOn(Entity tableSrc, Entity tableTarget, String attribute) {
			
			query.append(" JOIN ")
				 .append(tableTarget.getClass().getSimpleName())
				 .append(" ON ")
				 .append(tableSrc.getClass().getSimpleName())
				 .append(".").append(attribute)
				 .append(" = ")
				 .append(tableTarget.getClass().getSimpleName())
				 .append(".").append(attribute);	
			return this;
		}
		
		public SQLBuilder joinOnAliased(String alias, Entity tableSrc, Entity tableTarget, String attribute) {
			
			query.append(" JOIN ")
				 .append(tableTarget.getClass().getSimpleName())
				 .append(" AS ").append(alias)
				 .append(" ON ")
				 .append(tableSrc.getClass().getSimpleName())
				 .append(".").append(attribute)
				 .append(" = ")
				 .append(alias)
				 .append(".").append(attribute);	
			return this;
		}
		
		public SQLBuilder innerJoinOn(Entity tableSrc, Entity tableTarget, String attribute) {
			
			query.append(" INNER JOIN ")
				 .append(tableTarget.getClass().getSimpleName())
				 .append(" ON ")
				 .append(tableSrc.getClass().getSimpleName())
				 .append(".").append(attribute)
				 .append(" = ")
				 .append(tableTarget.getClass().getSimpleName())
				 .append(".").append(attribute);	
			return this;
		}
		
		public SQLBuilder outerJoinOn(Entity tableSrc, Entity tableTarget, String attribute) {
			
			query.append(" OUTER JOIN ")
				 .append(tableTarget.getClass().getSimpleName())
				 .append(" ON ")
				 .append(tableSrc.getClass().getSimpleName())
				 .append(".").append(attribute)
				 .append(" = ")
				 .append(tableTarget.getClass().getSimpleName())
				 .append(".").append(attribute);	
			return this;
		}
	
		
		public SQLBuilder leftJoinOn(Entity tableSrc, Entity tableTarget, String attribute) {
			
			query.append(" LEFT JOIN ")
				 .append(tableTarget.getClass().getSimpleName())
				 .append(" ON ")
				 .append(tableSrc.getClass().getSimpleName())
				 .append(".").append(attribute)
				 .append(" = ")
				 .append(tableTarget.getClass().getSimpleName())
				 .append(".").append(attribute);	
			return this;
		}
		
		public SQLBuilder rightJoinOn(Entity tableSrc, Entity tableTarget, String attribute) {
			
			query.append(" RIGHT JOIN ")
				 .append(tableTarget.getClass().getSimpleName())
				 .append(" ON ")
				 .append(tableSrc.getClass().getSimpleName())
				 .append(".").append(attribute)
				 .append(" = ")
				 .append(tableTarget.getClass().getSimpleName())
				 .append(".").append(attribute);	
			return this;
		}
		
		public SQLBuilder where(String arg) {
			
			// WHERE arg = 1;
			// WHERE arg ==> progressão de Query
			query.append(" WHERE ")
				 .append(arg);
			return this;
		}
		
		public SQLBuilder where(Entity tableSrc, String field) {
			
			// WHERE arg = 1;
			// WHERE arg ==> progressão de Query
			query.append(" WHERE ")
				 .append(tableSrc.getClass().getSimpleName())
				 .append(".").append(field);
			return this;
		}
		
		public SQLBuilder whereEqual(Entity tableSrc, String field, Object value) {
			
			// WHERE arg = 1;
			// WHERE arg ==> progressão de Query
			query.append(" WHERE ")
				 .append(tableSrc.getClass().getSimpleName())
				 .append(".").append(field)
				 .append(" = ");
			if(value.getClass() != String.class || value.getClass() != Date.class)  query.append(value);
			else {
				query.append(Formatter.formatSQLString(value.toString()));
			}
			return this;
		}
		
		public SQLBuilder whereAllAND(ArrayList<String> conditions) {
			
			query.append(" WHERE ");
			for(String cond : conditions) {
				query.append(cond)
					 .append(" AND ");
			}
			query.replace(query.lastIndexOf(" AND "), query.toString().length()," ");
			return this;
		}
		
		public SQLBuilder whereAllOR(ArrayList<String> conditions) {
			
			query.append(" WHERE ");
			for(String cond : conditions) {
				query.append(cond)
					 .append(" OR ");
			}
			query.replace(query.lastIndexOf(" OR "), query.toString().length()," ");
			return this;
		}
		
		public SQLBuilder equal(Object value) {
			
			query.append(" = ");
//			System.out.println(value.getClass().getSimpleName());
			if(value.getClass() != String.class) query.append(value);
			else{
				query.append(Formatter.formatSQLString(value.toString()));
			}
			return this;
		}
		
		public SQLBuilder notEqual(Object value) {
			
			query.append(" != ");
			if(value.getClass() != String.class || value.getClass() != Date.class) query.append(value);
			else{
				query.append(Formatter.formatSQLString(value.toString()));
			}
			return this;
		}
		
		public SQLBuilder greater(Integer value) {
			
			query.append(" > ")
				 .append(value);
			return this;
		}
		
		public SQLBuilder lesser(Integer value) {
			
			query.append(" < ")
				 .append(value);
			return this;
		}
		
		public SQLBuilder greaterEqual(Integer value) {
			
			query.append(" >= ")
				 .append(value);
			return this;
		}
		
		public SQLBuilder lesserEqual(Integer value) {
			
			query.append(" <= ")
				 .append(value);
			return this;
		}
		
		public SQLBuilder like(String pattern) {
			
			query.append(" LIKE ")
				 .append(Formatter.formatSQLString(pattern));
			return this;
		}
		
		public SQLBuilder isNull(boolean notFlag) {
			
			if(notFlag) {
				query.append(" IS NOT NULL ");
			}else {
				query.append(" IS NULL ");
			}
			return this;
		}
		
		public SQLBuilder between(String minValue, String maxValue, boolean isNumeric) {
			
			query.append(" BETWEEN ");
			if(isNumeric) {
				query.append(minValue)
					 .append(" AND ")
					 .append(maxValue);
			}else {
				query.append(Formatter.formatSQLString(minValue))
					 .append(" AND ")
					 .append(Formatter.formatSQLString(maxValue));
			}
			return this;
		}
		
		public SQLBuilder groupBy(String attribute) {
			
			query.append(" GROUP BY ")
				 .append(attribute);
			return this;
		}
		
		public SQLBuilder orderBy(String attribute, boolean desc) {
			
			query.append(" ORDER BY ")
				 .append(attribute);
			if(desc)	query.append(" DESC ");
			return this;
		}
		
		public SQLBuilder AND(String condition) {
			
			query.append(" AND ")
				 .append(condition);
			return this;
		}
		
		public SQLBuilder OR(String condition) {
			
			query.append(" OR ")
				 .append(condition);
			return this;	
		}
		
		public SQLBuilder endStatement() {
			
			query = new StringBuilder();
			return this;
		}
		
		public String getSQL() {
			return this.query.toString();
		}
		
		
	}
