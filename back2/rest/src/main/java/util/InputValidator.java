package util;

import api.PostmonAPI;

	public class InputValidator {

		public static boolean validatePostalCode(String postalCode) {

	        try {
	            PostmonAPI req = new PostmonAPI(postalCode);
	            req.run();
	            String response = req.getJson();
	            System.out.println("Response = " + response);
	            return !(response == null || response.contains("\"erro\": true"));

	        } catch (Exception e) {
	            e.printStackTrace();
	            return false;
	        }

	    }
		
		public static boolean validateCPF(String cpf) {
			
			return (cpf.matches("[0-9]+") && (cpf.length() == 11) && calculoCPF(cpf));
		}
		
		public static boolean calculoCPF(String cpf) {

	        Integer[] value = new Integer[cpf.length()];

	        for(int i = 0 ; i < cpf.length() ; i++) {
	            value[i] = cpf.charAt(i) - 48;
	        }

	        int s1 = 0, r1, s2 = 0, r2;
	        for(int i = 0, j = 10 ; i < 9; i++, j--) {
	            s1 += value[i] * j;
	        }

	        r1 = s1 % 11;
	        if(r1 < 2)		r1 = 0;
	        else			r1 = 11 - r1;

	        for(int i = 0 , j = 11 ; i < 10 ; i++, j--) {
	            s2 += value[i] * j;
	        }

	        r2 = s2 % 11;
	        if(r2 < 2)		r2 = 0;
	        else 			r2 = 11 - r2;

	        return r1 == value[9] && r2 == value[10];
	    }
		
	}
