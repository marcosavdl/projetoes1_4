package util;

import java.sql.Date;

public class Formatter {
	
		public static String formatSQLString(String str) {
			return "'" + str + "'";
		}
		
		public static String convertRealToSQL_DateTime(String realDateTime) {
			return "";
		}
		
		public static String convertSQLToReal_DateTime(String sqlDateTime) {
			
			// sqlDateTime = 'yyyy-mm-dd hh:mm:ss'
			String[] splitDateTime = sqlDateTime.split(" ");
			String date, time;
			date = splitDateTime[0];
			time = splitDateTime[1];
			
			String[] splitDate = date.split("-");
			String[] splitTime = time.split(":");			
			StringBuilder result = new StringBuilder();
			result.append(splitDate[2]).append("/")
				  .append(splitDate[1]).append("/")
				  .append(splitDate[0]).append(" ")
				  .append(splitTime[0]).append(":")
				  .append(splitTime[1]).append(":")
				  .append(splitTime[2]);
			
			return result.toString();
		}
		
		public static Object formatSQLValue(Object obj) {
			try {
				if(obj.getClass() == String.class || obj.getClass() == Date.class)	return formatSQLString((String) obj);
				return obj;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}
