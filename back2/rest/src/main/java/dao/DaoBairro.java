package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entity.Bairro;
import util.SQLBuilder;

	public class DaoBairro extends DefaultDao{

		public DaoBairro() {
			super();
		}
		
		public DaoBairro(Connection conn) {
			super(conn);
		}
		
		public long getIDByDistrict(Bairro bairro){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(bairro)
				 .where("nomeBairro").equal(bairro.getNomeBairro());
				long districtID = -1;
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL(),Statement.RETURN_GENERATED_KEYS)){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						districtID = rs.getLong(bairro.getIdentifierName());
					}
				}
				return districtID;
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public Bairro getDistrictByID(long idBairro){
			
			try {
				Connection conn = getConnection();
				Bairro b = new Bairro();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(b)
					 .where(b,b.getIdentifierName())
					 .equal(idBairro);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						return new Bairro(rs.getString("nomeBairro"));
					}else {
						return null;
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public Bairro getDistrictByName(String nomeBairro){
			
			try {
				Connection conn = getConnection();
				Bairro b = new Bairro();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(b)
					 .where(b,"nomeBairro")
					 .equal(nomeBairro);
				return null;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<Bairro> getDistrictList(){
			
			try {
				Connection conn = getConnection();		
				Bairro b = new Bairro();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(b);
				List<Bairro> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						list.add(new Bairro(rs.getString("nomeBairro")));
					}
				}
				if(list.isEmpty())		return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public boolean registerDistrict(Bairro b){
			
			try {
				Connection conn = getConnection();		
				SQLBuilder query = new SQLBuilder();
				query.insert(b);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					if(this.getIDByDistrict(b) == -1)	return stmt.execute();
					return true;
				}
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
		public long getDistrictIDWithInsert(Bairro b){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(b)
					 .where("nomeBairro").equal(b.getNomeBairro());
				long idBairro = -1;
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						idBairro =  rs.getLong(b.getIdentifierName());
					}else {
						query.endStatement();
						query.insert(b);
						try(Statement insertStmt = conn.createStatement()){
							insertStmt.executeUpdate(query.getSQL(),Statement.RETURN_GENERATED_KEYS);
							ResultSet rs1 = insertStmt.getGeneratedKeys();
							if(rs1.next()) {
								idBairro = rs1.getLong(1);
							}
						}
					}
				}
				return idBairro;
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public boolean updateDistrict(Bairro b){
			
			try {
				Connection conn = getConnection();	
				SQLBuilder query = new SQLBuilder();
				query.update(b,"Bairro.idBairro = " + b.getIdentifier());
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					return stmt.execute();
				}
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
	}
