package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import entity.AgendaConsultaMedica;
import entity.EspecialidadeMedica;
import entity.Medico;
import entity.Paciente;
import util.Formatter;
import util.SQLBuilder;
	
	public class DaoAgendaConsultaMedica extends DefaultDao{
	
		public DaoAgendaConsultaMedica() {
			super();
		}
		
		public DaoAgendaConsultaMedica(Connection conn) {
			super(conn);
		}
		
		public List<AgendaConsultaMedica> getAppointScheduleList(){
			
			try {
				Connection conn = getConnection();
				AgendaConsultaMedica acm = new AgendaConsultaMedica();
				DaoMedico daoMed = new DaoMedico(conn);
				DaoPaciente daoPac = new DaoPaciente(conn);
				DaoEspecialidadeMedica daoEsp = new DaoEspecialidadeMedica(conn);
				SQLBuilder query = new SQLBuilder(conn);
				
				query.select("*").from(acm)
				 	.joinOn(acm, acm.getMedico(), acm.getMedico().getIdentifierName())
				 	.joinOn(acm, acm.getPaciente(), acm.getPaciente().getIdentifierName())
				 	.joinOnAliased("acm",acm, acm.getEspecialidadeMedica(), acm.getEspecialidadeMedica().getIdentifierName());
				
				System.out.println(query.getSQL());
				List<AgendaConsultaMedica> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						Medico m = daoMed.getMedicByID(rs.getLong(acm.getMedico().getIdentifierName()));
						Paciente p = daoPac.getPatientByID(rs.getLong(acm.getPaciente().getIdentifierName()));
						EspecialidadeMedica esp = daoEsp.getSpecialityByID(rs.getLong(acm.getIdentifierName()));
						list.add(new AgendaConsultaMedica(rs.getLong(acm.getIdentifierName()),
														 Formatter.convertSQLToReal_DateTime(rs.getString("dataRealizada")),
														 Formatter.convertSQLToReal_DateTime(rs.getString("dataAgendada")), 
														 rs.getString("sintomas"), m, p, esp
														 )
								);
					}
				}
				if(list.isEmpty())		return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public long getAppointScheduleQuantity() {
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder(conn);
				AgendaConsultaMedica acm = new AgendaConsultaMedica();
				query.selectCount(acm.getIdentifierName())
				 	 .from(acm);
				long count = -1; 
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					rs.next();
					count = rs.getLong("COUNT(" + acm.getIdentifierName() + ")");
				}
				return count;
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public boolean registerAppointSchedule(AgendaConsultaMedica agenda){
			
			try {
				Connection conn = getConnection();		
				SQLBuilder query = new SQLBuilder(conn);
				
				
				query.insert(agenda);
				System.out.println(query.getSQL());
				
				return true;
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
		public boolean removeAppointSchedule(long idAgendaConsultaMedica){
			
			try {
				Connection conn = getConnection();		
				AgendaConsultaMedica acm = new AgendaConsultaMedica();
				SQLBuilder query = new SQLBuilder(conn);
				query.delete(acm)
					 .whereEqual(acm,acm.getIdentifierName(), idAgendaConsultaMedica);
				System.out.println(query.getSQL());
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					int removed = stmt.executeUpdate();
					if(removed == 1)	return true;
				}
				return false;
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
	}
