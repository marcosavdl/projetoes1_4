package dao;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import entity.Bairro;
import entity.Cidade;
import entity.Endereco;
import entity.EspecialidadeMedica;
import entity.Logradouro;
import entity.Medico;
import entity.Medico_EspecialidadeMedica;
import entity.TipoLogradouro;
import entity.UnidadeFederacao;
import util.Formatter;
import util.SQLBuilder;

	public class DaoMedico extends DefaultDao{

		public DaoMedico() {
			super();
		}
		
		public DaoMedico(Connection conn) {
			super(conn);
		}
		
		public Medico getMedicByID(long idMedico){
			
			try {
				Connection conn = getConnection();
				Medico m = new Medico();
				EspecialidadeMedica em = new EspecialidadeMedica();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(m)
					 .joinOn(m, new Medico_EspecialidadeMedica(), m.getIdentifierName())
					 .joinOn(new Medico_EspecialidadeMedica(), em, em.getIdentifierName())	
					 .joinOn(m, m.getEndereco(), m.getEndereco().getIdentifierName())
					 .joinOn(m.getEndereco(), m.getEndereco().getBairro(), 
							 m.getEndereco().getBairro().getIdentifierName())
					 .joinOn(m.getEndereco(), m.getEndereco().getCidade(), 
							 m.getEndereco().getCidade().getIdentifierName())
					 .joinOn(m.getEndereco().getCidade(), m.getEndereco().getCidade().getUf(),
							 m.getEndereco().getCidade().getUf().getIdentifierName())
					 .joinOn(m.getEndereco(), m.getEndereco().getLogradouro(), 
							 m.getEndereco().getLogradouro().getIdentifierName())
					 .joinOn(m.getEndereco().getLogradouro(), m.getEndereco().getLogradouro().getTipoLogradouro(), 
							 m.getEndereco().getLogradouro().getTipoLogradouro().getIdentifierName())
					 .where(m,m.getIdentifierName()).equal(idMedico);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						m = new Medico(rs.getLong(m.getIdentifierName()),
								rs.getString("crm"), rs.getString("primeiroNome"), rs.getString("sobrenome"),
								rs.getDate("dataNascimento").toLocalDate().toString(), rs.getString("tipoSanguineo"), rs.getString("fatorRh"),
								rs.getString("sexo"), rs.getInt("nroLogradouro"), rs.getString("complementoLogradouro"),
								new Endereco(rs.getLong(m.getEndereco().getIdentifierName()),
											 new Bairro(rs.getLong(m.getEndereco().getBairro().getIdentifierName()),rs.getString("nomeBairro")), 
											 new Cidade(rs.getLong(m.getEndereco().getCidade().getIdentifierName()),rs.getString("nomeCidade"),
													    new UnidadeFederacao(rs.getString("siglaUF"),rs.getString("nomeUF"))), 
											 new Logradouro(rs.getLong(m.getEndereco().getLogradouro().getIdentifierName()),
													 		rs.getString("nomeLogradouro"), 
													 	    new TipoLogradouro(rs.getString("siglaTipoLogradouro"), rs.getString("nomeTipo"))), 
											 rs.getString("cep")), 
								new UnidadeFederacao(rs.getString("crmUF"),""));
						List<EspecialidadeMedica> listEsp = new ArrayList<>();
						listEsp.add(new EspecialidadeMedica(rs.getString("tipoEspecialidade")));
						while(rs.next()) {
							listEsp.add(new EspecialidadeMedica(rs.getString("tipoEspecialidade")));
						}
						m.setListaEspecialidade(listEsp);
					}
				}
				if(m.equals(new Medico()))	return null;
				return m;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<Medico> getMedicList(){
			
			try {
				Connection conn = getConnection();
				Medico m = new Medico();
				EspecialidadeMedica em = new EspecialidadeMedica();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(m)
				 .joinOn(m, new Medico_EspecialidadeMedica(), m.getIdentifierName())
				 .joinOn(new Medico_EspecialidadeMedica(), em, em.getIdentifierName())				 
				 .joinOn(m, m.getEndereco(), m.getEndereco().getIdentifierName())
				 .joinOn(m.getEndereco(), m.getEndereco().getBairro(), 
						 m.getEndereco().getBairro().getIdentifierName())
				 .joinOn(m.getEndereco(), m.getEndereco().getCidade(), 
						 m.getEndereco().getCidade().getIdentifierName())
				 .joinOn(m.getEndereco().getCidade(), m.getEndereco().getCidade().getUf(),
						 m.getEndereco().getCidade().getUf().getIdentifierName())
				 .joinOn(m.getEndereco(), m.getEndereco().getLogradouro(), 
						 m.getEndereco().getLogradouro().getIdentifierName())
				 .joinOn(m.getEndereco().getLogradouro(), m.getEndereco().getLogradouro().getTipoLogradouro(), 
						 m.getEndereco().getLogradouro().getTipoLogradouro().getIdentifierName())
				 ;
				
				LinkedHashMap<Long,Medico> medicMap = new LinkedHashMap<>();	
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						long idMedico = rs.getLong(m.getIdentifierName());
						m = new Medico(idMedico,
								rs.getString("crm"), rs.getString("primeiroNome"), rs.getString("sobrenome"),
								rs.getDate("dataNascimento").toLocalDate().toString(), rs.getString("tipoSanguineo"), rs.getString("fatorRh"),
								rs.getString("sexo"), rs.getInt("nroLogradouro"), rs.getString("complementoLogradouro"),
								new Endereco(rs.getLong(m.getEndereco().getIdentifierName()),
											 new Bairro(rs.getLong(m.getEndereco().getBairro().getIdentifierName()),rs.getString("nomeBairro")), 
											 new Cidade(rs.getLong(m.getEndereco().getCidade().getIdentifierName()),rs.getString("nomeCidade"),
													    new UnidadeFederacao(rs.getString("siglaUF"),rs.getString("nomeUF"))), 
											 new Logradouro(rs.getLong(m.getEndereco().getLogradouro().getIdentifierName()),
													 		rs.getString("nomeLogradouro"), 
													 	    new TipoLogradouro(rs.getString("siglaTipoLogradouro"), rs.getString("nomeTipo"))), 
											 rs.getString("cep")), 
								new UnidadeFederacao(rs.getString("crmUF"),""));
						EspecialidadeMedica esp = new EspecialidadeMedica(rs.getLong(em.getIdentifierName()),rs.getString("tipoEspecialidade"));
						if(medicMap.containsKey(idMedico)) {
							medicMap.get(idMedico).getListaEspecialidade().add(esp);
						}else {
							m.getListaEspecialidade().add(esp);
							medicMap.put(idMedico,m);
						}
					}
				}
				if(medicMap.isEmpty())	return null;
				return new ArrayList<>(medicMap.values());
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		

		public List<Medico> getActiveMedicList(){
			
			try {
				Connection conn = getConnection();
				Medico m = new Medico();
				EspecialidadeMedica em = new EspecialidadeMedica();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(m)
				 .joinOn(m, new Medico_EspecialidadeMedica(), m.getIdentifierName())
				 .joinOn(new Medico_EspecialidadeMedica(), em, em.getIdentifierName())				 
				 .joinOn(m, m.getEndereco(), m.getEndereco().getIdentifierName())
				 .joinOn(m.getEndereco(), m.getEndereco().getBairro(), 
						 m.getEndereco().getBairro().getIdentifierName())
				 .joinOn(m.getEndereco(), m.getEndereco().getCidade(), 
						 m.getEndereco().getCidade().getIdentifierName())
				 .joinOn(m.getEndereco().getCidade(), m.getEndereco().getCidade().getUf(),
						 m.getEndereco().getCidade().getUf().getIdentifierName())
				 .joinOn(m.getEndereco(), m.getEndereco().getLogradouro(), 
						 m.getEndereco().getLogradouro().getIdentifierName())
				 .joinOn(m.getEndereco().getLogradouro(), m.getEndereco().getLogradouro().getTipoLogradouro(), 
						 m.getEndereco().getLogradouro().getTipoLogradouro().getIdentifierName())
				 .where("Medico.isAtivo").equal(true);
				LinkedHashMap<Long,Medico> medicMap = new LinkedHashMap<>();	
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						long idMedico = rs.getLong(m.getIdentifierName());
						m = new Medico(idMedico,
								rs.getString("crm"), rs.getString("primeiroNome"), rs.getString("sobrenome"),
								rs.getDate("dataNascimento").toLocalDate().toString(), rs.getString("tipoSanguineo"), rs.getString("fatorRh"),
								rs.getString("sexo"), rs.getInt("nroLogradouro"), rs.getString("complementoLogradouro"),
								new Endereco(rs.getLong(m.getEndereco().getIdentifierName()),
											 new Bairro(rs.getLong(m.getEndereco().getBairro().getIdentifierName()),rs.getString("nomeBairro")), 
											 new Cidade(rs.getLong(m.getEndereco().getCidade().getIdentifierName()),rs.getString("nomeCidade"),
													    new UnidadeFederacao(rs.getString("siglaUF"),rs.getString("nomeUF"))), 
											 new Logradouro(rs.getLong(m.getEndereco().getLogradouro().getIdentifierName()),
													 		rs.getString("nomeLogradouro"), 
													 	    new TipoLogradouro(rs.getString("siglaTipoLogradouro"), rs.getString("nomeTipo"))), 
											 rs.getString("cep")), 
								new UnidadeFederacao(rs.getString("crmUF"),""));
						EspecialidadeMedica esp = new EspecialidadeMedica(rs.getLong(em.getIdentifierName()),rs.getString("tipoEspecialidade"));
						if(medicMap.containsKey(idMedico)) {
							medicMap.get(idMedico).getListaEspecialidade().add(esp);
						}else {
							m.getListaEspecialidade().add(esp);
							medicMap.put(idMedico,m);
						}
					}
				}
				if(medicMap.isEmpty())	return null;
				return new ArrayList<>(medicMap.values());
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<Medico> getMedicListBySpecialityID(long idEspecialidade){
			
			try {
				Connection conn = getConnection();
				EspecialidadeMedica em = new EspecialidadeMedica();
				Medico m = new Medico();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(m)
		  			 .joinOn(m, new Medico_EspecialidadeMedica(), m.getIdentifierName())
					 .joinOn(new Medico_EspecialidadeMedica(), em, em.getIdentifierName())
					 .joinOn(m, m.getEndereco(), m.getEndereco().getIdentifierName())
					 .joinOn(m.getEndereco(), m.getEndereco().getBairro(), 
							 m.getEndereco().getBairro().getIdentifierName())
					 .joinOn(m.getEndereco(), m.getEndereco().getCidade(), 
							 m.getEndereco().getCidade().getIdentifierName())
					 .joinOn(m.getEndereco().getCidade(), m.getEndereco().getCidade().getUf(),
							 m.getEndereco().getCidade().getUf().getIdentifierName())
					 .joinOn(m.getEndereco(), m.getEndereco().getLogradouro(), 
							 m.getEndereco().getLogradouro().getIdentifierName())
					 .joinOn(m.getEndereco().getLogradouro(), m.getEndereco().getLogradouro().getTipoLogradouro(), 
							 m.getEndereco().getLogradouro().getTipoLogradouro().getIdentifierName())
				   .where("EspecialidadeMedica." + em.getIdentifierName()).equal(idEspecialidade);
				LinkedHashMap<Long,Medico> medicMap = new LinkedHashMap<>();	
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						long idMedico = rs.getLong(m.getIdentifierName());
						m = new Medico(idMedico,
								rs.getString("crm"), rs.getString("primeiroNome"), rs.getString("sobrenome"),
								rs.getDate("dataNascimento").toLocalDate().toString(), rs.getString("tipoSanguineo"), rs.getString("fatorRh"),
								rs.getString("sexo"), rs.getInt("nroLogradouro"), rs.getString("complementoLogradouro"),
								new Endereco(rs.getLong(m.getEndereco().getIdentifierName()),
											 new Bairro(rs.getLong(m.getEndereco().getBairro().getIdentifierName()),rs.getString("nomeBairro")), 
											 new Cidade(rs.getLong(m.getEndereco().getCidade().getIdentifierName()),rs.getString("nomeCidade"),
													    new UnidadeFederacao(rs.getString("siglaUF"),rs.getString("nomeUF"))), 
											 new Logradouro(rs.getLong(m.getEndereco().getLogradouro().getIdentifierName()),
													 		rs.getString("nomeLogradouro"), 
													 	    new TipoLogradouro(rs.getString("siglaTipoLogradouro"), rs.getString("nomeTipo"))), 
											 rs.getString("cep")), 
								new UnidadeFederacao(rs.getString("crmUF"),""));
						EspecialidadeMedica esp = new EspecialidadeMedica(rs.getLong(em.getIdentifierName()),rs.getString("tipoEspecialidade"));
						if(medicMap.containsKey(idMedico)) {
							medicMap.get(idMedico).getListaEspecialidade().add(esp);
						}else {
							m.getListaEspecialidade().add(esp);
							medicMap.put(idMedico,m);
						}
					}
				}
				if(medicMap.isEmpty())	return null;
				return new ArrayList<>(medicMap.values());
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public long getMedicIDWithInsert(Medico m) {
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder(conn);
				DaoEndereco daoE = new DaoEndereco(conn);
				
				if(m.getEndereco().getIdentifier() == null) {
					m.getEndereco().setIdentifier(daoE.getAddressIDWithInsert(m.getEndereco()));	
				}
				query.select("*").from(m)
					 .whereAllAND(this.getAllFieldsConditionFromMedic(m));
				System.out.println(query.getSQL());
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						return rs.getLong(m.getIdentifierName());
					}else {
						query.endStatement();
						query.insert(m);
						try(Statement insertStmt = conn.createStatement()){
							insertStmt.executeUpdate(query.getSQL(),Statement.RETURN_GENERATED_KEYS);
							ResultSet rs1 = insertStmt.getGeneratedKeys();
							if(rs1.next()) {
								return rs1.getLong(1);
							}
						}
					}
				}
				return -1;
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public long getMedicQuantity(){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder();
				Medico m = new Medico();
				query.selectCount(m.getIdentifierName())
					 .from(m);
				long count = -1; 
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					rs.next();
					count = rs.getLong("COUNT(" + m.getIdentifierName() + ")");
				}
				return count;
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public boolean registerMedicSpecialityList(Medico m){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder(conn);
				DaoEspecialidadeMedica daoEsp = new DaoEspecialidadeMedica(conn);
				for(EspecialidadeMedica em : m.getListaEspecialidade()) {
					long idEspecialidade = daoEsp.getSpecialityIDWithInsert(em);
					System.out.println("ID = " + idEspecialidade);
					if(idEspecialidade != -1) {
						Medico_EspecialidadeMedica mem = new Medico_EspecialidadeMedica((long) m.getIdentifier(),idEspecialidade);
						query.insert(mem);
						System.out.println("Medico_EspecialidadeMedica => " + query.getSQL());
						try(PreparedStatement midEntityStmt = conn.prepareStatement(query.getSQL())){
							midEntityStmt.execute();
						}
						query.endStatement();
					}
				}
				// TODO Testar inserção de especialidade médica considerando um médico válido
				return true;
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
		public boolean registerMedic(Medico m){
			
			try {
				Connection conn = getConnection();	
				DaoEndereco daoEndereco = new DaoEndereco(conn);
				long idEndereco = daoEndereco.getAddressIDWithInsert(m.getEndereco());
				m.getEndereco().setIdEndereco(idEndereco);
				/*
				DaoUnidadeFederacao daoCrmUf = new DaoUnidadeFederacao();
				UnidadeFederacao crmUf = daoCrmUf.getUFByInitials(m.getCrmUF().getSiglaUF());
				if(crmUf == null) {
					daoCrmUf.getInitialsWithInsert(m.getCrmUF());
				}
				*/
				SQLBuilder query = new SQLBuilder(conn);
				query.insert(m);
				try(Statement stmt = conn.createStatement()){
					System.out.println(query.getSQL());
					stmt.execute(query.getSQL(),Statement.RETURN_GENERATED_KEYS);
					System.out.println(stmt);
					ResultSet rs = stmt.getGeneratedKeys();
					if(rs.next()) {
						m.setIdMedico(rs.getLong(1));
					}
				}
				if(m.getIdentifier() != null) {
					this.registerMedicSpecialityList(m);
					return true;
				}
				return false;
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
		public boolean deactivateMedic(long idMedico) {
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder(conn);
				Medico m = new Medico();
				LinkedHashMap<String,Object> columnValueMap = new LinkedHashMap<>();
				columnValueMap.put("isAtivo",false);
				List<String> whereCond = new ArrayList<>();
				whereCond.add(m.getIdentifierName() + " = " + idMedico);
				query.updateSetWhere(m, columnValueMap, whereCond);
				System.out.println(query.getSQL());
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					int updated = stmt.executeUpdate();
					if(updated == 1) {
						return true;
					}
				}
				return false;
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
		public boolean removeMedic(long idMedico){
			
			// Update no boolean isValido
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder(conn);
				query.deleteByID(new Medico(), idMedico);
				System.out.println(query.getSQL());
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					int removed = stmt.executeUpdate();
					if(removed == 1) {
						return true;
					}
				}
				return false;
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
		public ArrayList<String> getAllFieldsConditionFromMedic(Medico m){
			
			ArrayList<String> list = new ArrayList<>();
			try {
				for(Field f : m.getClass().getDeclaredFields()) {
					f.setAccessible(true);
					if(!(f.get(m) instanceof List || f.get(m) instanceof Map)) {
						if(f.get(m) instanceof Endereco) {
							Endereco e = (Endereco) f.get(m);
							list.add(e.getIdentifierName() + " = " + Formatter.formatSQLValue(e.getIdentifier()));
						}else if(f.get(m) instanceof UnidadeFederacao) {
							UnidadeFederacao uf = (UnidadeFederacao) f.get(m);
							list.add("crmUF" + " = " + Formatter.formatSQLValue(uf.getIdentifier()));
						}else{
							list.add(f.getName() + " = " + Formatter.formatSQLValue(f.get(m)));
						}
					}			
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			/*
			list.add("cpf = " + Formatter.formatSQLString(p.getCpf()));
			list.add("primeiroNome = " + Formatter.formatSQLString(p.getPrimeiroNome()));
			list.add("sobrenome = " + Formatter.formatSQLString(p.getSobrenome()));
			list.add("dataNascimento = " + Formatter.formatSQLString(p.getDataNascimento()));
			list.add("tipoDanguineo = " + Formatter.formatSQLString(p.getTipoSanguineo()));
			list.add("fatorRh = " + Formatter.formatSQLString(p.getFatorRh()));
			list.add("sexo = " + Formatter.formatSQLString(p.getSexo()));
			list.add("nroLogradouro = " + p.getNroLogradouro());
			if(p.getComplementoLogradouro() != null)	list.add("complementoLogradouro = " + Formatter.formatSQLValue(list)))
			*/
			return list;
		}
	}
