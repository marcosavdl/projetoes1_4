package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entity.EspecialidadeMedica;
import entity.Medico;
import entity.Medico_EspecialidadeMedica;
import util.SQLBuilder;

	public class DaoEspecialidadeMedica extends DefaultDao{

		public DaoEspecialidadeMedica() {
			super();
		}
		
		public DaoEspecialidadeMedica(Connection conn) {
			super(conn);
		}
		
		public long getIDBySpeciality(EspecialidadeMedica em){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(em)
					 .where("tipoEspecialidade")
					 .equal(em.getTipoEspecialidade());
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						return rs.getLong(em.getIdentifierName());
					}else {
						return -1;
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public EspecialidadeMedica getSpecialityByID(long idEspecialidadeMedica){
			
			try {
				Connection conn = getConnection();
				EspecialidadeMedica esp = new EspecialidadeMedica();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(esp)
					 .where(esp,esp.getIdentifierName())
					 .equal(idEspecialidadeMedica);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					rs.next();
					esp = new EspecialidadeMedica(rs.getString("tipoEspecialidade"));
				}
				if(esp.equals(new EspecialidadeMedica()))		return null;
				return esp;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<EspecialidadeMedica> getSpecialityList(){
			
			try {
				Connection conn = getConnection();
				EspecialidadeMedica esp = new EspecialidadeMedica();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(esp)
				     .groupBy("EspecialidadeMedica." + esp.getIdentifierName());
				List<EspecialidadeMedica> list = new ArrayList<>();
				try (PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						list.add(new EspecialidadeMedica(rs.getLong(esp.getIdentifierName()),rs.getString("tipoEspecialidade")));
					}
				}
				if(list.isEmpty())		return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<EspecialidadeMedica> getActiveSpecialityList(){
			
			try {
				Connection conn = getConnection();
				EspecialidadeMedica esp = new EspecialidadeMedica();
				Medico m = new Medico();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(esp)
					 .joinOn(esp, new Medico_EspecialidadeMedica(), esp.getIdentifierName())
					 .joinOn(new Medico_EspecialidadeMedica(), m, m.getIdentifierName())
					 .groupBy("EspecialidadeMedica." + esp.getIdentifierName());
				List<EspecialidadeMedica> list = new ArrayList<>();
				try (PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						list.add(new EspecialidadeMedica(rs.getLong(esp.getIdentifierName()),rs.getString("tipoEspecialidade")));
					}
				}
				if(list.isEmpty())		return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<EspecialidadeMedica> getSpecialityListByMedicID(long idMedico){
			
			try {
				Connection conn = getConnection();
				EspecialidadeMedica em = new EspecialidadeMedica();
				Medico m = new Medico();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(em)
					 .joinOn(em, new Medico_EspecialidadeMedica(), em.getIdentifierName())
					 .joinOn(new Medico_EspecialidadeMedica(), m, m.getIdentifierName())
					 .where(m,m.getIdentifierName()).equal(idMedico);
				List<EspecialidadeMedica> list = new ArrayList<>();
				try (PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						list.add(new EspecialidadeMedica(rs.getString("tipoEspecialidade")));
					}
				}
				if(list.isEmpty())		return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public long getSpecialityIDWithInsert(EspecialidadeMedica em){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(em)
					 .where("tipoEspecialidade")
					 .equal(em.getTipoEspecialidade());
				long idEspecialidade = -1;
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						idEspecialidade = rs.getLong(em.getIdentifierName());
					}else {
						query.endStatement();
						query.insert(em);
						try(Statement insertStmt = conn.createStatement()){
							insertStmt.executeUpdate(query.getSQL(),Statement.RETURN_GENERATED_KEYS);
							ResultSet rs1 = insertStmt.getGeneratedKeys();
							if(rs1.next()) {
								idEspecialidade = rs1.getLong(1);
							}
						}
					}
				}
				return idEspecialidade;
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public boolean registerMedicSpeciality(EspecialidadeMedica em){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder();
				query.insert(em);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					if(this.getIDBySpeciality(em) == -1)	return stmt.execute();
					return true;
				}
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
	}
