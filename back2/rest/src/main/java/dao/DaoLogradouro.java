package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entity.Logradouro;
import util.SQLBuilder;

	public class DaoLogradouro extends DefaultDao{

		public DaoLogradouro() {
			super();
		}
		
		public DaoLogradouro(Connection conn) {
			super(conn);
		}
		
		public long getIDByPlace(Logradouro lograd){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(lograd)
					.joinOn(lograd, lograd.getTipoLogradouro(), lograd.getTipoLogradouro().getIdentifierName()) 
					.where("nomeLogradouro").equal(lograd.getNomeLogradouro())
					.AND(lograd.getTipoLogradouro().getIdentifierName()).equal(lograd.getTipoLogradouro().getIdentifier())
					.AND("nomeTipo").equal(lograd.getTipoLogradouro().getNomeTipo());
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(!rs.first())		return -1;
					return rs.getLong(lograd.getIdentifierName());
				}
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public Logradouro getPlaceByID(long idLogradouro){
			
			try {
				Connection conn = getConnection();
				Logradouro l = new Logradouro();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(l)
					 .where(l,l.getIdentifierName())
					 .equal(idLogradouro);
				return null;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public Logradouro getPlaceByName(String nomeLogradouro){
			
			try {
				Connection conn = getConnection();
				Logradouro l = new Logradouro();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(l)
					 .where(l,"nomeLogradouro")
					 .equal(nomeLogradouro);
				return null;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<Logradouro> getPlaceList(){
			
			try {
				Connection conn = getConnection();
				Logradouro l = new Logradouro();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(l);
				List<Logradouro> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						list.add(new Logradouro());
					}
				}
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public long getPlaceIDWithInsert(Logradouro lograd){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder(conn);
				DaoTipoLogradouro daoTipo = new DaoTipoLogradouro(conn);
				String sigla = daoTipo.getInitialsByPlaceType(lograd.getTipoLogradouro());
				
				if(sigla != null) {
					lograd.getTipoLogradouro().setIdentifier(sigla);
				}else {
					lograd.getTipoLogradouro().setIdentifier("R");	
				}
				query.select("*").from(lograd)
					 .joinOn(lograd, lograd.getTipoLogradouro(), lograd.getTipoLogradouro().getIdentifierName())
					 .where("nomeLogradouro")
					 .equal(lograd.getNomeLogradouro())
					 .AND("nomeTipo")
					 .equal(lograd.getTipoLogradouro().getNomeTipo());
				System.out.println(query.getSQL());
				long idLogradouro = -1;
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						idLogradouro = rs.getLong(lograd.getIdentifierName());
					}else {
						query.endStatement();
						query.insert(lograd);
						try(Statement insertStmt = conn.createStatement()){
							insertStmt.executeUpdate(query.getSQL(),Statement.RETURN_GENERATED_KEYS);
							ResultSet rs1 = insertStmt.getGeneratedKeys();
							if(rs1.next()) {
								idLogradouro = rs1.getLong(1);
							}	
						}
					}
				}
				return idLogradouro;	 
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public boolean registerPlace(Logradouro l){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder();
				query.insert(l);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					DaoTipoLogradouro daoTipo = new DaoTipoLogradouro(conn);
					if(daoTipo.getInitialsByPlaceType(l.getTipoLogradouro()) == null) {
						
					}
					return true;
				}
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
	}
