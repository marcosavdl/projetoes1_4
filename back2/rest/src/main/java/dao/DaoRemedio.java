package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import entity.PrescricaoRemedio;
import entity.Remedio;
import util.SQLBuilder;

	public class DaoRemedio extends DefaultDao{

		public DaoRemedio() {
			super();
		}
		
		public DaoRemedio(Connection conn) {
			super(conn);
		}
		
		public Remedio getRemedyByID(long idRemedio){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder();
				Remedio r = new Remedio();
				query.select("*").from(r)
					 .where(r.getIdentifierName())
					 .equal(idRemedio);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						r = new Remedio(rs.getString("nomeRemedio"));
					}else {
						r = null;
					}
				}
				return r;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public long getIDByRemedy(Remedio r){
			
			try {
				Connection conn = getConnection();		
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(r)
					 .where("nomeRemedio")
					 .equal(r.getNomeRemedio());
				long id = -1;
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						id =  rs.getLong(r.getIdentifierName());
					}
				}
				return id;
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public List<Remedio> getRemedyList(){
	
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder();
				Remedio r = new Remedio();
				query.select("*").from(r);
				List<Remedio> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						list.add(new Remedio(rs.getString("nomeRemedio")));
					}
				}
				if(list.isEmpty())		return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<Remedio> getRemedyListByPrescriptionID(long idPrescricao){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder();
				Remedio r = new Remedio();
				PrescricaoRemedio p = new PrescricaoRemedio();
				query.select("*").from(r)
					 .joinOn(r, p, p.getIdentifierName())
					 .where(p.getIdentifierName()).equal(idPrescricao);
				List<Remedio> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						list.add(new Remedio(rs.getString("nomeRemedio")));
					}
				}
				if(list.isEmpty())		return list;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}
