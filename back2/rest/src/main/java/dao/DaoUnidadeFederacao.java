package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entity.UnidadeFederacao;
import util.SQLBuilder;

	public class DaoUnidadeFederacao extends DefaultDao{

		public DaoUnidadeFederacao() {
			super();
		}
		
		public DaoUnidadeFederacao(Connection conn) {
			super(conn);
		}
		
		public String getInitialsByUf(UnidadeFederacao uf) {
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(uf)
					 .where("nomeUF").equal(uf.getNomeUF());
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						return rs.getString(uf.getIdentifierName());
					}else {
						return null;
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<UnidadeFederacao> getStateList(){
			
			try {
				Connection conn = getConnection();
				UnidadeFederacao uf = new UnidadeFederacao();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(uf);
				List<UnidadeFederacao> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						list.add(new UnidadeFederacao(rs.getString(uf.getIdentifierName()),rs.getString("nomeUF")));
					}
				}
				if(list.isEmpty())		return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public UnidadeFederacao getUFByInitials(String sigla){
			
			try {
				Connection conn = getConnection();
				UnidadeFederacao uf = new UnidadeFederacao();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(uf)
					.where(uf,uf.getIdentifierName())
					.equal(sigla);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					ResultSet rs = stmt.executeQuery();
					rs.next();
					uf = new UnidadeFederacao(rs.getString(uf.getIdentifierName()),rs.getString("nomeUF"));
				}
				if(uf.equals(new UnidadeFederacao()))	return null;
				return uf;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public UnidadeFederacao getUFByName(String nomeUF){
			
			try {
				Connection conn = getConnection();
				UnidadeFederacao uf = new UnidadeFederacao();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(uf)
					 .where(uf,"nomeUF")
					 .equal(nomeUF);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					rs.next();
					uf = new UnidadeFederacao(rs.getString(uf.getIdentifierName()),rs.getString("nomeUF"));
				}
				if(uf.equals(new UnidadeFederacao()))	return null;
				return uf;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public String getInitialsWithInsert(UnidadeFederacao uf){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder(conn);			
				query.select("*").from(uf)
					.where("siglaUF")
					.equal(uf.getIdentifier());
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						return rs.getString(uf.getIdentifierName());
					}else {
						query.endStatement();
						// TODO Lógica não funcional para inserção de medico para registrar lista de especialidades médicas
					}
					
				}
				
				/*
				 * Considerar que todos os estados já estejam pré-inseridos ou pré-atribuídos
				 */
				
				switch((String)uf.getIdentifier()) {
				case "PR": uf.setNomeUF("Paraná");
				break;
				case "SP": uf.setNomeUF("São Paulo");
				break;
				default:	uf.setNomeUF("Amazonas");
				}
				query.insert(uf);
				try(Statement stmt = conn.createStatement()){
					System.out.println(query.getSQL());
					stmt.executeUpdate(query.getSQL());
					query.endStatement();
					query.select("*").from(uf)
						 .where("nomeUF")
						 .equal(uf.getNomeUF());
					ResultSet rs = conn.prepareStatement(query.getSQL()).executeQuery();
					if(rs.next()) {
						return rs.getString(uf.getIdentifierName());
					}
				}
				return null;
			}catch(Exception e) {
				e.printStackTrace();
				if(uf.getIdentifier() != null)	return (String) uf.getIdentifier();
				return null;
			}
		}
		
		public boolean registerUF(UnidadeFederacao uf){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder();
				query.insert(uf);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					if(this.getInitialsByUf(uf) == null)	return stmt.execute();
					return true;
				}
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
	}
