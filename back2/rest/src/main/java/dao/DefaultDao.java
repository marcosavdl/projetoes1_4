package dao;

import java.sql.Connection;

	public class DefaultDao {

		private Connection conn;
		 
		public DefaultDao() {
			super();
		}
		
		public DefaultDao(Connection conn) {
			super();
			this.conn = conn;
		}
		
		public Connection getConnection() {
			return conn;
		}
		
		public void setConnection(Connection conn) {
			this.conn = conn;
		}
	}
