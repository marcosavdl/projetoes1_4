package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import entity.ConsultaMedica;
import entity.PrescricaoRemedio;
import util.SQLBuilder;

	public class DaoPrescricaoRemedio extends DefaultDao{

		public DaoPrescricaoRemedio() {
			super();
		}
		
		public DaoPrescricaoRemedio(Connection conn) {
			super(conn);
		}
		
		public PrescricaoRemedio getRemedyPrescriptionByID(long idPrescricaoRemedio){
			
			try {
				Connection conn = getConnection();
				PrescricaoRemedio pr = new PrescricaoRemedio();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(pr)
					 .where(pr,pr.getIdentifierName()).equal(idPrescricaoRemedio);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
//						pr = new PrescricaoRemedio(rs.getString("nomeRemedio"),rs.getString("posologia"));
					}
				}
				if(pr.equals(new PrescricaoRemedio()))		return null;
				return pr;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<PrescricaoRemedio> getRemedyPrescriptionList(){
			
			try {
				Connection conn = getConnection();
				PrescricaoRemedio pr = new PrescricaoRemedio();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(pr);
				List<PrescricaoRemedio> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
//						list.add(new PrescricaoRemedio(rs.getString("nomeRemedio"),rs.getString("posologia")));
					}
				}
				if(list.isEmpty())	return null;
				return list;
			}catch(Exception e) {
				return null;
			}
		}
		
		public List<PrescricaoRemedio> getRemedyPrescriptionListByAppopintmentID(long idConsultaMedica){
			
			try {
				Connection conn = getConnection();
				PrescricaoRemedio pr = new PrescricaoRemedio();
				ConsultaMedica cm = new ConsultaMedica();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(pr)
					 .joinOn(pr, cm, cm.getIdentifierName())
					 .where(cm,cm.getIdentifierName()).equal(idConsultaMedica);
				List<PrescricaoRemedio> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						/*
						list.add(new PrescricaoRemedio(rs.getString("nomeRemedio"),rs.getString("posologia"),
								new ConsultaMedica(rs.getInt("nroConsulta"),rs.getString("dataConsulta"))));
						*/
					}
				}
				if(list.isEmpty())	return null;
				return list;
			}catch(Exception e) {
				return null;
			}
		}
			
	}
