package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import entity.TipoLogradouro;
import util.SQLBuilder;

	public class DaoTipoLogradouro extends DefaultDao{
		
		public DaoTipoLogradouro() {
			super();
		}
		
		public DaoTipoLogradouro(Connection conn) {
			super(conn);
		}
	
		public String getInitialsByPlaceType(TipoLogradouro tipoLograd){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(tipoLograd)
					 .where("nomeTipo")
					 .equal(tipoLograd.getNomeTipo());
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						return (String) tipoLograd.getIdentifier();
					}else {
						return null;
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public String getPlaceTypeNameByInitials(String sigla){
			
			try {
				Connection conn = getConnection();
				TipoLogradouro tl = new TipoLogradouro();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(tl)
					 .where(tl,tl.getIdentifierName())
					 .equal(sigla);
				String result = "";
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					rs.next();
					result = rs.getString("nomeTipo");
				}
				if(result.isBlank())	return null;
				return result;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<TipoLogradouro> getPlaceTypeList(){
			
			try {
				Connection conn = getConnection();
				TipoLogradouro tl = new TipoLogradouro();
				SQLBuilder query = new SQLBuilder();
				query.select("*").from(tl);
				List<TipoLogradouro> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						list.add(new TipoLogradouro(rs.getString(tl.getIdentifierName()),
													rs.getString("nomeTipo")));
					}
				}
				if(list.isEmpty())	return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public boolean registerPlaceType(TipoLogradouro tipoLograd){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder();
				query.insert(tipoLograd);
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					System.out.println(stmt);
					if(this.getInitialsByPlaceType(tipoLograd) == null)	return stmt.execute();
					return true;
				}
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
	}
