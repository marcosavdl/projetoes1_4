package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entity.ConsultaMedica;
import entity.EspecialidadeMedica;
import entity.Medico;
import entity.Paciente;
import entity.PrescricaoRemedio;
import entity.Remedio;
import util.Formatter;
import util.SQLBuilder;

	public class DaoConsultaMedica extends DefaultDao{

		public DaoConsultaMedica() {
			super();
		}
		
		public DaoConsultaMedica(Connection conn) {
			super(conn);
		}
		
		public ConsultaMedica getMedicAppointmentByID(long idConsultaMedica){
			
			try {
				Connection conn = getConnection();
				ConsultaMedica cm = new ConsultaMedica();
				SQLBuilder query = new SQLBuilder(conn);
				query.select("*").from(cm)
				 .joinOn(cm, cm.getMedico(), cm.getMedico().getIdentifierName())
				 .joinOn(cm, cm.getPaciente(), cm.getPaciente().getIdentifierName())
				 .joinOnAliased("cm",cm, cm.getEspecialidadeMedica(), cm.getEspecialidadeMedica().getIdentifierName())
				 .where(cm.getIdentifierName()).equal(idConsultaMedica); 
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					if(rs.next()) {
						DaoMedico daoMed = new DaoMedico(conn);
						DaoPaciente daoPac = new DaoPaciente(conn);
						DaoEspecialidadeMedica daoEsp = new DaoEspecialidadeMedica(conn);
						cm = new ConsultaMedica(rs.getLong(cm.getIdentifierName()),rs.getInt("nroConsulta"),rs.getString("dataConsulta"),
								daoMed.getMedicByID(rs.getLong(cm.getMedico().getIdentifierName())),
								daoPac.getPatientByID(rs.getLong(cm.getPaciente().getIdentifierName())),
								daoEsp.getSpecialityByID(rs.getLong(cm.getEspecialidadeMedica().getIdentifierName()))
								);
					}
				}
				if(cm.equals(new ConsultaMedica()))		return null;
				return cm;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<ConsultaMedica> getMedicAppointmentList(){
			
			try {
				Connection conn = getConnection();
				ConsultaMedica cm = new ConsultaMedica();
				SQLBuilder query = new SQLBuilder(conn);
				DaoMedico daoMed = new DaoMedico(conn);
				DaoPaciente daoPac = new DaoPaciente(conn);
				DaoEspecialidadeMedica daoEsp = new DaoEspecialidadeMedica(conn);
				
				query.select("*").from(cm)
				 	.joinOn(cm, cm.getMedico(), cm.getMedico().getIdentifierName())
				 	.joinOn(cm, cm.getPaciente(), cm.getPaciente().getIdentifierName())
				 	.joinOnAliased("cm",cm, cm.getEspecialidadeMedica(), cm.getEspecialidadeMedica().getIdentifierName());
				
				List<ConsultaMedica> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						Medico m = daoMed.getMedicByID(rs.getLong(cm.getMedico().getIdentifierName()));
						Paciente p = daoPac.getPatientByID(rs.getLong(cm.getPaciente().getIdentifierName()));
						EspecialidadeMedica esp = daoEsp.getSpecialityByID(rs.getLong(cm.getIdentifierName()));
						list.add(new ConsultaMedica(rs.getLong(cm.getIdentifierName()),
													rs.getInt("nroConsulta"),
													Formatter.convertSQLToReal_DateTime(rs.getString("dataConsulta")),
													m, p, esp
													)
								);
					}
				}
				if(list.isEmpty())		return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<ConsultaMedica> getMedicAppointmentListByPatientID(long idPaciente){
			
			try {
				Connection conn = getConnection();
				ConsultaMedica cm = new ConsultaMedica();
				SQLBuilder query = new SQLBuilder(conn);
				DaoMedico daoMed = new DaoMedico(conn);
				DaoPaciente daoPac = new DaoPaciente(conn);
				DaoEspecialidadeMedica daoEsp = new DaoEspecialidadeMedica(conn);
				
				query.select("*").from(cm)
				 	.joinOn(cm, cm.getMedico(), cm.getMedico().getIdentifierName())
				 	.joinOn(cm, cm.getPaciente(), cm.getPaciente().getIdentifierName())
				 	.joinOnAliased("cm",cm, cm.getEspecialidadeMedica(), cm.getEspecialidadeMedica().getIdentifierName()) 
				 	.where("Paciente." + cm.getPaciente().getIdentifierName()).equal(idPaciente);
				
				System.out.println(query.getSQL());
				List<ConsultaMedica> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						Medico m = daoMed.getMedicByID(rs.getLong(cm.getMedico().getIdentifierName()));
						Paciente p = daoPac.getPatientByID(rs.getLong(cm.getPaciente().getIdentifierName()));
						EspecialidadeMedica esp = daoEsp.getSpecialityByID(rs.getLong(cm.getIdentifierName()));
						list.add(new ConsultaMedica(rs.getLong(cm.getIdentifierName()),rs.getInt("nroConsulta"),
													Formatter.convertSQLToReal_DateTime(rs.getString("dataConsulta")),
													m, p, esp
													)
								);
					}
				}
				if(list.isEmpty())		return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<ConsultaMedica> getMedicAppointmentListByMedicID(long idMedico){
			
			try {
				Connection conn = getConnection();
				ConsultaMedica cm = new ConsultaMedica();
				SQLBuilder query = new SQLBuilder(conn);
				DaoMedico daoMed = new DaoMedico(conn);
				DaoPaciente daoPac = new DaoPaciente(conn);
				DaoEspecialidadeMedica daoEsp = new DaoEspecialidadeMedica(conn);
				query.select("*").from(cm)
				 	.joinOn(cm, cm.getMedico(), cm.getMedico().getIdentifierName())
				 	.joinOn(cm, cm.getPaciente(), cm.getPaciente().getIdentifierName())
				 	.joinOnAliased("cm",cm, cm.getEspecialidadeMedica(), cm.getEspecialidadeMedica().getIdentifierName()) 
				 	.where("Medico." + cm.getMedico().getIdentifierName()).equal(idMedico);
				System.out.println(query.getSQL());
				List<ConsultaMedica> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
						Medico m = daoMed.getMedicByID(rs.getLong(cm.getMedico().getIdentifierName()));
						Paciente p = daoPac.getPatientByID(rs.getLong(cm.getPaciente().getIdentifierName()));
						EspecialidadeMedica esp = daoEsp.getSpecialityByID(rs.getLong(cm.getIdentifierName()));
						list.add(new ConsultaMedica(rs.getLong(cm.getIdentifierName()), rs.getInt("nroConsulta"),
													Formatter.convertSQLToReal_DateTime(rs.getString("dataConsulta")),
													m, p, esp
													)
								);
					}
				}
				if(list.isEmpty())		return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public List<Remedio> getRemedyListByMedicAppointmentID(long idConsultaMedica){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder(conn);
				Remedio r = new Remedio();
				PrescricaoRemedio pr = new PrescricaoRemedio();
				ConsultaMedica cm = new ConsultaMedica();
				query.select("*").from(r)
					 .joinOn(r, pr, pr.getIdentifierName())
					 .joinOn(pr, cm, cm.getIdentifierName())
					 .where(cm.getIdentifierName()).equal(idConsultaMedica)
					 ;
				System.out.println(query.getSQL());
				List<Remedio> list = new ArrayList<>();
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					while(rs.next()) {
//						list.add(new Remedio());
					}
				}
				if(list.isEmpty())		return null;
				return list;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public long getMedicAppointmentQuantity(){
			
			try {
				Connection conn = getConnection();
				SQLBuilder query = new SQLBuilder(conn);
				ConsultaMedica cm = new ConsultaMedica();
				query.selectCount(cm.getIdentifierName())
					 .from(cm);
				long count = -1;
				try(PreparedStatement stmt = conn.prepareStatement(query.getSQL())){
					ResultSet rs = stmt.executeQuery();
					rs.next();
					count = rs.getLong("COUNT(" + cm.getIdentifierName() + ")");
				}
				return count;
			}catch(Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public boolean registerMedicAppointment(ConsultaMedica cm){
			
			try {
				Connection conn = getConnection();
//				DaoPaciente daoPac = new DaoPaciente(conn);
				DaoMedico daoMed = new DaoMedico(conn);	
				DaoEspecialidadeMedica daoEsp = new DaoEspecialidadeMedica(conn);

				long idMed, idPac, idEsp;
				idMed = idPac = idEsp = -1;
				idMed = daoMed.getMedicIDWithInsert(cm.getMedico());
//				idPac = daoPac.get
				idEsp = daoEsp.getSpecialityIDWithInsert(cm.getEspecialidadeMedica());	
				if(idMed == -1 || idPac == -1 || idEsp == -1) {
					return false;
				}else {
					SQLBuilder query = new SQLBuilder(conn);
					query.insert(cm);
					System.out.println(query.getSQL());
					try(Statement stmt = conn.createStatement()){
						stmt.execute(query.getSQL(),Statement.RETURN_GENERATED_KEYS);
						ResultSet rs = stmt.getGeneratedKeys();
						if(rs.next()) {
							cm.setIdConsultaMedica(rs.getLong(1));
							System.out.println(cm.getIdConsultaMedica());
							return true;
						}
					}
					return false;
				}
				/* 
				 * Buscar entidades
				 * Fazer select primeiro
				 * Retornar false ou inserir IDs em consulta e realizar insercao de consulta 
				 */
				
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
	}
