package resources;

import dao.DaoCidade;
import entity.Cidade;
import entity.UnidadeFederacao;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

	@Path("/city")
	public class ResourcesCidade extends DefaultResources{
	
		private DaoCidade dao;
		
		public ResourcesCidade() {
			super();
			dao = new DaoCidade();
		}
		
		@GET
		@Path("/getAll")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaCidade() throws Exception {
			return this.entityToString(dao.getCityList());
		}
		
		@GET
		@Path("/getListByUf/{state}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaCidade_porUF(@PathParam("state") String siglaUF) throws Exception{
			return this.entityToString(dao.getCityListByUF(new UnidadeFederacao(siglaUF,"")));
		}
		
		@PUT
		@Path("/insert")
		@Consumes (MediaType.APPLICATION_JSON)
		public String cadastrarCidade(Cidade cidade) throws Exception{
			return this.entityToString(dao.registerCity(cidade));
		}
	}
