package resources;

import dao.DaoPaciente;
import entity.Paciente;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

	@Path("/patient")
	public class ResourcesPaciente extends DefaultResources{
	
		private DaoPaciente dao;
		
		public ResourcesPaciente() {
			super();
			dao = new DaoPaciente(this.getConnection());
		}
		
		@GET
		@Path("/get/{patientID}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterPaciente(@PathParam("patientID") long id) throws Exception {
		
			return this.entityToString(dao.getPatientByID(id));
		}
		
		@GET
		@Path("/getAll")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaPaciente() throws Exception{
			
			return this.entityToString(dao.getPatientList());	
		}
		
		@GET
		@Path("/getAllActive")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaPacienteAtivo() throws Exception{
			
			return this.entityToString(dao.getActivePatientList());	
		}
		
		@GET
		@Path("/quantity")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterQuantidadePaciente() throws Exception{
			
			return this.entityToString(dao.getPatientQuantity());
		}
		
		@POST
		@Path("/insert")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String cadastrarPaciente(Paciente paciente) throws Exception{
			
			System.out.println("Paciente = " + paciente);
			return this.entityToString(dao.registerPatient(paciente));
		}
		
		@PUT
		@Path("/deactivate/{patientID}")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String desativarPaciente(@PathParam("patientID") long idPaciente) throws Exception{
			
			return this.entityToString(dao.deactivatePatient(idPaciente));
		}
		/*
		@POST
		@Path("/getID")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String getID(Paciente p) throws Exception{
			
			return this.entityToString(dao.getIDByPatient(p));
		}
		*/
	}
