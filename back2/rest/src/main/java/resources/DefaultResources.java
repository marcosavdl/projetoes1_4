package resources;

import java.sql.Connection;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import connection.ConnectionFactory;
import jakarta.ws.rs.Path;

	@Path("/")
	public class DefaultResources {

		private Gson gson;
		private Connection conn;
		
		public DefaultResources() {
			gson = new GsonBuilder().setLenient().create();
			try {
				conn = ConnectionFactory.getConnection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				conn = null;
			}
		}
		
		public DefaultResources(Connection conn) {
			gson = new GsonBuilder().setLenient().create();
			this.conn = conn;
		}
		
		public String entityToString(Object object) {

			return gson.toJson(object);
		}
		
		public Connection getConnection() {
			return conn;
		}
		
		public void setConnection(Connection conn) {
			this.conn = conn;
		}
		
		/*
		@GET
		@Produces (MediaType.TEXT_PLAIN)
		public String sayPlainTextHello() {
			return "Hi to the Server";
		}
		*/
	}
