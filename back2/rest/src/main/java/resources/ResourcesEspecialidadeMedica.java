package resources;

import dao.DaoEspecialidadeMedica;
import entity.EspecialidadeMedica;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

	@Path("/speciality")
	public class ResourcesEspecialidadeMedica extends DefaultResources{
	
		private DaoEspecialidadeMedica dao;
		
		public ResourcesEspecialidadeMedica() {
			super();
			dao = new DaoEspecialidadeMedica(this.getConnection());
		}
		
		@GET
		@Path("/getAll")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaEspecialidadeMedica() throws Exception {
			return this.entityToString(dao.getSpecialityList());
		}
		
		@GET
		@Path("/getAllActive")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaEspecialidadeMedicaAtiva() throws Exception{
			return this.entityToString(dao.getActiveSpecialityList());
		}
		
		@GET
		@Path("/getByMedic/{medicID}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterEspecialidadeMedica_porMedico(@PathParam("medicID") long idMedico) throws Exception{
			
			return this.entityToString(dao.getSpecialityListByMedicID(idMedico));
		}
		
		@POST
		@Path("/insert")
		@Consumes (MediaType.APPLICATION_JSON)
		public String cadastrarEspecialidadeMedica(EspecialidadeMedica em) throws Exception{
			
			return this.entityToString(dao.registerMedicSpeciality(em));
		}
		
		@POST
		@Path("/getInsert")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String getInsert(EspecialidadeMedica em) throws Exception{
			
			return this.entityToString(dao.getSpecialityIDWithInsert(em));
		}
		
	}
