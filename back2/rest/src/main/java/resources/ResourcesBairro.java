package resources;

import dao.DaoBairro;
import entity.Bairro;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

	@Path("/district")
	public class ResourcesBairro extends DefaultResources{

		private DaoBairro dao;
	
		public ResourcesBairro() {
			super();
			dao = new DaoBairro(this.getConnection());
		}
		
		@GET
		@Path("/getAll")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaBairro() throws Exception {
			
			return this.entityToString(dao.getDistrictList());
		}
		
		@POST
		@Path("/insert")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String cadastrarBairro(Bairro b) throws Exception{

			return this.entityToString(dao.registerDistrict(b));
		}
		
		@PUT
		@Path("/update")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String atuaizarBairro(Bairro b) throws Exception{
			
			return this.entityToString(dao.updateDistrict(b));
		}
		
		@POST
		@Path("/insertGet")
		@Consumes (MediaType.APPLICATION_JSON)
		public String getInsert(Bairro b) throws Exception{
			
			return this.entityToString(dao.getDistrictIDWithInsert(b));
		}
		
	}
