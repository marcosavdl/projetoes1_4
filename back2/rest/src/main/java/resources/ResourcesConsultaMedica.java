package resources;

import dao.DaoConsultaMedica;
import entity.ConsultaMedica;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

	@Path("/appointment")
	public class ResourcesConsultaMedica extends DefaultResources{

		private DaoConsultaMedica dao;
		
		public ResourcesConsultaMedica() {
			super();
			dao = new DaoConsultaMedica(this.getConnection());
		}
		
		@GET
		@Path("/getAll")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaConsultaMedica() {
			
			return entityToString(dao.getMedicAppointmentList());
		}
		
		@GET
		@Path("/getMedicHistory/{medicID}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterHistoricoConsultaMedica_porMedico(@PathParam("medicID") long idMedico) {
		
			
			return entityToString(dao.getMedicAppointmentListByMedicID(idMedico));
		}
		
		@GET
		@Path("/getPatientHistory/{patientID}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterHistoricoConsultaMedica_porPaciente(@PathParam("patientID") long idPaciente) {
		
			return entityToString(dao.getMedicAppointmentListByPatientID(idPaciente));
		}
		
		@GET
		@Path("/quantity")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterQuantidadeConsultaMedica() {
			
			return entityToString(dao.getMedicAppointmentQuantity());
		}
		
		@GET
		@Path("/remedyList/{appointmentID}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaRemedio_porConsulta(@PathParam("appointmentID") long idConsultaMedica) {
			
			return entityToString(dao.getRemedyListByMedicAppointmentID(idConsultaMedica));
		}
		
		@POST
		@Path("/insert")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String cadastrarConsultaMedica(ConsultaMedica consultaMedica) throws Exception{
			
			return this.entityToString(dao.registerMedicAppointment(consultaMedica));
		}
		
	}
