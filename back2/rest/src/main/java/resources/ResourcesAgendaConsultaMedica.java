package resources;

import dao.DaoAgendaConsultaMedica;
import entity.AgendaConsultaMedica;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

	@Path("/appointSchedule")
	public class ResourcesAgendaConsultaMedica extends DefaultResources{
	
		private DaoAgendaConsultaMedica dao;

		public ResourcesAgendaConsultaMedica() {
			super();
			dao = new DaoAgendaConsultaMedica(this.getConnection());
		}
		
		@GET
		@Path("/getAll")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaAgendaConsultaMedica() throws Exception{
			
			return entityToString(dao.getAppointScheduleList());
		}
		
		@GET
		@Path("/quantity")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterQuantidadeConsultaAgendada() throws Exception{
			
			return entityToString(dao.getAppointScheduleQuantity());
		}
		
		@POST
		@Path("/insert")
		@Produces (MediaType.APPLICATION_JSON)
		public String agendarConsulta(AgendaConsultaMedica agendaConsulta) throws Exception{
			
			return entityToString(dao.registerAppointSchedule(agendaConsulta));
		}
	}
