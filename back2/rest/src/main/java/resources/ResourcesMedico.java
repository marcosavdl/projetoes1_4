package resources;

import dao.DaoMedico;
import entity.Medico;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

	@Path("/medic")
	public class ResourcesMedico extends DefaultResources {

		private DaoMedico dao;
		
		public ResourcesMedico() {
			super();
			dao = new DaoMedico(this.getConnection());
		}
		
		@GET
		@Path("/get/{medicID}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterMedico(@PathParam("medicID") long idMedico) throws Exception {
			
			return entityToString(dao.getMedicByID(idMedico));
		}
		
		@GET
		@Path("/getAll")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaMedico() throws Exception{
			
			return entityToString(dao.getMedicList());
		}
		
		@GET
		@Path("/getAllActive")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaMedicoAtivo() throws Exception{
			
			return entityToString(dao.getActiveMedicList());
		}
		
		@GET
		@Path("getListBySpeciality/{specialityID}")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaMedico_porIDEspecialidadeMedica(@PathParam("specialityID") 
																long idEspecialiadeMedica) throws Exception{
			
			return entityToString(dao.getMedicListBySpecialityID(idEspecialiadeMedica));
		}
		
		@GET
		@Path("/quantity")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterQuantidadeMedico() throws Exception{
			
			return entityToString(dao.getMedicQuantity());
		}
		
		@POST
		@Path("/insert")
		@Consumes (MediaType.APPLICATION_JSON)
		public String cadastrarMedico(Medico m) throws Exception{
			
			return entityToString(dao.registerMedic(m));
		}
		
		@PUT
		@Path("/deactivate/{medicID}")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String desativarMedico(@PathParam("medicID") long idMedico) throws Exception{
			
			return entityToString(dao.deactivateMedic(idMedico));
		}
		
		@POST
		@Path("/insertSpecialityList")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String insertSpeciality(Medico m) throws Exception{
			
			return entityToString(dao.registerMedicSpecialityList(m));
		}
		
		@POST
		@Path("/getInsert")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String getInsert(Medico m) throws Exception{
			return entityToString(dao.getMedicIDWithInsert(m));
		}
	}
