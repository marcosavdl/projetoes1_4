package resources;

import dao.DaoLogradouro;
import entity.Logradouro;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
	
	public class ResourcesLogradouro extends DefaultResources{
	
		private DaoLogradouro dao;
	
		public ResourcesLogradouro() {
			super();
			dao = new DaoLogradouro();
		}
		
		@GET
		@Path("/getAll")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaLogradouro() throws Exception{
			
			return this.entityToString(dao.getPlaceList());
		}
		
		@POST
		@Path("/insert")
		@Produces (MediaType.APPLICATION_JSON)
		@Consumes (MediaType.APPLICATION_JSON)
		public String cadastrarLogradouro(Logradouro l) throws Exception{
			
			return this.entityToString(dao.registerPlace(l));
		}
		
	}