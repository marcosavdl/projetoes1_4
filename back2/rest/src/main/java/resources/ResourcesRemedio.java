package resources;

import dao.DaoRemedio;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

	@Path("/remedy")
	public class ResourcesRemedio extends DefaultResources{

		private DaoRemedio dao;

		public ResourcesRemedio() {
			super();
			dao = new DaoRemedio(this.getConnection());
		}
		
		@GET
		@Path("/getAll")
		@Produces (MediaType.APPLICATION_JSON)
		public String obterListaRemedio() {
			return entityToString(dao.getRemedyList());
		}
	}
