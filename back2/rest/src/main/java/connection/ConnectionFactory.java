package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

	public class ConnectionFactory {
	
		public static String DATABASE_TYPE = "mysql";
		private String url;
		private String userName;
		private String password;
		private static ConnectionFactory instance;
		private String driver;
		
		private ConnectionFactory()	{
			
		}
		
		private ConnectionFactory(Properties properties)	{
			
			try {
				setConnectionData(properties);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public static void setConnectionData(Properties properties) throws Exception{
			
			if(instance == null)
				instance= new ConnectionFactory();
			//protocol= protocol.trim();
			instance.driver= properties.getProperty("_driver");
			// Class.forName(instance.driver);
			instance.url= properties.getProperty("_host");
			instance.userName= properties.getProperty("_username");
			instance.password= properties.getProperty("_password");
			System.out.println("driver: "+instance.driver);		
			System.out.println("url: "+ instance.url);
			System.err.println(instance.url + " " + instance.userName + " " + instance.password);
			DriverManager.getConnection(instance.url, instance.userName, instance.password);
		}
		
		/*
		public static Connection getConnection() throws Exception{
			
			Connection con = null;		
			if(instance == null)
				return null;
			con = DriverManager.getConnection(instance.url, instance.userName, instance.password);
			return con;
		}
		*/
		public static Connection getConnection() throws Exception{
			
			Connection conn = null;
			try {
				String url, host, port, dbName, username, password;
				host = "localhost";
				port = "3306";
				dbName = "mydb";
				username = "root";
				password = "123456";
				url = "jdbc:mysql://" + host + ":" + port + "/" + dbName; 
				Class.forName("com.mysql.cj.jdbc.Driver");
//				Class.forName("org.mariadb.jdbc.Driver");
				conn = DriverManager.getConnection(url,username,password);
//				System.out.println("Connection Settings = " + conn.toString());
			}catch(Exception e) {
				e.printStackTrace();
			}
			return conn;
		}
		
		/*
		public static void test_select() {
			
			try {
				Gson gsonBuilder = new Gson();
				Properties p = new Properties();
				p.put("_driver", "com.mysql.jdbc.Driver");
				p.put("_host", "jdbc:mysql://localhost:3306/mydb");
				p.put("_username", "user");
				p.put("_password", "123456");
				setConnectionData(p);
				
				Connection c = getConnection();
				Statement st = c.createStatement();
				ResultSet rs = st.executeQuery("select * from teste");
				while(rs.next()) {
				}
				c.close();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		/*
		public static void main(String[] args) {
		
			Properties p = new Properties();
			p.put("_driver", "com.mysql.jdbc.Driver");
			p.put("_host", "jdbc:mysql://localhost:3306/mydb");
			p.put("_username", "user");
			p.put("_password", "123456");
			try {
				ConnectionFactory.setConnectionData(p);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ConnectionFactory.test_insert();
			Properties p = new Properties();
			p.put("_driver", "com.mysql.jdbc.Driver");
			p.put("_host", "jdbc:mysql://localhost:3306/mydb");
			p.put("_username", "user");
			p.put("_password", "123456");
			try {
				ConnectionFactory.setConnectionData(p);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
		}
		*/
	}
