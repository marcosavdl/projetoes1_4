package entity;

	public class EspecialidadeMedica extends Entity{

		private String tipoEspecialidade;
	
		public EspecialidadeMedica() {
			super("idEspecialidadeMedica");
		}
		
		public EspecialidadeMedica(long idEspecialidadeMedica) {
			super("idEspecialidadeMedica",idEspecialidadeMedica);
		}
		
		public EspecialidadeMedica(String tipoEspecialidade) {
			super("idEspecialidadeMedica");
			this.tipoEspecialidade = tipoEspecialidade;
		}
		
		public EspecialidadeMedica(long idEspecialidadeMedica, String tipoEspecialidade) {
			super("idEspecialidadeMedica",idEspecialidadeMedica);
			this.tipoEspecialidade = tipoEspecialidade;
		}

		public long getIdEspecialidadeMedica() {
			return (long) super.getIdentifier();
		}
		
		public void setIdEspecialidadeMedica(long idEspecialidadeMedica) {
			super.setIdentifier(idEspecialidadeMedica);
		}
		
		public String getTipoEspecialidade() {
			return tipoEspecialidade;
		}

		public void setTipoEspecialidade(String tipoEspecialidade) {
			this.tipoEspecialidade = tipoEspecialidade;
		}
		
		
	}

