package entity;

	public class TipoLogradouro extends Entity{
	
		private String nomeTipo;

		public TipoLogradouro() {
			super("siglaTipoLogradouro");
		}
		
		public TipoLogradouro(String nomeTipo) {
			super("siglaTipoLogradouro");
			this.nomeTipo = nomeTipo;
		}
		
		public TipoLogradouro(String siglaTipoLogradouro, String nomeTipo) {
			super("siglaTipoLogradouro",siglaTipoLogradouro);
			this.nomeTipo = nomeTipo;
		}

		public String getSiglaTipoLogradouro() {
			return (String) super.getIdentifier();
		}
		
		public void setSiglaTipoLogradouro(String siglaTipoLogradouro) {
			super.setIdentifier(siglaTipoLogradouro);
		}
		
		public String getNomeTipo() {
			return nomeTipo;
		}

		public void setNomeTipo(String nomeTipo) {
			this.nomeTipo = nomeTipo;
		}
		
	}
