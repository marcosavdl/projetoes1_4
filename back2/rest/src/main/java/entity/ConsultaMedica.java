package entity;

	public class ConsultaMedica extends Entity{
	
		private int nroConsulta;
		private String dataConsulta;
		private Medico medico;
		private Paciente paciente;
		private EspecialidadeMedica especialidadeMedica;
		
		public ConsultaMedica() {
			super("idConsultaMedica");
			medico = new Medico();
			paciente = new Paciente();
			especialidadeMedica = new EspecialidadeMedica();
		}
		
		public ConsultaMedica(long idConsultaMedica) {
			super("idConsultaMedica",idConsultaMedica);
			medico = new Medico();
			paciente = new Paciente();
			especialidadeMedica = new EspecialidadeMedica();
		}

		public ConsultaMedica(int nroConsulta, String dataConsulta, Medico medico, Paciente paciente, EspecialidadeMedica especialidadeMedica) {
			super("idConsultaMedica");
			this.nroConsulta = nroConsulta;
			this.dataConsulta = dataConsulta;
			this.medico = medico;
			this.paciente = paciente;
			this.especialidadeMedica = especialidadeMedica;
		}
		
		public ConsultaMedica(long idConsultaMedica, int nroConsulta, String dataConsulta, Medico medico, Paciente paciente, EspecialidadeMedica especialidadeMedica) {
			super("idConsultaMedica",idConsultaMedica);
			this.nroConsulta = nroConsulta;
			this.dataConsulta = dataConsulta;
			this.medico = medico;
			this.paciente = paciente;
			this.especialidadeMedica = especialidadeMedica;
		}

		public long getIdConsultaMedica() {
			return (long) super.getIdentifier();
		}
		
		public void setIdConsultaMedica(long idConsultaMedica) {
			super.setIdentifier(idConsultaMedica);
		}
		
		public int getNroConsulta() {
			return nroConsulta;
		}

		public void setNroConsulta(int nroConsulta) {
			this.nroConsulta = nroConsulta;
		}

		public String getDataConsulta() {
			return dataConsulta;
		}

		public void setDataConsulta(String dataConsulta) {
			this.dataConsulta = dataConsulta;
		}

		public Medico getMedico() {
			return medico;
		}

		public void setMedico(Medico medico) {
			this.medico = medico;
		}

		public Paciente getPaciente() {
			return paciente;
		}

		public void setPaciente(Paciente paciente) {
			this.paciente = paciente;
		}

		public EspecialidadeMedica getEspecialidadeMedica() {
			return especialidadeMedica;
		}

		public void setEspecialidadeMedica(EspecialidadeMedica especialidadeMedica) {
			this.especialidadeMedica = especialidadeMedica;
		}
		
	}
