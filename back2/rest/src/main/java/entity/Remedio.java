package entity;

	public class Remedio extends Entity{

		private String nomeRemedio;
		
		public Remedio() {
			super("idRemedio");
		}
		
		public Remedio(long idRemedio) {
			super("idRemedio",idRemedio);
		}
		
		public Remedio(String nomeRemedio) {
			super("idRemedio");
			this.nomeRemedio = nomeRemedio;
		}
		
		public Remedio(long idRemedio, String nomeRemedio) {
			super("idRemedio",idRemedio);
			this.nomeRemedio = nomeRemedio;
		}
		
		public String getNomeRemedio() {
			return nomeRemedio;
		}

		public void setNomeRemedio(String nomeRemedio) {
			this.nomeRemedio = nomeRemedio;
		}
	
	}
