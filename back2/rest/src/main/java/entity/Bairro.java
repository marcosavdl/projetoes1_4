package entity;

	public class Bairro extends Entity{
	
		private String nomeBairro;
		
		public Bairro() {
			super("idBairro");
		}
		
		public Bairro(long idBairro) {
			super("idBairro",idBairro);
		}
		
		public Bairro(String nomeBairro) {
			super("idBairro");
			this.nomeBairro = nomeBairro;
		}
		
		public Bairro(long idBairro, String nomeBairro) {
			super("idBairro",idBairro);
			this.nomeBairro = nomeBairro;
		}

		public long getIdBairro() {
			return (long) super.getIdentifier();
		}

		public void setIdBairro(long idBairro) {
			super.setIdentifier(idBairro);
		}

		public String getNomeBairro() {
			return nomeBairro;
		}

		public void setNomeBairro(String nomeBairro) {
			this.nomeBairro = nomeBairro;
		}
		
	}
