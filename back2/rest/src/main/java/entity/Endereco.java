package entity;

	public class Endereco extends Entity{
	
		private Bairro bairro;
		private Cidade cidade;
		private Logradouro logradouro;
		private String cep;
		
		public Endereco() {
			super("idEndereco");
			bairro = new Bairro();
			cidade = new Cidade();
			logradouro = new Logradouro();
		}
		
		public Endereco(long idEndereco) {
			super("idEndereco",idEndereco);
			bairro = new Bairro();
			cidade = new Cidade();
			logradouro = new Logradouro();
		}

		public Endereco(Bairro bairro, Cidade cidade, Logradouro logradouro, String cep) {
			super("idEndereco");
			this.bairro = bairro;
			this.cidade = cidade;
			this.logradouro = logradouro;
			this.cep = cep;
		}
		
		public Endereco(long idEndereco, Bairro bairro, Cidade cidade, Logradouro logradouro, String cep) {
			super("idEndereco",idEndereco);
			this.bairro = bairro;
			this.cidade = cidade;
			this.logradouro = logradouro;
			this.cep = cep;
		}

		public long getIdEndereco() {
			return (long) super.getIdentifier();
		}
		
		public void setIdEndereco(long idEndereco) {
			super.setIdentifier(idEndereco);
		}
		
		public Bairro getBairro() {
			return bairro;
		}

		public void setBairro(Bairro bairro) {
			this.bairro = bairro;
		}

		public Cidade getCidade() {
			return cidade;
		}

		public void setCidade(Cidade cidade) {
			this.cidade = cidade;
		}

		public Logradouro getLogradouro() {
			return logradouro;
		}

		public void setLogradouro(Logradouro logradouro) {
			this.logradouro = logradouro;
		}

		public String getCep() {
			return cep;
		}

		public void setCep(String cep) {
			this.cep = cep;
		}

		@Override
		public String toString() {
			return "Endereco [bairro=" + bairro + ", cidade=" + cidade + ", logradouro=" + logradouro + ", cep=" + cep
					+ "]";
		}
		
	}
