package entity;

	public class Paciente extends Entity{
	
		private String cpf;
		private String primeiroNome;
		private String sobrenome;
		private String dataNascimento;
		private String tipoSanguineo;
		private String fatorRh;
		private String sexo;
		private int nroLogradouro;
		private String complementoLogradouro;
		private Endereco endereco;
		
		public Paciente() {
			super("idPaciente");
			endereco = new Endereco();
		}
		
		public Paciente(long idPaciente) {
			super("idPaciente",idPaciente);
			endereco = new Endereco();
		}

		public Paciente(long idPaciente, String cpf, String primeiroNome, String sobrenome) {
			super("idPaciente",idPaciente);
			this.cpf = cpf;
			this.primeiroNome = primeiroNome;
			this.sobrenome = sobrenome;
		}
		
		public Paciente(String cpf, String primeiroNome, String sobrenome, String dataNascimento, String tipoSanguineo,
				String fatorRh, String sexo) {
			super("idPaciente");
			this.cpf = cpf;
			this.primeiroNome = primeiroNome;
			this.sobrenome = sobrenome;
			this.dataNascimento = dataNascimento;
			this.tipoSanguineo = tipoSanguineo;
			this.fatorRh = fatorRh;
			this.sexo = sexo;
		}
		
		public Paciente(String cpf, String primeiroNome, String sobrenome, String dataNascimento, String tipoSanguineo,
				String fatorRh, String sexo, int nroLogradouro, String complementoLogradouro, Endereco endereco) {
			super("idPaciente");
			this.cpf = cpf;
			this.primeiroNome = primeiroNome;
			this.sobrenome = sobrenome;
			this.dataNascimento = dataNascimento;
			this.tipoSanguineo = tipoSanguineo;
			this.fatorRh = fatorRh;
			this.sexo = sexo;
			this.nroLogradouro = nroLogradouro;
			this.complementoLogradouro = complementoLogradouro;
			this.endereco = endereco;
		}
		
		public Paciente(long idPaciente, String cpf, String primeiroNome, String sobrenome, String dataNascimento, 
				String tipoSanguineo, String fatorRh, String sexo, 
				int nroLogradouro, String complementoLogradouro, Endereco endereco) {
			super("idPaciente",idPaciente);
			this.cpf = cpf;
			this.primeiroNome = primeiroNome;
			this.sobrenome = sobrenome;
			this.dataNascimento = dataNascimento;
			this.tipoSanguineo = tipoSanguineo;
			this.fatorRh = fatorRh;
			this.sexo = sexo;
			this.nroLogradouro = nroLogradouro;
			this.complementoLogradouro = complementoLogradouro;
			this.endereco = endereco;
		}

		public String getCpf() {
			return cpf;
		}

		public void setCpf(String cpf) {
			this.cpf = cpf;
		}

		public String getPrimeiroNome() {
			return primeiroNome;
		}

		public void setPrimeiroNome(String primeiroNome) {
			this.primeiroNome = primeiroNome;
		}

		public String getSobrenome() {
			return sobrenome;
		}

		public void setSobrenome(String sobrenome) {
			this.sobrenome = sobrenome;
		}

		public String getDataNascimento() {
			return dataNascimento;
		}

		public void setDataNascimento(String dataNascimento) {
			this.dataNascimento = dataNascimento;
		}

		public String getTipoSanguineo() {
			return tipoSanguineo;
		}

		public void setTipoSanguineo(String tipoSanguineo) {
			this.tipoSanguineo = tipoSanguineo;
		}

		public String getFatorRh() {
			return fatorRh;
		}

		public void setFatorRh(String fatorRh) {
			this.fatorRh = fatorRh;
		}

		public String getSexo() {
			return sexo;
		}

		public void setSexo(String sexo) {
			this.sexo = sexo;
		}

		public int getNroLogradouro() {
			return nroLogradouro;
		}

		public void setNroLogradouro(int nroLogradouro) {
			this.nroLogradouro = nroLogradouro;
		}

		public String getComplementoLogradouro() {
			return complementoLogradouro;
		}

		public void setComplementoLogradouro(String complementoLogradouro) {
			this.complementoLogradouro = complementoLogradouro;
		}

		public Endereco getEndereco() {
			return endereco;
		}

		public void setEndereco(Endereco endereco) {
			this.endereco = endereco;
		}

		@Override
		public String toString() {
			return "Paciente [cpf=" + cpf + ", primeiroNome=" + primeiroNome + ", sobrenome=" + sobrenome
					+ ", dataNascimento=" + dataNascimento + ", tipoSanguineo=" + tipoSanguineo + ", fatorRh=" + fatorRh
					+ ", sexo=" + sexo + ", nroLogradouro=" + nroLogradouro + ", complementoLogradouro="
					+ complementoLogradouro + ", endereco=" + endereco + "]";
		}
	
	}
