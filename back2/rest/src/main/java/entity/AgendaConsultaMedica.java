package entity;

	public class AgendaConsultaMedica extends Entity{
	
		private String dataRealizada;
		private String dataAgendada;
		private String sintomas;
		private Medico medico;
		private Paciente paciente;
		private EspecialidadeMedica especialidadeMedica;
		
		public AgendaConsultaMedica() {
			super("idAgendaConsultaMedica");
			medico = new Medico();
			paciente = new Paciente();
			especialidadeMedica = new EspecialidadeMedica();
		}
		
		public AgendaConsultaMedica(String dataRealizada, String dataAgendada, String sintomas, Medico medico,
				Paciente paciente, EspecialidadeMedica especialidadeMedica) {
			super("idAgendaConsultaMedica");
			this.dataRealizada = dataRealizada;
			this.dataAgendada = dataAgendada;
			this.sintomas = sintomas;
			this.medico = medico;
			this.paciente = paciente;
			this.especialidadeMedica = especialidadeMedica;
		}

		public AgendaConsultaMedica(long idAgendaConsultaMedica, String dataRealizada, String dataAgendada, String sintomas, Medico medico,
				Paciente paciente, EspecialidadeMedica especialidadeMedica) {
			super("idAgendaConsultaMedica",idAgendaConsultaMedica);
			this.dataRealizada = dataRealizada;
			this.dataAgendada = dataAgendada;
			this.sintomas = sintomas;
			this.medico = medico;
			this.paciente = paciente;
			this.especialidadeMedica = especialidadeMedica;
		}

		public String getDataRealizada() {
			return dataRealizada;
		}
		
		public void setDataRealizada(String dataRealizada) {
			this.dataRealizada = dataRealizada;
		}
		
		public String getDataAgendada() {
			return dataAgendada;
		}
		
		public void setDataAgendada(String dataAgendada) {
			this.dataAgendada = dataAgendada;
		}
		
		public String getSintomas() {
			return sintomas;
		}
		
		public void setSintomas(String sintomas) {
			this.sintomas = sintomas;
		}
		
		public Medico getMedico() {
			return medico;
		}
		
		public void setMedico(Medico medico) {
			this.medico = medico;
		}
		
		public Paciente getPaciente() {
			return paciente;
		}
		
		public void setPaciente(Paciente paciente) {
			this.paciente = paciente;
		}
		
		public EspecialidadeMedica getEspecialidadeMedica() {
			return especialidadeMedica;
		}
		
		public void setEspecialidadeMedica(EspecialidadeMedica especialidadeMedica) {
			this.especialidadeMedica = especialidadeMedica;
		}
		
		
	}
