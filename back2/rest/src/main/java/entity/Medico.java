package entity;

import java.util.ArrayList;
import java.util.List;

public class Medico extends Entity{
		
		private String crm;
		private String primeiroNome;
		private String sobrenome;
		private String dataNascimento;
		private String tipoSanguineo;
		private String fatorRh;
		private String sexo;
		private int nroLogradouro;
		private String complementoLogradouro;
		private Endereco endereco;
		private UnidadeFederacao crmUF;
		private List<EspecialidadeMedica> listaEspecialidade = new ArrayList<>();
		
		public Medico() {
			super("idMedico");
			endereco = new Endereco();
			crmUF = new UnidadeFederacao();
		}
		
		public Medico(long idMedico) {
			super("idMedico",idMedico);
			endereco = new Endereco();
			crmUF = new UnidadeFederacao();
		}

		public Medico(long idMedico, String crm, String primeiroNome, String sobrenome, UnidadeFederacao crmUF) {
			super("idMedico",idMedico);
			this.crm = crm;
			this.primeiroNome = primeiroNome;
			this.sobrenome = sobrenome;
			this.crmUF = crmUF;
		}
		
		public Medico(String crm, String primeiroNome, String sobrenome, String dataNascimento, String tipoSanguineo,
				String fatorRh, String sexo, int nroLogradouro, String complementoLogradouro, Endereco endereco,
				UnidadeFederacao crmUF) {
			super("idMedico");
			this.crm = crm;
			this.primeiroNome = primeiroNome;
			this.sobrenome = sobrenome;
			this.dataNascimento = dataNascimento;
			this.tipoSanguineo = tipoSanguineo;
			this.fatorRh = fatorRh;
			this.sexo = sexo;
			this.nroLogradouro = nroLogradouro;
			this.complementoLogradouro = complementoLogradouro;
			this.endereco = endereco;
			this.crmUF = crmUF;
		}
		
		public Medico(long idMedico, String crm, String primeiroNome, String sobrenome, String dataNascimento, 
				String tipoSanguineo, String fatorRh, String sexo, int nroLogradouro, String complementoLogradouro, 
				Endereco endereco, UnidadeFederacao crmUF) {
			super("idMedico",idMedico);
			this.crm = crm;
			this.primeiroNome = primeiroNome;
			this.sobrenome = sobrenome;
			this.dataNascimento = dataNascimento;
			this.tipoSanguineo = tipoSanguineo;
			this.fatorRh = fatorRh;
			this.sexo = sexo;
			this.nroLogradouro = nroLogradouro;
			this.complementoLogradouro = complementoLogradouro;
			this.endereco = endereco;
			this.crmUF = crmUF;
		}

		public long getIdMedico() {
			return (long) super.getIdentifier();
		}
		
		public void setIdMedico(long idMedico) {
			super.setIdentifier(idMedico);
		}
		
		public String getCrm() {
			return crm;
		}

		public void setCrm(String crm) {
			this.crm = crm;
		}

		public String getPrimeiroNome() {
			return primeiroNome;
		}

		public void setPrimeiroNome(String primeiroNome) {
			this.primeiroNome = primeiroNome;
		}

		public String getSobrenome() {
			return sobrenome;
		}

		public void setSobrenome(String sobrenome) {
			this.sobrenome = sobrenome;
		}

		public String getDataNascimento() {
			return dataNascimento;
		}

		public void setDataNascimento(String dataNascimento) {
			this.dataNascimento = dataNascimento;
		}

		public String getTipoSanguineo() {
			return tipoSanguineo;
		}

		public void setTipoSanguineo(String tipoSanguineo) {
			this.tipoSanguineo = tipoSanguineo;
		}

		public String getFatorRh() {
			return fatorRh;
		}

		public void setFatorRh(String fatorRh) {
			this.fatorRh = fatorRh;
		}

		public String getSexo() {
			return sexo;
		}

		public void setSexo(String sexo) {
			this.sexo = sexo;
		}

		public int getNroLogradouro() {
			return nroLogradouro;
		}

		public void setNroLogradouro(int nroLogradouro) {
			this.nroLogradouro = nroLogradouro;
		}

		public String getComplementoLogradouro() {
			return complementoLogradouro;
		}

		public void setComplementoLogradouro(String complementoLogradouro) {
			this.complementoLogradouro = complementoLogradouro;
		}

		public Endereco getEndereco() {
			return endereco;
		}

		public void setEndereco(Endereco endereco) {
			this.endereco = endereco;
		}

		public UnidadeFederacao getCrmUF() {
			return crmUF;
		}

		public void setCrmUF(UnidadeFederacao crmUF) {
			this.crmUF = crmUF;
		}

		public List<EspecialidadeMedica> getListaEspecialidade() {
			return listaEspecialidade;
		}

		public void setListaEspecialidade(List<EspecialidadeMedica> listaEspecialidade) {
			this.listaEspecialidade = listaEspecialidade;
		}

		@Override
		public String toString() {
			return "Medico [crm=" + crm + ", primeiroNome=" + primeiroNome + ", sobrenome=" + sobrenome
					+ ", dataNascimento=" + dataNascimento + ", tipoSanguineo=" + tipoSanguineo + ", fatorRh=" + fatorRh
					+ ", sexo=" + sexo + ", nroLogradouro=" + nroLogradouro + ", complementoLogradouro="
					+ complementoLogradouro + ", endereco=" + endereco + ", crmUF=" + crmUF + ", listaEspecialidade="
					+ listaEspecialidade + "]";
		}
		
	}
