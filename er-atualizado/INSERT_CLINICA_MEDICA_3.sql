insert ignore into UnidadeFederacao(siglaUF,nomeUF) values('PR','Paraná');
insert ignore into UnidadeFederacao(siglaUF,nomeUF) values('SP','São Paulo');
insert ignore into UnidadeFederacao(siglaUF,nomeUF) values('SC','Santa Catarina');
insert ignore into UnidadeFederacao(siglaUF,nomeUF) values('MS','Mato Grosso do Sul');
insert ignore into UnidadeFederacao(siglaUF,nomeUF) values('RS','Rio Grande do Sul');
insert ignore into UnidadeFederacao(siglaUF,nomeUF) values('MT','Mato Grosso');
insert ignore into UnidadeFederacao(siglaUF,nomeUF) values('GO','Goiás');
insert ignore into UnidadeFederacao(siglaUF,nomeUF) values('RJ','Rio de Janeiro');
insert ignore into UnidadeFederacao(siglaUF,nomeUF) values('RR','Roraima');
insert ignore into UnidadeFederacao(siglaUF,nomeUF) values('AM','Amazonas');
insert ignore into UnidadeFederacao(siglaUF,nomeUF) values('ES','Espírito Santo');

insert ignore into Cidade(nomeCidade,siglaUF) values ('Niterói','RJ');
insert ignore into Cidade(nomeCidade,siglaUF) values ('Florianópolis','RS');
insert ignore into Cidade(nomeCidade,siglaUF) values ('São Paulo','SP');
insert ignore into Cidade(nomeCidade,siglaUF) values ('Foz do Iguaçu','PR');
insert ignore into Cidade(nomeCidade,siglaUF) values ('Londrina','PR');
insert ignore into Cidade(nomeCidade,siglaUF) values ('Maringá','PR');
insert ignore into Cidade(nomeCidade,siglaUF) values ('Cuiabá','MT');
insert ignore into Cidade(nomeCidade,siglaUF) values ('Chapada dos Guimarães','MT');
insert ignore into Cidade(nomeCidade,siglaUF) values ('Presidente Prudente','SP');
insert ignore into Cidade(nomeCidade,siglaUF) values ('Belo Horizonte','MG');

insert ignore into Bairro(nomeBairro) values ('Itaipu A');
insert ignore into Bairro(nomeBairro) values ('Jardim Lancaster');
insert ignore into Bairro(nomeBairro) values ('Centro');
insert ignore into Bairro(nomeBairro) values ('Itaipu C');
insert ignore into Bairro(nomeBairro) values ('Morumbi');
insert ignore into Bairro(nomeBairro) values ('Perobinha');
insert ignore into Bairro(nomeBairro) values ('Jardim Higienópolis');
insert ignore into Bairro(nomeBairro) values ('Shangri-la');
insert ignore into Bairro(nomeBairro) values ('Coxipó');
insert ignore into Bairro(nomeBairro) values ('Tijucal');
insert ignore into Bairro(nomeBairro) values ('Centro Político Administrativo');
insert ignore into Bairro(nomeBairro) values ('Jardim América');
insert ignore into Bairro(nomeBairro) values ('Jardim Itália');
insert ignore into Bairro(nomeBairro) values ('Jardim Industrial');
insert ignore into Bairro(nomeBairro) values ('Santa Rosa');
insert ignore into Bairro(nomeBairro) values ('Jardim Naipi');
insert ignore into Bairro(nomeBairro) values ('Jardim Ipê');
insert ignore into Bairro(nomeBairro) values ('Jardim Tropical');
insert ignore into Bairro(nomeBairro) values ('Jardim Curitibano');

insert ignore into TipoLogradouro(siglaTipoLogradouro,nomeTipo) values('R','Rua');
insert ignore into TipoLogradouro(siglaTipoLogradouro,nomeTipo) values('Av','Avenida');
insert ignore into TipoLogradouro(siglaTipoLogradouro,nomeTipo) values('Al','Alameda');
insert ignore into TipoLogradouro(siglaTipoLogradouro,nomeTipo) values('Pq','Parque');
insert ignore into TipoLogradouro(siglaTipoLogradouro,nomeTipo) values('Bal','Balneário');
insert ignore into TipoLogradouro(siglaTipoLogradouro,nomeTipo) values('Cj','Conjunto');
insert ignore into TipoLogradouro(siglaTipoLogradouro,nomeTipo) values('Etc','Estação');

insert ignore into Logradouro(nomeLogradouro,siglaTipoLogradouro) values ('Acarí','R');
insert ignore into Logradouro(nomeLogradouro,siglaTipoLogradouro) values ('Ariranha','R');
insert ignore into Logradouro(nomeLogradouro,siglaTipoLogradouro) values ('B','R');
insert ignore into Logradouro(nomeLogradouro,siglaTipoLogradouro) values ('Assunção','R');
insert ignore into Logradouro(nomeLogradouro,siglaTipoLogradouro) values ('Sucuri','R');
insert ignore into Logradouro(nomeLogradouro,siglaTipoLogradouro) values ('Cotia','R');
insert ignore into Logradouro(nomeLogradouro,siglaTipoLogradouro) values ('Marechal Rondon','Av');
insert ignore into Logradouro(nomeLogradouro,siglaTipoLogradouro) values ('Georgia','Pq');
insert ignore into Logradouro(nomeLogradouro,siglaTipoLogradouro) values ('Imburana','R');
insert ignore into Logradouro(nomeLogradouro,siglaTipoLogradouro) values ('Engenheiro Hildemar Leite Franca','Av');
insert ignore into Logradouro(nomeLogradouro,siglaTipoLogradouro) values ('Holanda','R');
insert ignore into Logradouro(nomeLogradouro,siglaTipoLogradouro) values ('Dona Letícia','Al');
insert ignore into Logradouro(nomeLogradouro,siglaTipoLogradouro) values ('Brodoski','Av');	
insert ignore into Logradouro(nomeLogradouro,siglaTipoLogradouro) values ('Figueira','R');
insert ignore into Logradouro(nomeLogradouro,siglaTipoLogradouro) values ('Cajati','R');

insert ignore into Endereco(cep,idBairro,idLogradouro,idCidade) values('78040225',15,11,7);
insert ignore into Endereco(cep,idBairro,idLogradouro,idCidade) values('85860320',1,10,4);
insert ignore into Endereco(cep,idBairro,idLogradouro,idCidade) values('78040225',16,12,4);
insert ignore into Endereco(cep,idBairro,idLogradouro,idCidade) values('85869050',17,13,4);
insert ignore into Endereco(cep,idBairro,idLogradouro,idCidade) values('85855070',18,14,4);
insert ignore into Endereco(cep,idBairro,idLogradouro,idCidade) values('85855070',18,14,4);

insert ignore into EspecialidadeMedica(tipoEspecialidade) values ('Neurologia');
insert ignore into EspecialidadeMedica(tipoEspecialidade) values ('Oftalmologia');
insert ignore into EspecialidadeMedica(tipoEspecialidade) values ('Endoscopia');
insert ignore into EspecialidadeMedica(tipoEspecialidade) values ('Dermatologia');
insert ignore into EspecialidadeMedica(tipoEspecialidade) values ('Cirgurgia Geral');
insert ignore into EspecialidadeMedica(tipoEspecialidade) values ('Cardiologia');
insert ignore into EspecialidadeMedica(tipoEspecialidade) values ('Clínica Médica');
insert ignore into EspecialidadeMedica(tipoEspecialidade) values ('Ortopedia');
insert ignore into EspecialidadeMedica(tipoEspecialidade) values ('Psiquiatria');
insert ignore into EspecialidadeMedica(tipoEspecialidade) values ('Patologia');
insert ignore into EspecialidadeMedica(tipoEspecialidade) values ('Pediatria');

insert ignore into Medico_EspecialidadeMedica(idMedico,idEspecialidadeMedica) values (1,11);
insert ignore into Medico_EspecialidadeMedica(idMedico,idEspecialidadeMedica) values (2,6);
insert ignore into Medico_EspecialidadeMedica(idMedico,idEspecialidadeMedica) values (3,9);

/* Medico em Pediatria
*/
insert ignore into Medico (crm,primeiroNome,sobrenome,dataNascimento,tipoSanguineo,fatorRh,sexo,nroLogradouro,complementoLogradouro,idEndereco,crmUF) values (3711,'Wilmar','Guimarães','1974-03-01','A','+','Masculino',109,'Próximo da Mercado X',1,'PR');

/* Medico em Cadiologia
*/
insert ignore into Medico (crm,primeiroNome,sobrenome,dataNascimento,tipoSanguineo,fatorRh,sexo,nroLogradouro,complementoLogradouro,idEndereco,crmUF) values (19050,'Luiz','Perez','1982-05-07','O','-','Masculino',300,'Próximo do prédio da prefeitura',2,'PR');

/* Medica em Psquiatria
*/
insert ignore into Medico (crm,primeiroNome,sobrenome,dataNascimento,tipoSanguineo,fatorRh,sexo,nroLogradouro,complementoLogradouro,idEndereco,crmUF) values (26228,'Sígridi','Wolfart','1989-04-15','B','-','Feminino',1293,'Sala 1204',3,'PR');

insert ignore into Paciente (cpf,primeiroNome,sobrenome,dataNascimento,tipoSanguineo,fatorRh,sexo,nroLogradouro,complementoLogradouro,idEndereco) values ('74916644905','Eduardo','Sakamoto','2000-12-10','AB','+','Masculino',604,'Casa',4);
insert ignore into Paciente (cpf,primeiroNome,sobrenome,dataNascimento,tipoSanguineo,fatorRh,sexo,nroLogradouro,complementoLogradouro,idEndereco) values ('74379554902','Tânia','Silva','1986-03-17','O','-','Feminino',485,'',5);
insert ignore into Paciente (cpf,primeiroNome,sobrenome,dataNascimento,tipoSanguineo,fatorRh,sexo,nroLogradouro,complementoLogradouro,idEndereco) values ('35819846958','Hugo','Vieira','1975-01-23','AB','-','Masculino',344,'Na esquina',6);

insert ignore into EmailPaciente (idPaciente,email) values (1,'genericPatient1@hotmail.com');
insert ignore into EmailPaciente (idPaciente,email) values (2,'genericPatient2@hotmail.com');
insert ignore into EmailPaciente (idPaciente,email) values (3,'specialPatient@hotmail.com');

insert ignore into EmailMedico (idMedico,email) values (1,'genericMedic1@hotmail.com');
insert ignore into EmailMedico (idMedico,email) values (2,'genericMedic2@hotmail.com');
insert ignore into EmailMedico (idMedico,email) values (3,'specialistMedic@hotmail.com');

insert ignore into CelularPaciente (idPaciente,nroCelular) values (1,'45998745632');
insert ignore into CelularPaciente (idPaciente,nroCelular) values (2,'45992571872');
insert ignore into CelularPaciente (idPaciente,nroCelular) values (3,'45998282864');

insert ignore into CelularMedico (idMedico,nroCelular) values (1,'996333256');
insert ignore into CelularMedico (idMedico,nroCelular) values (2,'998744125');
insert ignore into CelularMedico (idMedico,nroCelular) values (3,'996587426');



/* Neurologia
*/
insert ignore into Remedio(idPrescricaoRemedio,nomeRemedio) values (1,'BETINA 24mg com 30 comprimidos');
insert ignore into Remedio(idPrescricaoRemedio,nomeRemedio) values (1,'ARCALION 200mg com 60 comprimidos');

/* Oftalmologia
*/
insert ignore into Remedio(idPrescricaoRemedio,nomeRemedio) values (1,'Efortil 5mg com 20 comprimidos');
insert ignore into Remedio(idPrescricaoRemedio,nomeRemedio) values (1,'Etilefril 10mg/mL, caixa com 6 ampolas com 1mL de solução de uso intravenoso, intramuscular ou subcutâneo');

/* Psiquiatria
*/
insert ignore into Remedio(idPrescricaoRemedio,nomeRemedio) values (1,'Lorazepam 2mg 20 comprimidos - Germed Genérico');
insert ignore into Remedio(idPrescricaoRemedio,nomeRemedio) values (1,'Mirtazapina 15mg Sandoz com 28 comprimidos');
