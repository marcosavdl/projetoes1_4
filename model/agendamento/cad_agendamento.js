const urlEspecialidade = "http://localhost:8080/rest/speciality/getAll";
fetch(urlEspecialidade)
    // fetch(`http://192.168.1.100:8080/rest/medic/getAll`)
    // Handle success
    .then(response => response.json())  // convert to json
    .then((json) => {

        for (i = 0; i < json.length; i++) {
            var x = document.getElementById("listaEspecialidadeMedicaAgendamento");
            var c = document.createElement("option");
            c.text = json[i].tipoEspecialidade;
            c.value = json[i].identifier;
            x.options.add(c);
        }

    })

const urlPaciente = "http://localhost:8080/rest/patient/getAllActive";
fetch(urlPaciente)
    // fetch(`http://192.168.1.100:8080/rest/medic/getAll`)
    // Handle success
    .then(response => response.json())  // convert to json
    .then((json) => {

        for (i = 0; i < paciente.length; i++) {
            var x = document.getElementById("listaPacienteAgendamento");
            var c = document.createElement("option");
            c.text = paciente[i].primeiroNome + " " + paciente[i].sobrenome;
            c.value = paciente[i].identifier;
            x.options.add(c);
        }

    })


function getListaMedico() {
const url_medico = "http://localhost:8080/rest/medic/getAllActive";
fetch(url_medico)
    // fetch(`http://192.168.1.100:8080/rest/medic/getAll`)
    // Handle success
    .then(response => response.json())  // convert to json
    .then((json) => {

        var listaMedico = [];
        document.getElementById("listaMedicoAgendamento").innerHTML = ""
        var e = document.getElementById("listaEspecialidadeMedicaAgendamento");
        var especialidade = e.options[e.selectedIndex].text;
        // listaMedico.push("teste")
        // console.log(listaMedico)



        for (i = 0; i < json.length; i++) {
            for (j = 0; j < json[i].listaEspecialidade.length; j++) {
                if (json[i].listaEspecialidade[j].tipoEspecialidade == especialidade) {
                    listaMedico.push(json[i].primeiroNome + " " + json[i].sobrenome)
                }
                // console.log(medico[i].listaEspecialidade[j].tipoEspecialidade)

            }


        }

        // console.log(listaMedico[0])
        // var x = document.getElementById("listaMedicoAgendamento");
        for (i = 0; i < listaMedico.length; i++) {
            var x = document.getElementById("listaMedicoAgendamento");
            var c = document.createElement("option");
            c.text = listaMedico[i];
            c.value = listaMedico[i];
            x.options.add(c);
        }

    })


}


// var teste = [{ "tipoEspecialidade": "Neurologia", "identifierName": "idEspecialidadeMedica", "identifier": 1 }, { "tipoEspecialidade": "Oftalmologia", "identifierName": "idEspecialidadeMedica", "identifier": 2 }, { "tipoEspecialidade": "Endoscopia", "identifierName": "idEspecialidadeMedica", "identifier": 3 }, { "tipoEspecialidade": "Dermatologia", "identifierName": "idEspecialidadeMedica", "identifier": 4 }, { "tipoEspecialidade": "Cirgurgia Geral", "identifierName": "idEspecialidadeMedica", "identifier": 5 }, { "tipoEspecialidade": "Cardiologia", "identifierName": "idEspecialidadeMedica", "identifier": 6 }, { "tipoEspecialidade": "Clínica Médica", "identifierName": "idEspecialidadeMedica", "identifier": 7 }, { "tipoEspecialidade": "Ortopedia", "identifierName": "idEspecialidadeMedica", "identifier": 8 }, { "tipoEspecialidade": "Psiquiatria", "identifierName": "idEspecialidadeMedica", "identifier": 9 }, { "tipoEspecialidade": "Patologia", "identifierName": "idEspecialidadeMedica", "identifier": 10 }, { "tipoEspecialidade": "Pediatria", "identifierName": "idEspecialidadeMedica", "identifier": 11 }]

// var medico = [{ "crm": "3711", "primeiroNome": "Wilmar", "sobrenome": "Guimarães", "dataNascimento": "1974-03-01", "tipoSanguineo": "A", "fatorRh": "+", "sexo": "Masculino", "nroLogradouro": 109, "complementoLogradouro": "Próximo da Mercado X", "endereco": { "bairro": { "nomeBairro": "Santa Rosa", "identifierName": "idBairro", "identifier": 15 }, "cidade": { "nomeCidade": "Cuiabá", "uf": { "nomeUF": "Mato Grosso", "identifierName": "siglaUF", "identifier": "MT" }, "identifierName": "idCidade", "identifier": 7 }, "logradouro": { "nomeLogradouro": "Holanda", "tipoLogradouro": { "nomeTipo": "Rua", "identifierName": "siglaTipoLogradouro", "identifier": "R" }, "identifierName": "idLogradouro", "identifier": 11 }, "cep": "78040225", "identifierName": "idEndereco", "identifier": 1 }, "crmUF": { "nomeUF": "", "identifierName": "siglaUF", "identifier": "PR" }, "listaEspecialidade": [{ "tipoEspecialidade": "Neurologia", "identifierName": "idEspecialidadeMedica", "identifier": 1 }, { "tipoEspecialidade": "Cardiologia", "identifierName": "idEspecialidadeMedica", "identifier": 6 }], "identifierName": "idMedico", "identifier": 1 }, { "crm": "26228", "primeiroNome": "Sígridi", "sobrenome": "Wolfart", "dataNascimento": "1989-04-15", "tipoSanguineo": "B", "fatorRh": "-", "sexo": "Feminino", "nroLogradouro": 1293, "complementoLogradouro": "Sala 1204", "endereco": { "bairro": { "nomeBairro": "Jardim Naipi", "identifierName": "idBairro", "identifier": 16 }, "cidade": { "nomeCidade": "Foz do Iguaçu", "uf": { "nomeUF": "Paraná", "identifierName": "siglaUF", "identifier": "PR" }, "identifierName": "idCidade", "identifier": 4 }, "logradouro": { "nomeLogradouro": "Dona Letícia", "tipoLogradouro": { "nomeTipo": "Alameda", "identifierName": "siglaTipoLogradouro", "identifier": "Al" }, "identifierName": "idLogradouro", "identifier": 12 }, "cep": "78040225", "identifierName": "idEndereco", "identifier": 3 }, "crmUF": { "nomeUF": "", "identifierName": "siglaUF", "identifier": "PR" }, "listaEspecialidade": [{ "tipoEspecialidade": "Psiquiatria", "identifierName": "idEspecialidadeMedica", "identifier": 9 }], "identifierName": "idMedico", "identifier": 3 }]


// var paciente = [{ "cpf": "74916644905", "primeiroNome": "Eduardo", "sobrenome": "Sakamoto", "dataNascimento": "2000-12-10", "tipoSanguineo": "AB", "fatorRh": "+", "sexo": "Masculino", "nroLogradouro": 604, "complementoLogradouro": "Casa", "endereco": { "bairro": { "nomeBairro": "Jardim Ipê", "identifierName": "idBairro" }, "cidade": { "nomeCidade": "Foz do Iguaçu", "uf": { "nomeUF": "Paraná", "identifierName": "siglaUF", "identifier": "PR" }, "identifierName": "idCidade" }, "logradouro": { "nomeLogradouro": "Brodoski", "tipoLogradouro": { "nomeTipo": "Avenida", "identifierName": "siglaTipoLogradouro" }, "identifierName": "idLogradouro" }, "cep": "85869050", "identifierName": "idEndereco" }, "identifierName": "idPaciente", "identifier": 1 }, { "cpf": "74379554902", "primeiroNome": "Tânia", "sobrenome": "Silva", "dataNascimento": "1986-03-17", "tipoSanguineo": "O", "fatorRh": "-", "sexo": "Feminino", "nroLogradouro": 485, "complementoLogradouro": "", "endereco": { "bairro": { "nomeBairro": "Jardim Tropical", "identifierName": "idBairro" }, "cidade": { "nomeCidade": "Foz do Iguaçu", "uf": { "nomeUF": "Paraná", "identifierName": "siglaUF", "identifier": "PR" }, "identifierName": "idCidade" }, "logradouro": { "nomeLogradouro": "Figueira", "tipoLogradouro": { "nomeTipo": "Rua", "identifierName": "siglaTipoLogradouro" }, "identifierName": "idLogradouro" }, "cep": "85855070", "identifierName": "idEndereco" }, "identifierName": "idPaciente", "identifier": 2 }, { "cpf": "35819846958", "primeiroNome": "Hugo", "sobrenome": "Vieira", "dataNascimento": "1975-01-23", "tipoSanguineo": "AB", "fatorRh": "-", "sexo": "Masculino", "nroLogradouro": 344, "complementoLogradouro": "Na esquina", "endereco": { "bairro": { "nomeBairro": "Jardim Tropical", "identifierName": "idBairro" }, "cidade": { "nomeCidade": "Foz do Iguaçu", "uf": { "nomeUF": "Paraná", "identifierName": "siglaUF", "identifier": "PR" }, "identifierName": "idCidade" }, "logradouro": { "nomeLogradouro": "Figueira", "tipoLogradouro": { "nomeTipo": "Rua", "identifierName": "siglaTipoLogradouro" }, "identifierName": "idLogradouro" }, "cep": "85855070", "identifierName": "idEndereco" }, "identifierName": "idPaciente", "identifier": 3 }]

// console.log(paciente)

// for (i = 0; i < paciente.length; i++) {
//     var x = document.getElementById("listaPacienteAgendamento");
//     var c = document.createElement("option");
//     c.text = paciente[i].primeiroNome + " " + paciente[i].sobrenome;
//     c.value = paciente[i].identifier;
//     x.options.add(c);
// }

// for (i = 0; i < teste.length; i++) {
//     var x = document.getElementById("listaEspecialidadeMedicaAgendamento");
//     var c = document.createElement("option");
//     c.text = teste[i].tipoEspecialidade;
//     c.value = teste[i].identifier;
//     x.options.add(c);
// }

// function getListaMedico() {
//     var listaMedico = [];
//     document.getElementById("listaMedicoAgendamento").innerHTML = ""
//     var e = document.getElementById("listaEspecialidadeMedicaAgendamento");
//     var especialidade = e.options[e.selectedIndex].text;
//     // listaMedico.push("teste")
//     // console.log(listaMedico)



//     for (i = 0; i < medico.length; i++) {
//         for (j = 0; j < medico[i].listaEspecialidade.length; j++) {
//             if (medico[i].listaEspecialidade[j].tipoEspecialidade == especialidade) {
//                 listaMedico.push(medico[i].primeiroNome + " " + medico[i].sobrenome)
//             }
//             // console.log(medico[i].listaEspecialidade[j].tipoEspecialidade)

//         }


//     }

//     // console.log(listaMedico[0])
//     // var x = document.getElementById("listaMedicoAgendamento");
//     for (i = 0; i < listaMedico.length; i++) {
//         var x = document.getElementById("listaMedicoAgendamento");
//         var c = document.createElement("option");
//         c.text = listaMedico[i];
//         c.value = listaMedico[i];
//         x.options.add(c);
//     }

// }


