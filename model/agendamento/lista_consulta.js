const url = "http://localhost:8080/rest/appointment/getAll";


fetch(url, {
    method: "GET",
    // mode: 'no-cors',
    header: {
        // "Access-token": "xxx",
        Accept: 'application/json',
        "Content-Type": "application/json"
    }
}
)
    // Handle success
    .then(response => response.json())  // convert to json
    .then((data) => {


        console.log(data)



        var lista = data;
        var teste = {};
        var lista2 = [];
        var anoAtual = new Date().getFullYear();
        
        for (var i = 0; i < lista.length; i++) {
            var dataConsulta = lista[i].dataConsulta.split(" ")[0]
            var horaConsulta = lista[i].dataConsulta.split(" ")[1].split(":")
            // console.log(dataConsulta,horaConsulta)
            lista2[i] = teste = {
                ordem: i + 1,
                nroConsulta: lista[i].nroConsulta,
                dataConsulta: dataConsulta,
                horaConsulta: horaConsulta[0] + ":" + horaConsulta[1],
                tipoEspecialidade: lista[i].especialidadeMedica.tipoEspecialidade,
                paciente: lista[i].paciente.primeiroNome + " " + lista[i].paciente.sobrenome,
                medico: lista[i].medico.primeiroNome + " " + lista[i].medico.sobrenome,
        
            }
        
        }
        
        
        var dataSet = lista2.map(Object.values)
        console.log(lista2)
        
        
        $(document).ready(function () {
            $('#tbListaConsultaMedica').DataTable({
                data: dataSet,
                columns: [
                    { title: "Ordem" },
                    { title: "Nro. Consulta" },
                    { title: "Data Consulta" },
                    { title: "Hora Consulta" },
                    { title: "Tipo Consulta" },
                    { title: "Nome Paciente" },
                    { title: "Médico" },
                    {
                        title: "Detalhes",
                        data: null,
                        defaultContent: '<a class="view" title="Info Paciente" data-toggle="modal" data-target=".bd-example-modal-xl"><i class="material-icons">&#xe92c;</i></a>    <a href="../agendamento/historico_consultas.html" class="view" title="Consultas"  data-toggle="tooltip"><i class="material-icons">&#xe85d;</i></a>'
                        // defaultContent: '<i class="fa fa-pencil"/>'
        
                    },
                    {
                        title: "Actions",
                        data: null,
                        defaultContent: '<a ref="#" class="view" title="Editar" data-toggle="tooltip"><i class="material-icons">&#xe3c9;</i></a> <a href="#" class="view btnDelete" title="Consulta" data-toggle="tooltip"><i class="material-icons">&#xe872;</i></a>'
                    }
                    // { title: "Detalhes" },
                    // { title: "Action" }
                ]
            });
        });





    })




// var obj = [{ "nroConsulta": 1, "dataConsulta": "02/08/2022 00:00:00", "medico": { "crm": "3711", "primeiroNome": "Wilmar", "sobrenome": "Guimarães", "dataNascimento": "1974-03-01", "tipoSanguineo": "A", "fatorRh": "+", "sexo": "Masculino", "nroLogradouro": 109, "complementoLogradouro": "Próximo da Mercado X", "endereco": { "bairro": { "nomeBairro": "Santa Rosa", "identifierName": "idBairro", "identifier": 15 }, "cidade": { "nomeCidade": "Cuiabá", "uf": { "nomeUF": "Mato Grosso", "identifierName": "siglaUF", "identifier": "MT" }, "identifierName": "idCidade", "identifier": 7 }, "logradouro": { "nomeLogradouro": "Holanda", "tipoLogradouro": { "nomeTipo": "Rua", "identifierName": "siglaTipoLogradouro", "identifier": "R" }, "identifierName": "idLogradouro", "identifier": 11 }, "cep": "78040225", "identifierName": "idEndereco", "identifier": 1 }, "crmUF": { "nomeUF": "", "identifierName": "siglaUF", "identifier": "PR" }, "listaEspecialidade": [{ "tipoEspecialidade": "Neurologia", "identifierName": "idEspecialidadeMedica" }], "identifierName": "idMedico", "identifier": 1 }, "paciente": { "cpf": "74916644905", "primeiroNome": "Eduardo", "sobrenome": "Sakamoto", "dataNascimento": "2000-12-10", "tipoSanguineo": "AB", "fatorRh": "+", "sexo": "Masculino", "nroLogradouro": 604, "complementoLogradouro": "Casa", "endereco": { "bairro": { "nomeBairro": "Jardim Ipê", "identifierName": "idBairro" }, "cidade": { "nomeCidade": "Foz do Iguaçu", "uf": { "identifierName": "siglaUF", "identifier": "Paraná" }, "identifierName": "idCidade" }, "logradouro": { "nomeLogradouro": "Brodoski", "tipoLogradouro": { "nomeTipo": "Avenida", "identifierName": "siglaTipoLogradouro" }, "identifierName": "idLogradouro" }, "cep": "85869050", "identifierName": "idEndereco" }, "identifierName": "idPaciente", "identifier": 1 }, "especialidadeMedica": { "tipoEspecialidade": "Neurologia", "identifierName": "idEspecialidadeMedica" }, "identifierName": "idConsultaMedica", "identifier": 1 }, { "nroConsulta": 1, "dataConsulta": "02/08/2022 00:00:00", "medico": { "crm": "19050", "primeiroNome": "Luiz", "sobrenome": "Perez", "dataNascimento": "1982-05-07", "tipoSanguineo": "O", "fatorRh": "-", "sexo": "Masculino", "nroLogradouro": 300, "complementoLogradouro": "Próximo do prédio da prefeitura", "endereco": { "bairro": { "nomeBairro": "Itaipu A", "identifierName": "idBairro", "identifier": 1 }, "cidade": { "nomeCidade": "Foz do Iguaçu", "uf": { "nomeUF": "Paraná", "identifierName": "siglaUF", "identifier": "PR" }, "identifierName": "idCidade", "identifier": 4 }, "logradouro": { "nomeLogradouro": "Engenheiro Hildemar Leite Franca", "tipoLogradouro": { "nomeTipo": "Avenida", "identifierName": "siglaTipoLogradouro", "identifier": "Av" }, "identifierName": "idLogradouro", "identifier": 10 }, "cep": "85860320", "identifierName": "idEndereco", "identifier": 2 }, "crmUF": { "nomeUF": "", "identifierName": "siglaUF", "identifier": "PR" }, "listaEspecialidade": [{ "tipoEspecialidade": "Cardiologia", "identifierName": "idEspecialidadeMedica" }], "identifierName": "idMedico", "identifier": 2 }, "paciente": { "cpf": "74379554902", "primeiroNome": "Tânia", "sobrenome": "Silva", "dataNascimento": "1986-03-17", "tipoSanguineo": "O", "fatorRh": "-", "sexo": "Feminino", "nroLogradouro": 485, "complementoLogradouro": "", "endereco": { "bairro": { "nomeBairro": "Jardim Tropical", "identifierName": "idBairro" }, "cidade": { "nomeCidade": "Foz do Iguaçu", "uf": { "identifierName": "siglaUF", "identifier": "Paraná" }, "identifierName": "idCidade" }, "logradouro": { "nomeLogradouro": "Figueira", "tipoLogradouro": { "nomeTipo": "Rua", "identifierName": "siglaTipoLogradouro" }, "identifierName": "idLogradouro" }, "cep": "85855070", "identifierName": "idEndereco" }, "identifierName": "idPaciente", "identifier": 2 }, "especialidadeMedica": { "tipoEspecialidade": "Oftalmologia", "identifierName": "idEspecialidadeMedica" }, "identifierName": "idConsultaMedica", "identifier": 2 }]

// console.log(obj)



// var lista = obj;
// var teste = {};
// var lista2 = [];
// var anoAtual = new Date().getFullYear();

// for (var i = 0; i < lista.length; i++) {
//     var dataConsulta = lista[i].dataConsulta.split(" ")[0]
//     var horaConsulta = lista[i].dataConsulta.split(" ")[1].split(":")
//     // console.log(dataConsulta,horaConsulta)
//     lista2[i] = teste = {
//         ordem: i + 1,
//         nroConsulta: lista[i].nroConsulta,
//         dataConsulta: dataConsulta,
//         horaConsulta: horaConsulta[0] + ":" + horaConsulta[1],
//         tipoEspecialidade: lista[i].especialidadeMedica.tipoEspecialidade,
//         paciente: lista[i].paciente.primeiroNome + " " + lista[i].paciente.sobrenome,
//         medico: lista[i].medico.primeiroNome + " " + lista[i].medico.sobrenome,

//     }

// }


// var dataSet = lista2.map(Object.values)
// console.log(lista2)


// $(document).ready(function () {
//     $('#tbListaConsultaMedica').DataTable({
//         data: dataSet,
//         columns: [
//             { title: "Ordem" },
//             { title: "Nro. Consulta" },
//             { title: "Data Consulta" },
//             { title: "Hora Consulta" },
//             { title: "Tipo Consulta" },
//             { title: "Nome Paciente" },
//             { title: "Médico" },
//             {
//                 title: "Detalhes",
//                 data: null,
//                 defaultContent: '<a class="view" title="Info Paciente" data-toggle="modal" data-target=".bd-example-modal-xl"><i class="material-icons">&#xe92c;</i></a>    <a href="../agendamento/historico_consultas.html" class="view" title="Consultas"  data-toggle="tooltip"><i class="material-icons">&#xe85d;</i></a>'
//                 // defaultContent: '<i class="fa fa-pencil"/>'

//             },
//             {
//                 title: "Actions",
//                 data: null,
//                 defaultContent: '<a ref="#" class="view" title="Editar" data-toggle="tooltip"><i class="material-icons">&#xe3c9;</i></a> <a href="#" class="view btnDelete" title="Consulta" data-toggle="tooltip"><i class="material-icons">&#xe872;</i></a>'
//             }
//             // { title: "Detalhes" },
//             // { title: "Action" }
//         ]
//     });
// });