// const url = "http://localhost:8080/rest/medic/get/1";
// const url = "http://localhost:8080/rest/medic/getAll";
// const url = "http://localhost:8080/rest/patient/get/1";
// const url = "http://localhost:8080/rest/patient/getAll";
// const url = "http://172.25.239.120:8080/rest/patient/getAll";
// const url = "http://jsonplaceholder.typicode.com/users";
// const url = "http://localhost:8080/api/rest/ListaPaciente";
const url = "http://localhost:8080/rest/patient/getAllActive";


fetch(url, {
    method: "GET",
    // mode: 'no-cors',
    header: {
        // "Access-token": "xxx",
        Accept: 'application/json',
        "Content-Type": "application/json"
    }
}
)
    // Handle success
    .then(response => response.json())  // convert to json
    .then((data) => {


        // console.log(data)
        // var lista = data.Usuario;
        var lista = data;
        var teste = {};
        var lista2 = [];
        var anoAtual = new Date().getFullYear();

        for (var i = 0; i < lista.length; i++) {
            var dataAniversario = lista[i].dataNascimento.split("-")[0]
            lista2[i] = teste = {
                // idUsuario: lista[i].idUsuario,
                idUsuario: lista[i].identifier,
                firstName: lista[i].primeiroNome + " " + lista[i].sobrenome,
                cpf: lista[i].cpf,
                // dataConsulta: lista[i].ConsultaMedicas[0].dataConsulta,
                // dataConsulta: erro(i)
                idade: anoAtual - dataAniversario
            }

        }

        // function erro() {
        //     try {
        //         console.log("verdade")
        //         return lista[i].ConsultaMedicas[0].dataConsulta
        //     } catch (err) {
        //         console.log("Mentira")
        //         return "Sem consulta"
        //     }
        // }

        // console.log(lista2.map(Object.values));
        var dataSet = lista2.map(Object.values)
        console.log(dataSet)


        $(document).ready(function () {
            $('#tabelaListaPaciente').DataTable({
                data: dataSet,
                columns: [
                    { title: "ID" },
                    { title: "Nome" },
                    { title: "CPF" },
                    { title: "idade" },
                    {
                        title: "Detalhes",
                        data: null,
                        defaultContent: '<a class="view" title="Info Paciente" data-toggle="modal" data-target=".bd-example-modal-xl"><i class="material-icons">&#xe92c;</i></a> <a href="../agendamento/historico_consultas.html" class="view" title="Consultas" data-toggle="tooltip"><i class="material-icons">&#xe85d;</i></a> <a href="#" class="view" title="Baixar Prontuário" data-toggle="tooltip"><i class="material-icons">&#xe2c4;</i></a>'
                        // defaultContent: '<i class="fa fa-pencil"/>'

                    },
                    {
                        title: "Actions",
                        data: null,
                        defaultContent: '<a ref="#" class="view" title="Editar" data-toggle="tooltip"><i class="material-icons">&#xe3c9;</i></a> <a href="#" class="view btnDelete" title="Consulta" data-toggle="tooltip"><i class="material-icons">&#xe872;</i></a>'
                    }
                    // { title: "Detalhes" },
                    // { title: "Action" }
                ]
            });
        });





    })



// var data = [{ "cpf": "74916644905", "primeiroNome": "Eduardo", "sobrenome": "Sakamoto", "dataNascimento": "2000-12-10", "tipoSanguineo": "AB", "fatorRh": "+", "sexo": "Masculino", "nroLogradouro": 604, "complementoLogradouro": "Casa", "endereco": { "bairro": { "nomeBairro": "Jardim Ipê", "identifierName": "idBairro" }, "cidade": { "nomeCidade": "Foz do Iguaçu", "uf": { "nomeUF": "Paraná", "identifierName": "siglaUF", "identifier": "PR" }, "identifierName": "idCidade" }, "logradouro": { "nomeLogradouro": "Brodoski", "tipoLogradouro": { "nomeTipo": "Avenida", "identifierName": "siglaTipoLogradouro" }, "identifierName": "idLogradouro" }, "cep": "85869050", "identifierName": "idEndereco" }, "identifierName": "idPaciente", "identifier": 1 }, { "cpf": "74379554902", "primeiroNome": "Tânia", "sobrenome": "Silva", "dataNascimento": "1986-03-17", "tipoSanguineo": "O", "fatorRh": "-", "sexo": "Feminino", "nroLogradouro": 485, "complementoLogradouro": "", "endereco": { "bairro": { "nomeBairro": "Jardim Tropical", "identifierName": "idBairro" }, "cidade": { "nomeCidade": "Foz do Iguaçu", "uf": { "nomeUF": "Paraná", "identifierName": "siglaUF", "identifier": "PR" }, "identifierName": "idCidade" }, "logradouro": { "nomeLogradouro": "Figueira", "tipoLogradouro": { "nomeTipo": "Rua", "identifierName": "siglaTipoLogradouro" }, "identifierName": "idLogradouro" }, "cep": "85855070", "identifierName": "idEndereco" }, "identifierName": "idPaciente", "identifier": 2 }, { "cpf": "35819846958", "primeiroNome": "Hugo", "sobrenome": "Vieira", "dataNascimento": "1975-01-23", "tipoSanguineo": "AB", "fatorRh": "-", "sexo": "Masculino", "nroLogradouro": 344, "complementoLogradouro": "Na esquina", "endereco": { "bairro": { "nomeBairro": "Jardim Tropical", "identifierName": "idBairro" }, "cidade": { "nomeCidade": "Foz do Iguaçu", "uf": { "nomeUF": "Paraná", "identifierName": "siglaUF", "identifier": "PR" }, "identifierName": "idCidade" }, "logradouro": { "nomeLogradouro": "Figueira", "tipoLogradouro": { "nomeTipo": "Rua", "identifierName": "siglaTipoLogradouro" }, "identifierName": "idLogradouro" }, "cep": "85855070", "identifierName": "idEndereco" }, "identifierName": "idPaciente", "identifier": 3 }]


// // console.log(data)
// // var lista = data.Usuario;
// var lista = data;
// var teste = {};
// var lista2 = [];
// var anoAtual = new Date().getFullYear();

// for (var i = 0; i < lista.length; i++) {
//     var dataAniversario = lista[i].dataNascimento.split("-")[0]
//     lista2[i] = teste = {
//         // idUsuario: lista[i].idUsuario,
//         idUsuario: lista[i].identifier,
//         firstName: lista[i].primeiroNome + " " + lista[i].sobrenome,
//         cpf: lista[i].cpf,
//         // dataConsulta: lista[i].ConsultaMedicas[0].dataConsulta,
//         // dataConsulta: erro(i)
//         idade: anoAtual - dataAniversario
//     }

// }

// // function erro() {
// //     try {
// //         console.log("verdade")
// //         return lista[i].ConsultaMedicas[0].dataConsulta
// //     } catch (err) {
// //         console.log("Mentira")
// //         return "Sem consulta"
// //     }
// // }

// // console.log(lista2.map(Object.values));
// var dataSet = lista2.map(Object.values)
// console.log(dataSet)


// $(document).ready(function () {
//     $('#tabelaListaPaciente').DataTable({
//         data: dataSet,
//         columns: [
//             { title: "ID" },
//             { title: "Nome" },
//             { title: "CPF" },
//             { title: "idade" },
//             {
//                 title: "Detalhes",
//                 data: null,
//                 defaultContent: '<a class="view" title="Info Paciente" data-toggle="modal" data-target=".bd-example-modal-xl"><i class="material-icons">&#xe92c;</i></a> <a href="../agendamento/historico_consultas.html" class="view" title="Consultas" data-toggle="tooltip"><i class="material-icons">&#xe85d;</i></a> <a href="#" class="view" title="Baixar Prontuário" data-toggle="tooltip"><i class="material-icons">&#xe2c4;</i></a>'
//                 // defaultContent: '<i class="fa fa-pencil"/>'

//             },
//             {
//                 title: "Actions",
//                 data: null,
//                 defaultContent: '<a ref="#" class="view" title="Editar" data-toggle="tooltip"><i class="material-icons">&#xe3c9;</i></a> <a href="#" class="view btnDelete" title="Consulta" data-toggle="tooltip"><i class="material-icons">&#xe872;</i></a>'
//             }
//             // { title: "Detalhes" },
//             // { title: "Action" }
//         ]
//     });
// });
