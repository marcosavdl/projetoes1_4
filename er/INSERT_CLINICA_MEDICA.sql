insert into "UnidadeFederacao"("siglaUF","nomeUF") values('PR','Paraná');
insert into "UnidadeFederacao"("siglaUF","nomeUF") values('SP','São Paulo');
insert into "UnidadeFederacao"("siglaUF","nomeUF") values('SC','Santa Catarina');
insert into "UnidadeFederacao"("siglaUF","nomeUF") values('MS','Mato Grosso do Sul');
insert into "UnidadeFederacao"("siglaUF","nomeUF") values('RS','Rio Grande do Sul');
insert into "UnidadeFederacao"("siglaUF","nomeUF") values('MG','Minas Gerais');

insert into "Cidade"("nomeCidade","siglaUF") values ('Foz do Iguaçu','PR');
insert into "Cidade"("nomeCidade","siglaUF") values ('Londrina','PR');
insert into "Cidade"("nomeCidade","siglaUF") values ('Maringá','PR');
insert into "Cidade"("nomeCidade","siglaUF") values ('Cuiabá','MG');
insert into "Cidade"("nomeCidade","siglaUF") values ('Presidente Prudente','SP');
insert into "Cidade"("nomeCidade","siglaUF") values ('Belo Horizonte','MG');

insert into "Bairro"("nomeBairro") values ('Itaipu A');
insert into "Bairro"("nomeBairro") values ('Jardim Lancaster');
insert into "Bairro"("nomeBairro") values ('Centro');
insert into "Bairro"("nomeBairro") values ('Itaipu C');
insert into "Bairro"("nomeBairro") values ('Morumbi');

insert into "TipoLogradouro"("siglaLogradouro","nomeTipo") values('R','Rua');
insert into "TipoLogradouro"("siglaLogradouro","nomeTipo") values('Av','Avenida');
insert into "TipoLogradouro"("siglaLogradouro","nomeTipo") values('Pq','Parque');

insert into "Logradouro"("nomeLogradouro","siglaLogradouro") values ('Acarí','R');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro") values ('Ariranha','R');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro") values ('B','R');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro") values ('Assunção','R');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro") values ('Sucuri','R');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro") values ('Cotia','R');

insert into "Endereco"("cep","idBairro","idLogradouro","idCidade") values('85860330','1','1','1');
insert into "Endereco"("cep","idBairro","idLogradouro","idCidade") values('85861140','1','1','1');
insert into "Endereco"("cep","idBairro","idLogradouro","idCidade") values('85870300','4','3','1');
insert into "Endereco"("cep","idBairro","idLogradouro","idCidade") values('85860330','4','4','1');
insert into "Endereco"("cep","idBairro","idLogradouro","idCidade") values('85860320','1','5','1');
insert into "Endereco"("cep","idBairro","idLogradouro","idCidade") values('85860330','1','6','1');

insert into "Celular"("nroCelular","isWhatsapp") values('45998568952',true);
insert into "Celular"("nroCelular","isWhatsapp") values('45985236587',false);
insert into "Celular"("nroCelular","isWhatsapp") values('45997854254',false);
insert into "Celular"("nroCelular","isWhatsapp") values('45997125874',true);
insert into "Celular"("nroCelular","isWhatsapp") values('45998521679',true);
insert into "Celular"("nroCelular","isWhatsapp") values('45999974856',true);

insert into "Usuario"("senhaUsuario","tipoUsuario","sexo","dataNascimento","tipoSanguineo","firstName","lastName","numeroLogradouro","complementoLogradouro")
               values('8D969EEF6C92','Paciente'   ,'M'   ,'1982-08-25'    ,'B+'           ,'Francisco','Gonçalves',            '104',           'Na esquina');
insert into "Usuario"("senhaUsuario","tipoUsuario","sexo","dataNascimento","tipoSanguineo","firstName","lastName","numeroLogradouro","complementoLogradouro") values('B7E94BE513E96E8C45CD23D162275E5A12EBDE9100A425C4EBCDD7FA4DCD897C','Paciente','F','1957-04-26','A+','Antonia','Santana','1052','Em frente ao mercado X');
insert into "Usuario"("senhaUsuario","tipoUsuario","sexo","dataNascimento","tipoSanguineo","firstName","lastName","numeroLogradouro","complementoLogradouro") values('67A50AB68AF0F3D8384850AAB5680F93D7A4C59A26268B214B7C324A4DCDADF5','Paciente','F','1957-04-26','A-','Aline','Marques','604','Do lado da farmácia Y');
insert into "Usuario"("senhaUsuario","tipoUsuario","sexo","dataNascimento","tipoSanguineo","firstName","lastName","numeroLogradouro","complementoLogradouro") values('18A7B0C92E9F41B0AFEE27ED2C04E49192489F06489940F61661E3A8E43DFEDD','Médico','M','1979-07-06','AB+','Eduardo','Silva','407','Próximo do parque Z');
insert into "Usuario"("senhaUsuario","tipoUsuario","sexo","dataNascimento","tipoSanguineo","firstName","lastName","numeroLogradouro","complementoLogradouro") values('4067197C23C03C4B814B7A45D43BCE4FBA68D7AC701E43BDBD9F8ADA610CB948','Médico','F','1988-11-16','B-','Clara','Mendonça','201','Portão azul');
insert into "Usuario"("senhaUsuario","tipoUsuario","sexo","dataNascimento","tipoSanguineo","firstName","lastName","numeroLogradouro","complementoLogradouro") values('7CEEEAC08DD2D3A5A60DEE4CBED925A817E69568F2BA93CB7CE8A6B38ACDD231','Médico','M','1972-06-30','B+','Marcos','Rodrigues','254','Próximo da praça XYZ');

insert into "Email"("email", "idUsuario_Usuario") values('francisco_gg@outlook.com','1');
insert into "Email"("email", "idUsuario_Usuario") values('antonia_msantana@hotmail.com','1');
insert into "Email"("email", "idUsuario_Usuario") values('alineramos_marques@hotmail.com','1');
insert into "Email"("email", "idUsuario_Usuario") values('eduardo_opsilva@hotmail.com','1');
insert into "Email"("email", "idUsuario_Usuario") values('claracc_mendonca@hotmail.com','1');
insert into "Email"("email", "idUsuario_Usuario") values('marcos_rod@hotmail.com','1');


insert into "Paciente"("cpf","idUsuario_Usuario") values('613124415169',1);
insert into "Paciente"("cpf","idUsuario_Usuario") values('71113316179',2);
insert into "Paciente"("cpf","idUsuario_Usuario") values('90131078798',3);	

insert into "Medico"("crm","idUsuario_Usuario","siglaUF_UnidadeFederacao") values('1500',4,'PR');
insert into "Medico"("crm","idUsuario_Usuario","siglaUF_UnidadeFederacao") values('4569',5,'PR');
insert into "Medico"("crm","idUsuario_Usuario","siglaUF_UnidadeFederacao") values('5478',6,'PR');

insert into "EspecialidadeMedica"("tipoEspecialidade") values('Cirurgia Geral');
insert into "EspecialidadeMedica"("tipoEspecialidade") values('Clínica Médica');
insert into "EspecialidadeMedica"("tipoEspecialidade") values('Dermatologia');
insert into "EspecialidadeMedica"("tipoEspecialidade") values('Endoscopia');
insert into "EspecialidadeMedica"("tipoEspecialidade") values('Infectologia');
insert into "EspecialidadeMedica"("tipoEspecialidade") values('Nutrilogia');
insert into "EspecialidadeMedica"("tipoEspecialidade") values('Pediatria');
insert into "EspecialidadeMedica"("tipoEspecialidade") values('Urologia');

insert into "ConsultaMedica"("nroConsulta","dataConsulta","idUsuario_Usuario") values(1,'2022-02-20',1);
insert into "ConsultaMedica"("nroConsulta","dataConsulta","idUsuario_Usuario") values(1,'2022-03-05',2);
insert into "ConsultaMedica"("nroConsulta","dataConsulta","idUsuario_Usuario") values(1,'2022-04-19',3);

insert into "PrescricaoRemedio"("nomeRemedio","posologia","idConsultaMedica_ConsultaMedica") values('Ivermectina','Uso oral com dose única por dia (ingerido com água) durante o período de 10 dias',1);
insert into "PrescricaoRemedio"("nomeRemedio","posologia","idConsultaMedica_ConsultaMedica") values('Rosuvastatina Cálcica Sandoz','Os comprimidos de rosuvastatina cálcica devem ser ingeridos inteiros, uma vez ao dia, por via oral, com água, a qualquer hora do dia, com ou sem alimentos. Porém, procure tomar rosuvastatina cálcica no mesmo horário, todos os dias. Obs: O medicamento não deve ser partido ou mastigado',2);
insert into "PrescricaoRemedio"("nomeRemedio","posologia","idConsultaMedica_ConsultaMedica") values('Atorvastatina Cálcica Eurofarma','Este medicamento deve ser usado após a prescrição médica. A dose pode variar de 10 a 80 mg em dose única diária, usada a qualquer hora do dia, com ou sem alimentos. As doses iniciais e de manutenção devem ser individualizadas de acordo com os níveis iniciais do colesterol sanguíneo, a meta do tratamento e a resposta do paciente. Após o início do tratamento e/ou durante o ajuste de dose de atorvastatina cálcica, os efeitos aparecem após 2 a 4 semanas, portanto os exames para avaliação do resultado do ajuste da dose devem ser feitos após esse período',2);
insert into "PrescricaoRemedio"("nomeRemedio","posologia","idConsultaMedica_ConsultaMedica") values('Espironolactona EMS','A dose diária pode ser administrada em doses fracionadas ou em dose única.',3);
insert into "PrescricaoRemedio"("nomeRemedio","posologia","idConsultaMedica_ConsultaMedica") values('Furosemida Neo Química','Você deve tomar os comprimidos com líquido, por via oral e com o estômago vazio. É vantajoso tomar a dose diária de uma só vez, escolhendo-se o horário mais prático, de tal forma que não interfira no seu ritmo normal de vida, pela rapidez da diurese (desejo de urinar).',3);


